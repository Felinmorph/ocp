package lotto;

public class Lotto
{
	private int[] zahlen;
	private int anzWerte = 7;
	private int ausAnzWerten = 49;

	public Lotto()
	{
		this.zahlen = new int[this.anzWerte];
	}

	public void generate()
	{
		boolean nochmal = false;
		this.zahlen = new int[this.anzWerte];
		for (int i = 0; i < this.zahlen.length; i++)				// fuer alle zahlen
		{
			do
			{
				nochmal = false;
				this.zahlen[i] = (int) (Math.random() * 49) + 1; 	// zufallszahl generieren
				for (int j = 0; j < i; j++)							// fuer alle bereits existierenden zahlen
				{
					if (this.zahlen[i] == this.zahlen[j])			// pruefen ob schon gezogen -> nochmal
					{
						nochmal = true;
					}
				}

			} while (nochmal);
		}
		this.sortArray(this.zahlen);
	}

	private void sortArray(int[] werte)
	{
		for (int i = 0; i < werte.length; i++)
		{
			for (int j = i + 1; j < werte.length; j++)
			{
				if (werte[j] < werte[i])
				{
					int t = werte[j];
					werte[j] = werte[i];
					werte[i] = t;
				}
			}
		}
	}

	public int[] getDraw()
	{
		return this.zahlen;
	}

	public int compareWithTip(int[] tipp)
	{
		int hits = 0;
		this.validatedTipArray(tipp);
		for (int i = 0; i < tipp.length; i++)
		{
			for (int j = 0; j < this.zahlen.length; j++)
			{
				if (tipp[i] == this.zahlen[j])
					hits++;
			}
		}
		return hits;
	}

	private void validatedTipArray(int[] tipp)
	{
		
		for (int i = 0; i < tipp.length; i++)
		{
			for (int j = i + 1; j < tipp.length; j++)
			{
				if (tipp[i] == tipp[j])
					tipp[j] = -1;			// doppelter eintrag
			}
		}
	}
}
