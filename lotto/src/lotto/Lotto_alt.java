package lotto;

public class Lotto_alt
{

	private static final long l=1L;		// konstante zum rumschieben
	private long werte = 0L;

	public Lotto_alt()
	{
	}

	public void generate()
	{
		this.werte = 0L;
		
		for (int i = 0; i < 7; i++)						// fuer alle zahlen
		{
			this.generateSingleValueInLong();
		}
	}
	
	public int[] getDraw()
	{
		return this.LongToIntArray();
	}

	private void generateSingleValueInLong()
	{
		int v= 0;
		do
		{
			v = (int) (Math.random() * 49) + 1;
		} while ((this.werte & (l<<v)) != 0);			// boldly assuming we do not run out of possible values!
		this.werte += l<<v;
	}
	
	private int[] LongToIntArray()
	{
		int zahlen[] = new int[7];
		
		for (int i = 0, c = 0; i < 50 && c < 7; i++)						// fuer alle zahlen
		{
			if ((this.werte & (l<<i)) != 0)
			{
				zahlen[c] = i;
				c++;
			}
		}
		
		return zahlen;

	}
	private int compareWithTip(int[] tip,int[] draw)
	{
		int hits = 0;
		if (tip.length == draw.length)
		{
			for (int i = 0; i < tip.length; i++)
			{
				for (int j = 0; j < draw.length; j++)
				{
					if (tip[i]==draw[j])
						hits++;
				}
			}
		}
		return hits;
	}

}
