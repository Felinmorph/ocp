package lotto;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

public class A1 extends JPanel
{
	JLabel[] labels;
	/**
	 * Create the panel.
	 */
	public A1()
	{
		this.labels = new JLabel[7];
		
		GridBagLayout gbl_panel_A1 = new GridBagLayout();
		gbl_panel_A1.columnWidths = new int[]{0, 251, 0, 0};
		gbl_panel_A1.rowHeights = new int[]{0, 67, 0, 0};
		gbl_panel_A1.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_A1.rowWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
		this.setLayout(gbl_panel_A1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setPreferredSize(new Dimension(311, 65));
		panel_1.setSize(new Dimension(311, 65));
		panel_1.setMinimumSize(new Dimension(311, 65));
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 5);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 1;
		gbc_panel_1.gridy = 1;
		this.add(panel_1, gbc_panel_1);
		panel_1.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 310, 30);
		panel_1.add(panel);
		panel.setLayout(new GridLayout(1, 6, 12, 12));
		
		for (int i = 0; i < 7; i++)
		{
			this.labels[i] = new JLabel("0"+i);
			this.labels[i].setHorizontalAlignment(SwingConstants.CENTER);
			this.labels[i].setBorder(new LineBorder(new Color(0, 0, 0)));
			panel.add(this.labels[i]);
		}
		
		
		
		JButton btnGenerate = new JButton("Generate");
		btnGenerate.addActionListener( this::generate);
		btnGenerate.setBounds(0, 41, 310, 23);
		panel_1.add(btnGenerate);
	}
	public void generate(ActionEvent e)
	{
		Lotto l = new Lotto();
		l.generate();
		int[] v = l.getDraw();
		for (int i = 0; i < 7; i++)
		{
			String s = String.format("%2d", v[i]);
			this.labels[i].setText(s);
		}
	}
}
