package lotto;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

public class A2 extends JPanel
{
	private JLabel[] labels;
	private JComboBox[] comboBoxes;
	JLabel label_Gewinn;
	private static final String[] numbers = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
			"20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42",
			"43", "44", "45", "46", "47", "48", "49" };

	Lotto lotto;

	/**
	 * Create the panel.
	 */
	public A2()
	{
		this.labels = new JLabel[7];
		this.comboBoxes = new JComboBox[7];

		GridBagLayout gbl_panel_A1 = new GridBagLayout();
		gbl_panel_A1.columnWidths = new int[] { 0, 251, 0, 0 };
		gbl_panel_A1.rowHeights = new int[] { 0, 67, 0, 0 };
		gbl_panel_A1.columnWeights = new double[] { 1.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_panel_A1.rowWeights = new double[] { 1.0, 0.0, 1.0, Double.MIN_VALUE };
		this.setLayout(gbl_panel_A1);

		JPanel panel_1 = new JPanel();
		panel_1.setPreferredSize(new Dimension(360, 180));
		panel_1.setSize(new Dimension(360, 180));
		panel_1.setMinimumSize(new Dimension(360, 180));
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 5);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 1;
		gbc_panel_1.gridy = 1;
		this.add(panel_1, gbc_panel_1);
		panel_1.setLayout(new GridLayout(0, 1, 0, 0));

		JLabel lblIhrTipp = new JLabel("Ihr Tipp:");
		lblIhrTipp.setVerticalAlignment(SwingConstants.BOTTOM);
		panel_1.add(lblIhrTipp);

		JPanel panel_Tipp = new JPanel();
		for (int i = 0; i < 7; i++)
		{
			this.comboBoxes[i] = new JComboBox();
			panel_Tipp.add(this.comboBoxes[i]);
			this.comboBoxes[i].setModel(new DefaultComboBoxModel(numbers));
			this.comboBoxes[i].setSelectedIndex(i);
		}
		panel_1.add(panel_Tipp);
		panel_Tipp.setLayout(new GridLayout(0, 7, 12, 12));

		JButton btnGenerate = new JButton("Generate");
		btnGenerate.addActionListener(this::drawnumbers);

		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3);
		panel_1.add(btnGenerate);

		JLabel lblGenerierteWerte = new JLabel("Generierte werte:");
		lblGenerierteWerte.setVerticalAlignment(SwingConstants.BOTTOM);
		panel_1.add(lblGenerierteWerte);

		JPanel panel_Ziehung = new JPanel();
		for (int i = 0; i < 7; i++)
		{
			this.labels[i] = new JLabel("0" + i);
			this.labels[i].setHorizontalAlignment(SwingConstants.CENTER);
			this.labels[i].setBorder(new LineBorder(new Color(0, 0, 0)));
			panel_Ziehung.add(this.labels[i]);
		}
		panel_1.add(panel_Ziehung);
		panel_Ziehung.setLayout(new GridLayout(1, 6, 12, 12));

		this.label_Gewinn = new JLabel("Gewinn: 15€");
		this.label_Gewinn.setHorizontalAlignment(SwingConstants.CENTER);
		this.label_Gewinn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel_1.add(this.label_Gewinn);

//		
//		JComboBox comboBox = new JComboBox();
//		panel_2.add(comboBox);
//		comboBox.setModel(new DefaultComboBoxModel(numbers));
//		comboBox.setSelectedIndex(9);
//		
//		JComboBox comboBox_1 = new JComboBox();
//		comboBox_1.setModel(new DefaultComboBoxModel(numbers));
//		comboBox_1.setSelectedIndex(9);
//		panel_2.add(comboBox_1);
//		
//		JComboBox comboBox_2 = new JComboBox();
//		comboBox_2.setModel(new DefaultComboBoxModel(numbers));
//		comboBox_2.setSelectedIndex(9);
//		panel_2.add(comboBox_2);
//		
//		JComboBox comboBox_3 = new JComboBox();
//		comboBox_3.setModel(new DefaultComboBoxModel(numbers));
//		comboBox_3.setSelectedIndex(9);
//		panel_2.add(comboBox_3);
//		
//		JComboBox comboBox_4 = new JComboBox();
//		comboBox_4.setModel(new DefaultComboBoxModel(numbers));
//		comboBox_4.setSelectedIndex(9);
//		panel_2.add(comboBox_4);
//		
//		JComboBox comboBox_5 = new JComboBox();
//		comboBox_5.setModel(new DefaultComboBoxModel(numbers));
//		comboBox_5.setSelectedIndex(9);
//		panel_2.add(comboBox_5);
//		
//		JComboBox comboBox_6 = new JComboBox();
//		comboBox_6.setModel(new DefaultComboBoxModel(numbers));
//		comboBox_6.setSelectedIndex(9);
//		panel_2.add(comboBox_6);

	}

	public void drawnumbers(ActionEvent e)
	{
		this.lotto = new Lotto();
		this.lotto.generate();
		int[] v = this.lotto.getDraw();
		for (int i = 0; i < 7; i++)
		{
			String s = String.format("%2d", v[i]);
			this.labels[i].setText(s);
		}
		int t = this.lotto.compareWithTip(this.getTip());
		int gewinn = (int) Math.pow(10.0, t - 1);
		this.label_Gewinn.setText("Gewinn: " + gewinn + "€");
	}

	private int getNumberOfHits(int[] tip, int[] draw)
	{
		int hits = 0;
		if (tip.length == draw.length)
		{
			for (int i = 0; i < tip.length; i++)
			{
				for (int j = 0; j < draw.length; j++)
				{
					if (tip[i] == draw[j])
						hits++;
				}
			}
		}
		return hits;
	}

	public int[] getTip()
	{
		int[] tipp = new int[7];

		for (int i = 0; i < tipp.length; i++)
		{
			tipp[i] = this.comboBoxes[i].getSelectedIndex() + 1;
		}
		return tipp;
	}
}
