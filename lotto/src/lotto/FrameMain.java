package lotto;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;

public class FrameMain extends JFrame
{

	private JPanel contentPane;

	public FrameMain()
	{
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(100, 100, 689, 466);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.contentPane.setLayout(new BorderLayout(0, 0));
		this.setContentPane(this.contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		this.contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		
		JPanel panel_A1 = new A1();
		tabbedPane.addTab("Aufgabe 1", null, panel_A1, null);
		
		JPanel panel = new A2();
		tabbedPane.addTab("Aufgabe 2", null, panel, null);
	}
}
