package test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Test
{

	public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException
	{
		LocalTime t1_1 = LocalTime.of(8, 2);
		LocalTime t1_2 = LocalTime.of(9, 32);
		LocalTime t2_1 = LocalTime.of(9, 52);
		LocalTime t2_2 = LocalTime.of(11, 19);
		LocalTime t3_1 = LocalTime.of(11, 40);
		LocalTime t3_2 = LocalTime.of(13, 1);

		Duration d1 = Duration.between(t1_1, t1_2);
		Duration d2 = Duration.between(t2_1, t2_2);
		Duration d3 = Duration.between(t3_1, t3_2);

		Duration d123 = d1.plus(d2).plus(d3);
		System.out.println("unterrichtsdauer = " + d123.toMinutes() + " Minuten");

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		LocalDate ld1 = LocalDate.of(1980, 10, 8);
		long l1 = ChronoUnit.DAYS.between(ld1, LocalDate.now());
		System.out.printf("heute bin ich %,d tage alt.%n", l1);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		long l2 = ChronoUnit.DAYS.between(LocalDate.of(1900, 1, 1), LocalDate.of(2000, 1, 1));	// Inclusive, exclusive
		System.out.printf("Das 20. Jahrhundert hatte %d tage.%n", l2);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		ZonedDateTime zdt1 = ZonedDateTime.of(LocalDate.now(), LocalTime.of(19, 50), ZoneId.of("Europe/Berlin"));
		ZonedDateTime zdt2 = zdt1.plusHours(6).plusMinutes(20);
		ZonedDateTime zdt3 = zdt2.plusHours(2).plusMinutes(45);
		ZonedDateTime zdt4 = zdt3.plusHours(8).plusMinutes(10);
		ZonedDateTime zdt5 = zdt4.withZoneSameInstant(ZoneId.of("Australia/Sydney"));
		DateTimeFormatter dtf = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM, FormatStyle.LONG);
		System.out.println("Ankunft : " + dtf.format(zdt5));

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("-------------------------------------------------------");

		try ( BufferedReader buffIn = new BufferedReader(new FileReader("test.txt")) )
		{

			String line = "";
			int count = 1;
			buffIn.mark(1);
			line = buffIn.readLine();
			System.out.println(count + ": " + line);

			line = buffIn.readLine();
			count++;
			System.out.println(count + ": " + line);

			buffIn.reset();
			line = buffIn.readLine();
			count++;
			System.out.println(count + ": " + line);

		}
		catch (IOException e)
		{
			System.out.println("IOException");
			e.printStackTrace();
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("-------------------------------------------------------");
		
	    ExecutorService es = Executors.newSingleThreadExecutor();
	    es.execute( () -> System.out.print("1") );
	    es.execute( () -> System.out.print("2") );
	    es.execute( () -> System.out.print("3") );
	    es.shutdown();

	    System.out.println("");
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("-------------------------------------------------------");

        ExecutorService service = Executors.newSingleThreadExecutor();

    	Callable<String> c = () -> "a";
        Future<String> f = service.submit( c );
            
        System.out.print(f.get() + " ");
        System.out.print(f.get(1, TimeUnit.SECONDS) + " ");
        System.out.print(f.cancel(true)+ " ");
        System.out.print(f.isDone()+ " ");
        System.out.print(f.isCancelled()+ " ");
        
        service.shutdown();
        
        System.out.println("");
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("-------------------------------------------------------");

        Path p1 = FileSystems.getDefault().getPath("C:\\test1.bin");
        Path p2 = FileSystems.getDefault().getPath("C:\\test2.bin");


        
	}
	

}
