package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.util.Locale;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import logic.calc;

@SuppressWarnings("serial")
public class Panel_Main extends JPanel
{
	private JTextField text_StartKapital;
	private JTextField text_Zinssatz;
	private JTextField text_Laufzeit;
	private JTextField text_EndKapital;
	private JButton btnBerechnen;
	private JButton btnClear;
	private JButton btnBeenden;
	private JRadioButton radioButton_EndKapital;
	private JRadioButton radioButton_Laufzeit;
	private JTextArea text_Verlauf;

	public Panel_Main()
	{
		this.setBorder(new EmptyBorder(10, 10, 10, 10));
		this.setBounds(0, 0, 300, 226);
		this.setPreferredSize(new Dimension(719, 400));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 10, 95, 79, 89, 233, 0 };
		gridBagLayout.rowHeights = new int[] { 30, 0, 20, 0, 20, 0, 20, 0, 23, 0, 23, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0,
				Double.MIN_VALUE };
		this.setLayout(gridBagLayout);

		KeyAdapter myKeyAdapter = new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e)
			{
				if (e.getKeyChar() == '\n')
					KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
			}
		};

		KeyAdapter myButtonKeyAdapter = new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e)
			{
				if (e.getKeyChar() == '\n')
					((JButton) e.getComponent()).doClick();
			}
		};

		ButtonGroup group = new ButtonGroup();
		this.btnClear = new JButton_Loeschen("Löschen");
		this.btnClear.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.btnClear.addKeyListener(myButtonKeyAdapter);

		JLabel lblNewLabel_2 = new JLabel("Zinssatzberechner");
		lblNewLabel_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_2.gridwidth = 5;
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 0;
		this.add(lblNewLabel_2, gbc_lblNewLabel_2);
		this.text_StartKapital = new JTextField();
		this.text_StartKapital.addKeyListener(myKeyAdapter);

		JLabel lblNewLabel = new JLabel("Startkapital:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 2;
		this.add(lblNewLabel, gbc_lblNewLabel);
		this.text_StartKapital.setMargin(new Insets(2, 6, 2, 6));
		this.text_StartKapital.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_text_StartKapital = new GridBagConstraints();
		gbc_text_StartKapital.fill = GridBagConstraints.HORIZONTAL;
		gbc_text_StartKapital.insets = new Insets(0, 0, 5, 5);
		gbc_text_StartKapital.gridwidth = 2;
		gbc_text_StartKapital.gridx = 2;
		gbc_text_StartKapital.gridy = 2;
		this.add(this.text_StartKapital, gbc_text_StartKapital);
		this.text_StartKapital.setColumns(10);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.gridheight = 7;
		gbc_scrollPane.gridx = 4;
		gbc_scrollPane.gridy = 2;
		this.add(scrollPane, gbc_scrollPane);
		scrollPane.setFocusable(false);

		this.text_Verlauf = new JTextArea();
		this.text_Verlauf.setFont(new Font("Lucida Console", Font.PLAIN, 13));
		this.text_Verlauf.setEditable(false);
		this.text_Verlauf.setText("Verlauf");
		scrollPane.setViewportView(this.text_Verlauf);
		this.text_Verlauf.setFocusable(false);

		this.text_Zinssatz = new JTextField();
		this.text_Zinssatz.addKeyListener(myKeyAdapter);

		JLabel lblNewLabel_1 = new JLabel("Zinssatz:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.fill = GridBagConstraints.VERTICAL;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 4;
		this.add(lblNewLabel_1, gbc_lblNewLabel_1);
		this.text_Zinssatz.setMargin(new Insets(2, 6, 2, 6));
		this.text_Zinssatz.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GridBagConstraints gbc_text_Zinssatz = new GridBagConstraints();
		gbc_text_Zinssatz.fill = GridBagConstraints.BOTH;
		gbc_text_Zinssatz.insets = new Insets(0, 0, 5, 5);
		gbc_text_Zinssatz.gridwidth = 2;
		gbc_text_Zinssatz.gridx = 2;
		gbc_text_Zinssatz.gridy = 4;
		this.add(this.text_Zinssatz, gbc_text_Zinssatz);
		this.text_Zinssatz.setColumns(10);

		this.text_EndKapital = new JTextField();
		this.text_EndKapital.addKeyListener(myKeyAdapter);

		this.text_Laufzeit = new JTextField();
		this.text_Laufzeit.addKeyListener(myKeyAdapter);

		this.radioButton_Laufzeit = new JRadioButton("");
		this.radioButton_Laufzeit.setMnemonic(KeyEvent.VK_1);
		this.radioButton_Laufzeit.addActionListener(this::OnRadioButton);
		this.radioButton_Laufzeit.setHorizontalAlignment(SwingConstants.CENTER);
		this.radioButton_Laufzeit.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.radioButton_Laufzeit.setFocusable(false);
		GridBagConstraints gbc_radioButton_Laufzeit = new GridBagConstraints();
		gbc_radioButton_Laufzeit.anchor = GridBagConstraints.WEST;
		gbc_radioButton_Laufzeit.fill = GridBagConstraints.VERTICAL;
		gbc_radioButton_Laufzeit.insets = new Insets(0, 0, 5, 5);
		gbc_radioButton_Laufzeit.gridx = 0;
		gbc_radioButton_Laufzeit.gridy = 6;
		this.add(this.radioButton_Laufzeit, gbc_radioButton_Laufzeit);
		group.add(this.radioButton_Laufzeit);

		JLabel lblLaufzeit = new JLabel("Laufzeit:");
		lblLaufzeit.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblLaufzeit = new GridBagConstraints();
		gbc_lblLaufzeit.anchor = GridBagConstraints.WEST;
		gbc_lblLaufzeit.fill = GridBagConstraints.VERTICAL;
		gbc_lblLaufzeit.insets = new Insets(0, 0, 5, 5);
		gbc_lblLaufzeit.gridx = 1;
		gbc_lblLaufzeit.gridy = 6;
		this.add(lblLaufzeit, gbc_lblLaufzeit);
		this.text_Laufzeit.setMargin(new Insets(2, 6, 2, 6));
		this.text_Laufzeit.setFont(new Font("Tahoma", Font.PLAIN, 13));
		this.text_Laufzeit.setColumns(10);
		GridBagConstraints gbc_text_Laufzeit = new GridBagConstraints();
		gbc_text_Laufzeit.fill = GridBagConstraints.HORIZONTAL;
		gbc_text_Laufzeit.anchor = GridBagConstraints.NORTH;
		gbc_text_Laufzeit.insets = new Insets(0, 0, 5, 5);
		gbc_text_Laufzeit.gridwidth = 2;
		gbc_text_Laufzeit.gridx = 2;
		gbc_text_Laufzeit.gridy = 6;
		this.add(this.text_Laufzeit, gbc_text_Laufzeit);

		this.radioButton_EndKapital = new JRadioButton("");
		this.radioButton_EndKapital.setMnemonic(KeyEvent.VK_2);
		this.radioButton_EndKapital.addActionListener(this::OnRadioButton);
		this.radioButton_EndKapital.setSelected(true);
		this.radioButton_EndKapital.setHorizontalAlignment(SwingConstants.CENTER);
		this.radioButton_EndKapital.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.radioButton_EndKapital.setFocusable(false);
		GridBagConstraints gbc_radioButton_EndKapital = new GridBagConstraints();
		gbc_radioButton_EndKapital.anchor = GridBagConstraints.WEST;
		gbc_radioButton_EndKapital.fill = GridBagConstraints.VERTICAL;
		gbc_radioButton_EndKapital.insets = new Insets(0, 0, 5, 5);
		gbc_radioButton_EndKapital.gridx = 0;
		gbc_radioButton_EndKapital.gridy = 8;
		this.add(this.radioButton_EndKapital, gbc_radioButton_EndKapital);
		group.add(this.radioButton_EndKapital);

		JLabel lblEndkapital = new JLabel("Endkapital:");
		lblEndkapital.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblEndkapital = new GridBagConstraints();
		gbc_lblEndkapital.anchor = GridBagConstraints.WEST;
		gbc_lblEndkapital.fill = GridBagConstraints.VERTICAL;
		gbc_lblEndkapital.insets = new Insets(0, 0, 5, 5);
		gbc_lblEndkapital.gridx = 1;
		gbc_lblEndkapital.gridy = 8;
		this.add(lblEndkapital, gbc_lblEndkapital);
		this.text_EndKapital.setFocusable(false);
		this.text_EndKapital.setEditable(false);
		this.text_EndKapital.setMargin(new Insets(2, 6, 2, 6));
		this.text_EndKapital.setFont(new Font("Tahoma", Font.PLAIN, 13));
		this.text_EndKapital.setColumns(10);
		GridBagConstraints gbc_text_EndKapital = new GridBagConstraints();
		gbc_text_EndKapital.fill = GridBagConstraints.BOTH;
		gbc_text_EndKapital.insets = new Insets(0, 0, 5, 5);
		gbc_text_EndKapital.gridwidth = 2;
		gbc_text_EndKapital.gridx = 2;
		gbc_text_EndKapital.gridy = 8;
		this.add(this.text_EndKapital, gbc_text_EndKapital);

		//this.btnBerechnen = new JButton_Berechnen("<html><U>B</U>erechnen</html>");
		this.btnBerechnen = new JButton_Berechnen("Berechnen");
		this.btnBerechnen.addKeyListener(myButtonKeyAdapter);
		this.btnBerechnen.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.btnBerechnen.setMnemonic('B');
		this.btnBerechnen.setMnemonic(KeyEvent.VK_B);
		GridBagConstraints gbc_btnBerechnen = new GridBagConstraints();
		gbc_btnBerechnen.gridwidth = 2;
		gbc_btnBerechnen.anchor = GridBagConstraints.NORTHWEST;
		gbc_btnBerechnen.insets = new Insets(0, 0, 0, 5);
		gbc_btnBerechnen.gridx = 0;
		gbc_btnBerechnen.gridy = 10;
		this.add(this.btnBerechnen, gbc_btnBerechnen);
		this.btnClear.setMnemonic('l');
		this.btnClear.setMnemonic(KeyEvent.VK_L);
		GridBagConstraints gbc_btnClear = new GridBagConstraints();
		gbc_btnClear.anchor = GridBagConstraints.NORTHEAST;
		gbc_btnClear.insets = new Insets(0, 0, 0, 5);
		gbc_btnClear.gridx = 3;
		gbc_btnClear.gridy = 10;
		this.add(this.btnClear, gbc_btnClear);

		this.btnBeenden = new JButton_Beenden("Beenden");
		this.btnBeenden.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.btnBeenden.addKeyListener(myButtonKeyAdapter);
		this.btnBeenden.setMnemonic('e');
		this.btnBeenden.setMnemonic(KeyEvent.VK_E);
		GridBagConstraints gbc_btnBeenden = new GridBagConstraints();
		gbc_btnBeenden.anchor = GridBagConstraints.NORTHEAST;
		gbc_btnBeenden.gridx = 4;
		gbc_btnBeenden.gridy = 10;
		this.add(this.btnBeenden, gbc_btnBeenden);

	}

	private void OnRadioButton(ActionEvent e)
	{
		if (this.radioButton_EndKapital.isSelected())
		{
			this.text_EndKapital.setText("");
			this.text_EndKapital.setEditable(false);
			this.text_EndKapital.setFocusable(false);
			this.text_Laufzeit.setFocusable(true);
			this.text_Laufzeit.setEditable(true);
		}
		else
		{
			this.text_Laufzeit.setText("");
			this.text_Laufzeit.setEditable(false);
			this.text_Laufzeit.setFocusable(false);
			this.text_EndKapital.setFocusable(true);
			this.text_EndKapital.setEditable(true);
		}
	}

	public boolean isCalculatingEndKapital()
	{
		return this.radioButton_EndKapital.isSelected();
	}

	public double getStartkapital()
	{
		try
		{
			return DecimalFormat.getInstance(Locale.GERMAN).parse(this.text_StartKapital.getText()).doubleValue();
		} catch (Exception e)
		{
			return 0.0;
		}
	}

	public double getZinssatz()
	{
		try
		{
			return DecimalFormat.getInstance(Locale.GERMAN).parse(this.text_Zinssatz.getText()).doubleValue();
		} catch (Exception e)
		{
			return 0;
		}
	}

	public double getLaufzeit()
	{
		try
		{
			return DecimalFormat.getInstance(Locale.GERMAN).parse(this.text_Laufzeit.getText()).doubleValue();
		} catch (Exception e)
		{
			return 0.0;
		}
	}

	public double getEndKapital()
	{
		try
		{
			return DecimalFormat.getInstance(Locale.GERMAN).parse(this.text_EndKapital.getText()).doubleValue();
		} catch (Exception e)
		{
			return 0.0;
		}
	}

	public void setLaufzeit(double value)
	{
		this.text_Laufzeit.setText(calc.getValueAsIntegerText(value));
	}

	public void setLaufzeit(String value)
	{
		this.text_Laufzeit.setText(value);
	}

	public void setEndKapital(double value)
	{
		this.text_EndKapital.setText(calc.getValueAsCurrenyText(value));
	}

	public void setEndKapital(String value)
	{
		this.text_EndKapital.setText(value);
	}

	public void setVerlauf(String text)
	{
		this.text_Verlauf.setText(text);
	}
	
	public void setTableContent()
	{
		//this.table.
	}

	public void clear()
	{
		this.text_Laufzeit.setText("");
		this.text_EndKapital.setText("");
		this.text_StartKapital.setText("");
		this.text_Zinssatz.setText("");
		this.text_Verlauf.setText("");
	}

	public void OnKeyTyped(KeyEvent e)
	{
		if (e.getKeyChar() == '\n')
		{
			System.out.println("Enter");
			e.consume();
		}
		else
			System.out.println(e.getKeyChar());
	}
}
