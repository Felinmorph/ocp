package gui;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class FrameMain extends JFrame
{
	JPanel PanelMain;

	public FrameMain()
	{
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(FrameMain.class.getResource("/img/zinsrechner.png")));
//		try	{
//			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
//		} catch (Exception e) {	}	// try to set look and feel but dont cry if it dosnt work...

		this.init();

		this.pack();
		this.setVisible(true);
	}

	private void init()
	{
		this.setTitle("Zinsrechner");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(340, 300));

		//this.PanelMain = new Panel_Main();
		
		//this.setContentPane(this.PanelMain);

	}
	
	public void setMainPanel(JPanel panel)
	{
		this.PanelMain = panel;
		this.setContentPane(this.PanelMain);

		Dimension dimLI = panel.getPreferredSize();
		Dimension dimFM = new Dimension(dimLI.width + 20, dimLI.height + 40);
		this.setPreferredSize(dimFM);
		this.pack();
		this.setVisible(true);
		
	}




}
