package control;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import gui.FrameMain;
import gui.JButton_Beenden;
import gui.JButton_Berechnen;
import gui.JButton_Loeschen;
import gui.Panel_Main;
import logic.calc;

public class Controller
{
	private FrameMain frameMain;
	private Panel_Main panelMain;

	public Controller()
	{
		this.init();
	}

	private void init()
	{
		System.out.println("Starting...");

		this.frameMain = new FrameMain();
		this.panelMain = new Panel_Main();
		this.frameMain.setMainPanel(this.panelMain);

		this.rigUpUI();

	}

	private void rigUpUI()
	{
		this.traverseUI(this.panelMain);

	}

	private void traverseUI(Component rootComp)
	{
		for (Component child : ((Container) rootComp).getComponents())
		{
			//System.out.println(child.getClass().getSimpleName());
			if (child instanceof JButton_Berechnen)
				((JButton) child).addActionListener(this::onClickButtonBerechne);
			if (child instanceof JButton_Loeschen)
				((JButton) child).addActionListener(this::onClickButtonLoesche);
			if (child instanceof JButton_Beenden)
				((JButton) child).addActionListener(this::onClickButtonBeenden);

			this.traverseUI(child);
		}
	}

	public void onClickButtonBerechne(ActionEvent e)
	{
		//this.frameMain.setInnerPanel(this.panelOptions);
		System.out.println("berechnen");
		double startkapital = this.panelMain.getStartkapital();
		double zinssatz = this.panelMain.getZinssatz();
		double laufzeit = this.panelMain.getLaufzeit();
		double endKapital = this.panelMain.getEndKapital();
		if (this.panelMain.isCalculatingEndKapital())
		{
			endKapital = calc.berechneEndkapital(startkapital, zinssatz, laufzeit);
			this.panelMain.setEndKapital(endKapital);
			
			int l = calc.getValueAsCurrenyText(endKapital).length();
			StringBuilder sb = new StringBuilder();
			sb.append(" Jahr | Kapital \n");
			sb.append("------|");
			for (int i = 0; i < l+4-2; i++)
				sb.append("-");
			sb.append("\n");
			//sb.append("    1 | 1.234.567,8901 €");
			for (int i = 1; i <= laufzeit; i++)
			{
				double v = calc.berechneEndkapital(startkapital, zinssatz, i);
				String out = String.format(" %4d | %,"+(l-2)+".2f €%n", i,v);
				sb.append(out);
			}
			this.panelMain.setVerlauf(sb.toString());
		}
		else
		{
			laufzeit = calc.berechneLaufzeit(startkapital, zinssatz, endKapital);
			this.panelMain.setLaufzeit(laufzeit);
			StringBuilder sb = new StringBuilder();
			sb.append(" Jahr | Kapital \n");
			sb.append("------|-------------------\n");
			//sb.append("    1 | 1.234.567,8901 €");
			for (int i = 1; i <= laufzeit; i++)
			{
				double v = calc.berechneEndkapital(startkapital, zinssatz, i);
				String out = String.format(" %4d | %,15.2f €%n", i,v);
				sb.append(out);
			}
			if(laufzeit > (int)laufzeit)
				sb.append("------|-------------------\n");
				sb.append(String.format(" %4.1f | %,15.2f €%n", laufzeit ,endKapital));
			this.panelMain.setVerlauf(sb.toString());
		}

	}

	public void onClickButtonLoesche(ActionEvent e)
	{
		//this.frameMain.setInnerPanel(this.panelOptions);
		System.out.println("löschen");
		this.panelMain.clear();
	}

	public void onClickButtonBeenden(ActionEvent e)
	{
		//this.frameMain.setInnerPanel(this.panelOptions);
		System.out.println("beenden");
		int dialogResult = JOptionPane.showConfirmDialog(null, "Wirklich Beenden", "Zinsrechner", JOptionPane.YES_NO_OPTION);
		if (dialogResult == JOptionPane.YES_NO_OPTION)
			System.exit(0);
	}
}
