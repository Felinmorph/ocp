package logic;

public class calc
{

	public calc()
	{
	}

	public static double berechneEndkapital(double startkapitaal, double zinssatz, double laufzeitJahre ) 
	{
		return startkapitaal*Math.pow((1.0+zinssatz/100.0),laufzeitJahre);
	}
	
	public static double berechneLaufzeit(double startkapital, double zinssatz, double endKapital ) 
	{
		return Math.log(endKapital/startkapital)/Math.log(1.0+zinssatz/100.0);
	}
	
	public static String getValueAsCurrenyText(double value)
	{
		return String.format("%,.2f", value);
//		NumberFormat nf = NumberFormat.getCurrencyInstance();
//		return nf.format(value);
	}
	public static String getValueAsIntegerText(double value)
	{
		return String.format("%,f", value);
	}
	public static String getValueAsDoubleText(double value,int digits)
	{
		return String.format("%,."+digits+"f", value);
	}	
}
