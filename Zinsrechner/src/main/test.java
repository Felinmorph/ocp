package main;

import java.text.NumberFormat;

import logic.calc;

public class test
{

	public static void main(String[] args)
	{
		double startkapitaal = 1000.0;
		double zinssatz = 5;
		int laufzeitJahre = 5;
		
		double endKapital = calc.berechneEndkapital(startkapitaal, zinssatz, laufzeitJahre);
		System.out.printf("endkapital = %,.2f€%n", endKapital);
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		System.out.println("enfkapital = " + nf.format(endKapital));
		
		startkapitaal = 1000.0;
		zinssatz = 5;
		endKapital = 2000;
		double laufzeit = calc.berechneLaufzeit(startkapitaal, zinssatz, endKapital);
		System.out.printf("endkapital = %f%n", laufzeit);

	}

}
