package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class DB_Connector
{
	String url = "jdbc:mysql://localhost:3306/world?serverTimezone=Europe/Berlin";
	String userName = "root";
	String password = "12345";
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	ResultSetMetaData rsmd = null;
	SQLException lastSQLException = null;
	String lastResult;
	


	public DB_Connector()
	{
	}
	public DB_Connector(String userName, String password)
	{
		this.userName=userName;
		this.password = password;
	}
	
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}
	public SQLException getLastSQLException()
	{
		return this.lastSQLException;
	}
	
	
	public boolean openConnection()
	{
		try
		{
			this.conn = DriverManager.getConnection(this.url, this.userName, this.password);
			this.stmt = this.conn.createStatement(); 
			System.out.println("Verbindung erfolgreich!");
			return true;
		} 
		catch (SQLException e)
		{
			System.out.println("Error Code: " + e.getErrorCode() + ". " + e.getMessage());
			//e.printStackTrace();
			this.lastSQLException=e;
			return false;
		}
	}
	
	public void closeConnection()
	{
		if (this.conn == null)
			return;
		try
		{
			this.conn.close();
			this.conn = null;
			System.out.println("Verbindung geschlossen!");
		} 
		catch (SQLException e)
		{
			System.out.println("Error Code: " + e.getErrorCode() + ". " + e.getMessage());
			this.lastSQLException=e;
		}
	}
	
	public void execute(String sql)
	{
		StringBuilder sb = new StringBuilder();
		int n=0;
		try
		{
			if (this.stmt.execute(sql))
			{
				this.rs = this.stmt.getResultSet();
				this.rsmd = this.rs.getMetaData();
				
				int[] spaltenbreiten = new int[this.rsmd.getColumnCount() + 1];

				for (int i = 1; i <= this.rsmd.getColumnCount(); i++)
				{
					int breite = this.rsmd.getColumnDisplaySize(i);
					String title = this.rsmd.getColumnName(i);
					if (title.length() > breite)
					{
						breite = title.length();
					}
					spaltenbreiten[i] = breite;
					sb.append(String.format("%-" + breite + "s | ", title));
					
				}
				sb.append("\r\n");
				for (int i = 1; i < spaltenbreiten.length; i++)
				{
					if (i > 1)
						sb.append("-");
					for (int j = 0; j < spaltenbreiten[i]; j++)
					{
						sb.append("-");
					}
					sb.append("-|");
				}
				sb.append("\r\n");
				while (this.rs.next())
				{
					for (int i = 1; i <= this.rsmd.getColumnCount(); i++)
					{
						sb.append(String.format("%-" + spaltenbreiten[i] + "s | ", this.rs.getString(i)));
					}
					sb.append("\r\n");

					//System.out.printf("%-20s (%,6.0fk)%n", name, pop/1000.0);
					n++;
				}
			}
			else
			{
				n = this.stmt.getUpdateCount();
			}
			sb.append("\r\n");
			sb.append("--> ");
			sb.append(n);
			sb.append(" ergebnisse.");
		} catch (SQLException e)
		{
			System.out.println("Error Code: " + e.getErrorCode() + ". " + e.getMessage());
			sb.append("Error Code: ");
			sb.append(e.getErrorCode()); 
			sb.append(". ");
			sb.append(e.getMessage());
			this.lastSQLException=e;
		}
		

		this.lastResult = sb.toString();
	}
	
	public String getresult()
	{
		return this.lastResult;
	}
	
	
}
