package main;

import java.awt.EventQueue;

import gui.FrameMain;

public class _Start
{

	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run()
			{
				try
				{
					new FrameMain(new DB_Connector());
//					FrameMain frame = new FrameMain();
//					frame.setVisible(true);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

}
