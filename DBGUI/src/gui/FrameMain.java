package gui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;

import main.DB_Connector;

@SuppressWarnings("serial")
public class FrameMain extends JFrame
{
	Panel_DB panelDB;
	Panel_Login panelLogIn;
	private DB_Connector dbconn;

	public FrameMain(DB_Connector dbconn)
	{
		this.dbconn = dbconn;
		this.panelLogIn = new Panel_Login();
		this.panelDB = new Panel_DB();
		this.init();

		this.pack();
		this.setVisible(true);
	}

	private void init()
	{
		this.setTitle("DB-GUI");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(this.panelLogIn);
		Dimension dimLI = this.panelLogIn.getPreferredSize();
		Dimension dimFM = new Dimension(dimLI.width + 10, dimLI.height + 30);
		this.setPreferredSize(dimFM);
		this.init_LogInPanel();
		this.init_DBPanel();

	}

	private void init_LogInPanel()
	{
		for (Component child : this.panelLogIn.getComponents())
		{
			if (child instanceof JButton)
			{
				System.out.println("Login-Button found!");
				((JButton) child).addActionListener(this::OnLogIn);
			}
		}
	}

	private void init_DBPanel()
	{
		this.traverseDBPanel(this.panelDB);
//		for (Component child : this.panelDB.getComponents())
//		{
//			if (child instanceof JButton)
//			{
//				JButton btn = (JButton) child; 
//				System.out.println("Found!");
//				btn.addActionListener(this::OnDBGO);
//			}
//		}
	}

	private void traverseDBPanel(Component rootComp)
	{
		for (Component child : ((Container) rootComp).getComponents())
		{
			if (child instanceof JButton)
			{
				JButton btn = (JButton) child;
				if (btn.getText() == "Go")
				{
					System.out.println("Found!");
					btn.addActionListener(this::OnDBGO);
				}
			}
			this.traverseDBPanel(child);
		}
	}

	private void OnLogIn(ActionEvent e)
	{
		System.out.println("Login-Button clicked!");
		System.out.println("Setting credentials...");
		this.dbconn.setUserName(this.panelLogIn.getUserName());
		this.dbconn.setPassword(this.panelLogIn.getPWD());
		System.out.println("Trying connection...");
		if (this.dbconn.openConnection())
		{
			System.out.println("Connection successfull!");

			this.setContentPane(this.panelDB);
			Dimension dimDB = this.panelDB.getPreferredSize();
			Dimension dimFM = new Dimension(dimDB.width + 20, dimDB.height + 40);
			//Dimension dimFM = new Dimension(820, 540);
			this.setPreferredSize(dimFM);
			this.setSize(dimFM);
			this.pack();
			this.setVisible(true);
		}
		else
		{
			System.out.println("Error!");
			this.panelLogIn.setErrorText("Benutzername oder Passwort falsch!");
		}
	}

	private void OnDBGO(ActionEvent e)
	{
		System.out.println("GO-Button clicked!");
//		String db = panelDB.getDB();
		String sql = this.panelDB.getSQLCommand();
		this.dbconn.execute(sql);
		this.panelDB.setResultText(this.dbconn.getresult());

	}

}
