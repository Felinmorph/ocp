package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class Panel_DB extends JPanel
{
	private JPanel panel;
	private JButton btn_GO;
	private JLabel lblNewLabel;
	private JTextField textField_SQL;
	private JLabel lblNewLabel_1;
	private JComboBox<String> comboBox_DB;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_3;
	private JScrollPane scrollPane;
	private JTextArea txtr_Result;

	/**
	 * Create the panel.
	 */
	public Panel_DB()
	{
		this.setLayout(new BorderLayout(0, 0));
		this.setBounds(0, 0, 800, 500);
		this.setPreferredSize(new Dimension(800,500));
		
		JLabel lblNewLabel_2 = new JLabel("Datenbank-Abfrage");
		lblNewLabel_2.setBorder(new CompoundBorder(new EmptyBorder(10, 10, 10, 10), new LineBorder(new Color(0, 0, 0))));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		this.add(lblNewLabel_2, BorderLayout.NORTH);
		
		this.panel = new JPanel();
		this.panel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.add(this.panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{5, 100, 673, 5, 0};
		gbl_panel.rowHeights = new int[]{5, 20, 23, 358, 5, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		this.panel.setLayout(gbl_panel);
		
		this.panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 5);
		gbc_panel_1.fill = GridBagConstraints.VERTICAL;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 0;
		this.panel.add(this.panel_1, gbc_panel_1);
		
		this.lblNewLabel_1 = new JLabel("Datenbank:");
		this.lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 0;
		this.panel.add(this.lblNewLabel_1, gbc_lblNewLabel_1);
		
		this.comboBox_DB = new JComboBox<String>();
		this.comboBox_DB.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.comboBox_DB.setModel(new DefaultComboBoxModel<String>(new String[] {"world"}));
		GridBagConstraints gbc_comboBox_DB = new GridBagConstraints();
		gbc_comboBox_DB.anchor = GridBagConstraints.WEST;
		gbc_comboBox_DB.fill = GridBagConstraints.VERTICAL;
		gbc_comboBox_DB.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_DB.gridx = 2;
		gbc_comboBox_DB.gridy = 0;
		this.panel.add(this.comboBox_DB, gbc_comboBox_DB);
		
		this.panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(0, 0, 5, 0);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 3;
		gbc_panel_2.gridy = 0;
		this.panel.add(this.panel_2, gbc_panel_2);
		
		this.lblNewLabel = new JLabel("SQL-Befehl:");
		this.lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 1;
		this.panel.add(this.lblNewLabel, gbc_lblNewLabel);
		
		this.textField_SQL = new JTextField();
		this.textField_SQL.setFont(new Font("Lucida Console", Font.PLAIN, 14));
		this.textField_SQL.setText("Select * from country");
		GridBagConstraints gbc_textField_SQL = new GridBagConstraints();
		gbc_textField_SQL.fill = GridBagConstraints.BOTH;
		gbc_textField_SQL.insets = new Insets(0, 0, 5, 5);
		gbc_textField_SQL.gridx = 2;
		gbc_textField_SQL.gridy = 1;
		this.panel.add(this.textField_SQL, gbc_textField_SQL);
		this.textField_SQL.setColumns(10);
		
		this.btn_GO = new JButton("Go");
		GridBagConstraints gbc_btn_GO = new GridBagConstraints();
		gbc_btn_GO.fill = GridBagConstraints.BOTH;
		gbc_btn_GO.insets = new Insets(0, 0, 5, 5);
		gbc_btn_GO.gridwidth = 2;
		gbc_btn_GO.gridx = 1;
		gbc_btn_GO.gridy = 2;
		this.panel.add(this.btn_GO, gbc_btn_GO);
		
		this.scrollPane = new JScrollPane();
		this.scrollPane.setBorder(new LineBorder(new Color(130, 135, 144)));
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 2;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 3;
		this.panel.add(this.scrollPane, gbc_scrollPane);
		
		this.txtr_Result = new JTextArea();
		this.txtr_Result.setFont(new Font("Lucida Console", Font.PLAIN, 13));
		this.txtr_Result.setMargin(new Insets(6, 6, 6, 6));
		this.txtr_Result.setText("Result");
		this.scrollPane.setViewportView(this.txtr_Result);
		
		this.panel_3 = new JPanel();
		this.panel_3.setMinimumSize(new Dimension(5, 5));
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.insets = new Insets(0, 0, 0, 5);
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 2;
		gbc_panel_3.gridy = 4;
		this.panel.add(this.panel_3, gbc_panel_3);

	}

	public String getSQLCommand()
	{
		return this.textField_SQL.getText();
	}
	public String getDB()
	{
		return this.comboBox_DB.getModel().getElementAt(0).toString();
	}
	public void setResultText(String text)
	{
		this.txtr_Result.setText(text);
	}
}
