package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class Panel_Login extends JPanel
{
	private JTextField txtRoot;
	private JTextField textField_PWD;
	private JButton btn_Login;
	private JLabel lbl_Error;

	/**
	 * Create the panel.
	 */
	public Panel_Login()
	{
		this.setLayout(null);
		this.setBounds(0, 0, 300, 180);
		this.setPreferredSize(new Dimension(300,180));
		
		this.btn_Login = new JButton("Login");
		this.btn_Login.setBounds(105, 140, 89, 23);
		this.add(this.btn_Login);
		
		JLabel lblNewLabel = new JLabel("Benutzername:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(10, 60, 120, 20);
		this.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Passwort:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(10, 90, 120, 20);
		this.add(lblNewLabel_1);
		
		this.txtRoot = new JTextField();
		this.txtRoot.setMargin(new Insets(2, 6, 2, 6));
		this.txtRoot.setText("root");
		this.txtRoot.setFont(new Font("Tahoma", Font.PLAIN, 13));
		this.txtRoot.setBounds(140, 60, 144, 20);
		this.add(this.txtRoot);
		this.txtRoot.setColumns(10);
		
		this.textField_PWD = new JTextField();
		this.textField_PWD.setMargin(new Insets(2, 6, 2, 6));
		this.textField_PWD.setText("12345");
		this.textField_PWD.setFont(new Font("Tahoma", Font.PLAIN, 13));
		this.textField_PWD.setBounds(140, 90, 144, 20);
		this.add(this.textField_PWD);
		this.textField_PWD.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Datenbank-Login");
		lblNewLabel_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel_2.setBounds(10, 10, 274, 30);
		this.add(lblNewLabel_2);
		
		this.lbl_Error = new JLabel("ERROR !!!");
		this.lbl_Error.setVisible(false);
		this.lbl_Error.setHorizontalAlignment(SwingConstants.CENTER);
		this.lbl_Error.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.lbl_Error.setForeground(Color.RED);
		this.lbl_Error.setBounds(10, 115, 274, 20);
		this.add(this.lbl_Error);

	}

	public String getUserName()
	{
		return this.txtRoot.getText();
	}
	public String getPWD()
	{
		return this.textField_PWD.getText();
	}
	
	public void setErrorText(String errorMsg)
	{
		this.lbl_Error.setText(errorMsg);
		this.lbl_Error.setVisible(true);
	}
	public void clearErrorText()
	{
		this.lbl_Error.setText("");
		this.lbl_Error.setVisible(false);
	}

}
