package aufgaben_2019_07_19;

import java.util.Random;

public class ArrayUtils
{

	
	public static int[] createRandomIntArray(int maxLen, int minVal, int maxVal)
	{
		if (maxLen < 1)
			maxLen =1;
		if (minVal > maxVal)
		{
			int tmp = minVal;
			minVal=maxVal;
			maxVal=tmp;
		}
		int len = (int)(Math.random()*maxLen)+1;
		
		int erg[] = new int[len];
		Random r = new Random();
		for (int i = 0; i < len; i++)
		{
			erg[i] = r.nextInt( maxVal-minVal ) + minVal;
		}
		
		return erg;
	}
}
