package aufgaben_2019_07_19.serialisierung;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class _Main
{

	public static void main(String[] args)
	{
		TestKlasse t = new TestKlasse(7);
		t.b = 5;
		System.out.println("t(2)  = " + t.getValue(3));
		
		saveTestKlasse(t, "testklasse.bin");
		
		t.b = 7;
		
		TestKlasse t2 = loadTestKlasse("testklasse.bin");
		System.out.println("t2(2) = " + t2.getValue(3));
		System.out.println("b="+t.b);
	}
	
	

	private static void saveTestKlasse(TestKlasse o, String filepath)
	{
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filepath)))
		{
			oos.writeObject(o);
		}
		catch (Exception e) {}
	}
	private static TestKlasse loadTestKlasse(String filepath)
	{
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filepath)))
		{
			return (TestKlasse)ois.readObject();
		}
		catch (Exception e) {}
		return null;
	}


	
	
	
}
