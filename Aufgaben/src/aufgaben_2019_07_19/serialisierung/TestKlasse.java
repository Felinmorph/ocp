package aufgaben_2019_07_19.serialisierung;

import java.io.Serializable;

class TestHilfsKlasse implements Serializable
{
	private static final long serialVersionUID = -2020592942886579720L;
	
	private int a;
	private String b;
	public TestHilfsKlasse(int a, String b)
	{
		this.a = a;
		this.b = b;
	}
	@Override
	public String toString()
	{
		return this.b +"_"+ this.a;
	}
}


public class TestKlasse implements Serializable
{
	private static final long serialVersionUID = 8151519477871035516L;
	
	public static int b;
	private int anz;
	private TestHilfsKlasse[] werte;
	
	public TestKlasse(int anz)
	{
		this.werte = new TestHilfsKlasse[anz];
		for (int i = 0; i < anz; i++)
		{
			this.werte[i] = new TestHilfsKlasse(i, "Test");
		}
	}
	public String getValue(int index)
	{
		if (index < 0)
			index=0;
		
		if (index > this.anz)
			index=this.anz;

		return this.werte[index].toString();
	}

}
