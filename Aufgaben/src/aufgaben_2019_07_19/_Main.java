package aufgaben_2019_07_19;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.Arrays;

public class _Main
{

	public static void main(String[] args)
	{
		int[] arr1 = ArrayUtils.createRandomIntArray(10, -20, 20);
		
		System.out.println("->" +Arrays.toString(arr1));
		saveArray(arr1, "randomarray.txt");
		int[] arr2 = loadArray("randomarray.txt");
		System.out.println("->" +Arrays.toString(arr2));
		
		saveBinArray(arr1, "randomarray.bin");
		int[] arr3 = loadBinArray("randomarray.bin");
		System.out.println("->" +Arrays.toString(arr3));
	}
	
	

	private static int[] loadArray(String filepath)
	{
		int[] arr = null;
		try(BufferedReader in = new BufferedReader(new FileReader(filepath)))
		{
			String sLen = in.readLine();
			int len = Integer.parseInt(sLen);
			arr = new int[len];
			for (int i = 0; i < len; i++)
			{
				arr[i] = Integer.parseInt(in.readLine());
			}
		} catch (Exception e){}
		return arr;
	}

	private static void saveArray(int[] arr, String filepath)
	{
		try(PrintWriter out = new PrintWriter(filepath))
		{
			out.println(arr.length);
			for (int i = 0; i < arr.length; i++)
			{
				out.println(arr[i]);
			}
		}
		catch (Exception e) {}
	}
	
	private static void saveBinArray(int[] arr, String filepath)
	{
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filepath)))
		{
			oos.writeObject(arr);
		}
		catch (Exception e) {}
	}
	private static int[] loadBinArray(String filepath)
	{
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filepath)))
		{
			return (int[])ois.readObject();
		}
		catch (Exception e) {}
		return null;
	}


	
	
	
}
