package aufgaben_2019_06_17;

enum Size {SMALL,NORMAL,BIG;}


public class Tmp {

	public static void main(String[] args) {

		Size s1 = Size.SMALL;
		
		Comparable<Size> c1 = Size.BIG;
		
		int result = s1.compareTo(Size.NORMAL);
		System.out.println("result = " + result); // result = -1
		
		//SMALL
		//NORMAL
		//BIG
		for(Size s : Size.values()) {
			System.out.println(s);
		}
		
	}

}
