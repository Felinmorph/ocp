package aufgaben_2019_06_17;

public enum Hunderassen
{
	DACKEL(0.5), COLLIE(1.0), DOGGE(1.5);

	private final double maxGroesse;

	private Hunderassen(double maxSize)
	{
		this.maxGroesse = maxSize;
	}

	public double getMaxGroesse()
	{
		return this.maxGroesse;
	}
	
	@Override
	public String toString()
	{
		return (this.name().charAt(0) + this.name().substring(1).toLowerCase() + ", max. Größe: " + this.getMaxGroesse());
	}
}
