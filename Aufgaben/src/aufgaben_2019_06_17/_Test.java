package aufgaben_2019_06_17;

import java.util.Arrays;

import aufgaben_2019_06_14.MyComperator;
import aufgaben_2019_06_14.Person;

public class _Test
{

	public static void main(String[] args)
	{
		Person[] leute = new Person[6];
		leute[0] = new Person("Mueller", "Heinz");
		leute[1] = new Person("Brecht", "Bathelomaeus");
		leute[2] = new Person("Mueller", "Gertrude");
		leute[3] = new Person("Schulze", "Torben");
		leute[4] = new Person("Duerrer", "Jens");
		leute[5] = new Person("Xerxes", "Willhelm");
		
		Person toBeSearched = new Person("Duerrer", "Jens");

		System.out.print("Unsortiert:         ");
		System.out.println(Arrays.toString(leute));
		System.out.println();
		
		int n = 0;
		
		Arrays.sort(leute);
		System.out.print("nach sort:          ");
		System.out.println(Arrays.toString(leute));
		n = Arrays.binarySearch(leute,toBeSearched);
		System.out.println("fundort der Person: " + n);
		System.out.println();

		
		Arrays.sort(leute, new MyComperator());
		System.out.print("nach MyComperator:  ");
		System.out.println(Arrays.toString(leute));
		n = Arrays.binarySearch(leute,toBeSearched);
		System.out.println("fundort der Person: " + n);
		System.out.println();

		System.out.println("---------------------------------------------");
		System.out.println("");
		System.out.println("Alle Hunde mir maximaler groesse:");
		for (Hunderassen hund : Hunderassen.values())
		{
			System.out.println(hund);
		}
		
		
		
	}

}
