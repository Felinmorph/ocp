package aufgaben_2019_07_26_localisation;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

//import java.time.ZoneId;
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;

public class _Main
{

	public static void main(String[] args)
	{
//		Map<String,List<String>> out =
//		ZoneId
//		.getAvailableZoneIds()
//		.stream()
//		//.sorted(Comparator.naturalOrder())
//		.collect(Collectors.groupingBy( (String x)->x.contains("/")?x.substring(0, x.indexOf('/')):"<Special>") );
//
//		for (String k : out.keySet())
//		{
//			System.out.printf("%-10s -> (%2d) -> %s%n",k,out.get(k).size(), out.get(k));
//		}

		LocalDate ld = LocalDate.now();

		System.out.println("| Sprache              | Land | Tagesname  | Monatsname           |");
		System.out.println("|----------------------|------|------------|----------------------|");
		for (Locale loc : Locale.getAvailableLocales())
		{
			String s1 = loc.getDisplayLanguage();
			String s2 = loc.getCountry();
			String s3 = ld.format(DateTimeFormatter.ofPattern("EEEE", loc));
			String s4 = ld.format(DateTimeFormatter.ofPattern("MMMM", loc));
			System.out.printf("| %20s | %4s | %10s | %20s |%n", s1, s2, s3, s4);
		}

	}

}
