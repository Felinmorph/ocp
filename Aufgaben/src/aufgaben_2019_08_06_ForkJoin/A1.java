package aufgaben_2019_08_06_ForkJoin;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

@SuppressWarnings("serial")
class ListWalker extends RecursiveAction
{

	private List<Integer> li;

	public ListWalker(List<Integer> li)
	{
		this.li = li;
	}

	@Override
	protected void compute()
	{
		int l =this.li.size(); 
		if (l < 3)
		{
			for (int i = 0; i < l; i++)
			{
				if (this.li.get(i)<0)
				{
					this.li.set(i, 0);
				}
			}
		}
		else
		{
			List<Integer> li1 = this.li.subList(0, l/2);
			List<Integer> li2 = this.li.subList(l/2, l);

			ListWalker task1 = new ListWalker(li1);
			ListWalker task2 = new ListWalker(li2);

			// und an den ForkJoin zum Ausführen übergeben: 
			invokeAll(task1, task2);
		}
	}
}

public class A1
{

	public static void main(String[] args)
	{
		System.out.println("========== main Start ==========");

		List<Integer> li = Arrays.asList(1, -7, 22, 0, -5, 8);
		System.out.println("vorher:  " + li);
		
		ForkJoinPool pool = new ForkJoinPool();
		RecursiveAction ra = new ListWalker(li);
		pool.invoke(ra);

		System.out.println("nachher: " + li);

		
		

		System.out.println("========== main end ==========");
	}

}
