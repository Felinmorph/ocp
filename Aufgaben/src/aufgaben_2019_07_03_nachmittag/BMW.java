package aufgaben_2019_07_03_nachmittag;

public class BMW extends Auto
{

	public BMW(String modell, int baujahr)
	{
		super(modell,baujahr);
	}
	
	public void setBaujahr(int baujahr)
	{
		super.baujahr = baujahr;
	}

}
