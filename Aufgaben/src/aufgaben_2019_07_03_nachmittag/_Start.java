package aufgaben_2019_07_03_nachmittag;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.TreeSet;

public class _Start
{

	public static void main(String[] args)
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A1 + A2
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== A1,A2 ==========");
		Auto a1 = new VW("Golf", 1990);
		Auto a2 = new BMW("Z4", 2000);

		System.out.println(a1);
		System.out.println(a2);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A3
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== A3 ==========");
		VW vw1 = new VW("Golf1", 1980);
		VW vw2 = new VW("Golf2", 1990);
		VW vw3 = new VW("Golf3", 1998);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A4
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== A4 ==========");
		LinkedList<VW> ll = new LinkedList<>();
		HashSet<VW> hs = new HashSet<>();
		TreeSet<VW> ts = new TreeSet<>();
		PriorityQueue<VW> pq = new PriorityQueue<>();

		ll.add(vw3);
		ll.add(vw2);
		ll.add(vw1);
		hs.add(vw3);
		hs.add(vw2);
		hs.add(vw1);
		ts.add(vw3);
		ts.add(vw2);
		ts.add(vw1);
		pq.add(vw3);
		pq.add(vw2);
		pq.add(vw1);

		System.out.println("LinkedList    : " + ll);
		System.out.println("HashSet       : " + hs);
		System.out.println("TreeSet       : " + ts);
		System.out.println("PriorityQueue : " + pq);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A5
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== A5 ==========");
		System.out.println("Ausgabe per foreach");
		System.out.println("* LinkedList:");
		print(ll,"\t","",true);
		System.out.println("* HashSet:");
		print(hs,"\t","",true);
		System.out.println("* TreeSet:");
		print(ts,"\t","",true);
		System.out.println("* PriorityQueue:");
		print(pq,"\t","",true);
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A6
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== A6 ==========");
		BMW bmw1 = new BMW("Z1", 1850);
		BMW bmw2 = new BMW("Z2", 1930);
		ArrayList<BMW> al = new ArrayList<>();
		al.add(bmw1);
		al.add(bmw2);
		
		HashSet<BMW> hs2 = new HashSet<>(al);
		TreeSet<BMW> ts2 = new TreeSet<>(al);
		ArrayDeque<BMW> ad = new ArrayDeque<>(al);

		System.out.println("LinkedList    : " + ll);
		System.out.println("HashSet       : " + hs);
		System.out.println("TreeSet       : " + ts);
		System.out.println("PriorityQueue : " + pq);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A7
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== A7 ==========");
		System.out.println("hs2.contains(bmw1) -> " + hs2.contains(bmw1));

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A8
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== A8 ==========");
		System.out.println("changing bmw1.baujahr to 1500");
		bmw1.setBaujahr(1500);
		System.out.println("hs2.contains(bmw1) -> " + hs2.contains(bmw1));
		System.out.println("-> false weil der hashcode gecached ist und nach dem aendern sich dieser geaendert hat...");

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A9
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== A9 ==========");
		ll.add(new VW("Polo", 2200));
		hs.add(new VW("Polo", 2200));
		ts.add(new VW("Polo", 2200));
		pq.add(new VW("Polo", 2200));
		System.out.println("LinkedList    : " + ll);
		System.out.println("HashSet       : " + hs);
		System.out.println("TreeSet       : " + ts);
		System.out.println("PriorityQueue : " + pq);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A10
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== A10 ==========");
		System.out.println("Collections.binarySearch(ll, new VW(\"Polo\",2200)) -> " + Collections.binarySearch(ll, new VW("Polo", 2200)));

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A11
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== A11 ==========");
		Collections.sort(ll);
		System.out.println("LinkedList (sorted): " + ll);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A12
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== A12 ==========");
		Collections.sort(ll, Collections.reverseOrder());
		//Collections.sort(ll, Comparator.reverseOrder());
		System.out.println("LinkedList (sorted - reverse): " + ll);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A13
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== A13 ==========");
		System.out.println("Collections.binarySearch(ll, new VW(\"Polo\",2200)) -> " + Collections.binarySearch(ll, new VW("Polo", 2200)));
		System.out.println("ergebnis ist fuer nicht korrekt sortierte listen nicht definiert...");

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A43
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== A14 ==========");
		System.out.println("Collections.binarySearch(ll, new VW(\"Polo\",3300)) -> " + Collections.binarySearch(ll, new VW("Polo", 3300)));
		System.out.println("element existiert nicht -> negatives ergebnis");
		
	}

	public static void print(Collection<? extends Auto> autos, String prefix, String suffix, boolean withIndex)
	{
		int i = 0;
		int n = 1 + autos.size()/10; 
		String indexString = "";
		
		for (Auto auto : autos)
		{
			if (withIndex)
			{
				indexString = String.format("%"+n+"d.: ", i);
			}
			System.out.println(prefix +indexString +auto + suffix);
			i++;
		}
	}
}
