package aufgaben_2019_07_03_nachmittag;

public abstract class Auto implements Comparable<Auto>
{
	String modell;
	int baujahr;

	public Auto(String modell, int baujahr)
	{
		if (modell == null || modell == "")
		{
			//throw new IllegalArgumentException("modell must not be null or empty!");
			modell = "-";
		}
		this.modell = modell;
		this.baujahr = baujahr;
	}

	@Override
	public String toString()
	{
		return this.getClass().getSimpleName() + ". Modell: " + this.modell + ", Baujahr:" + this.baujahr;
	}

	@Override
	public int compareTo(Auto o)
	{
		int res = this.modell.compareTo(o.modell);
		if (res == 0)
			res = this.baujahr - o.baujahr;
		return res;
	}

	@Override
	public int hashCode()
	{
		// erstes zeichen + letztes zeichen + baujahr
		int result = ((byte) this.modell.charAt(0) << 24);
		result += ((byte) this.modell.charAt(this.modell.length() - 1) << 16);
		result += this.baujahr;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		Auto other = (Auto) obj;
		if (this.baujahr != other.baujahr)
			return false;
		if (this.modell == null)
		{
			if (other.modell != null)
				return false;
		}
		else if (!this.modell.equals(other.modell))
			return false;
		return true;
	}

}
