package aufgaben_2019_07_10;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

class Tier
{
	int alter;

	public Tier(int alter)
	{
		super();
		this.alter = alter;
	}

	@Override
	public String toString()
	{
		return "Tier (" + this.alter + ")";
	}

	@Override
	public int hashCode()
	{
		return this.alter;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		Tier other = (Tier) obj;
		if (this.alter != other.alter)
			return false;
		return true;
	}

}

public class Test
{

	public static void main(String[] args)
	{

		System.out.println("========== A1: map mit typ wechsel==========");

		Stream.of(2, 4, 3, 7)
				.map(x -> new Tier(x))
				.forEach(System.out::println);

		System.out.println("");
		System.out.println("");

		System.out.println("========== A2: flatmap ==========");

		Stream<Integer> s1 = Stream.of(1, 2, 3);
		Stream<Integer> s2 = Stream.of(-7, -8);
		Stream<Integer> s3 = Stream.of(100, 200);
		Stream.of(s1, s2, s3).flatMap(x -> x)
				//.map(x->new Tier(x))
				.forEach(x -> System.out.print("[" + x + "]  "));

		System.out.println("");
		System.out.println("");

		System.out.println("========== A3: flatmap - tokenizer ==========");

		List<String> list = Arrays.asList("Java ist toll", "Atrophysik ist besser", "Streams sind einfach", "- meist");

//"1 2 3".split(" ")
		list.stream()
				.flatMap(x -> Stream.of(x.split(" ")))
//				.flatMap(x->Arrays.stream(x.split(" ")))
				.forEach(x -> System.out.print("[" + x + "]  "));

		System.out.println("");
		System.out.println("");

		System.out.println("========== A4: skip ==========");

		Stream.iterate(1, x -> x + 1)
				.limit(10)
				.skip(5)
				.forEach(x -> System.out.print("[" + x + "]  "));

		System.out.println("");
		System.out.println("");

		System.out.println("========== A4: distinct ==========");
		// .distinct() nutzt equals() UND hashCode() !

		Stream.of(1, 33, 1, 77, 2, 18, 2)
				.distinct()
				.forEach(x -> System.out.print("[" + x + "]  "));

		System.out.println("");
		System.out.println("");

		System.out.println("========== A5: distinct Objects==========");

		Tier[] tiere = { new Tier(8), new Tier(9), new Tier(9), new Tier(10), new Tier(12), new Tier(8), };
		Stream.of(tiere)
				.distinct()
				.forEach(x -> System.out.print("[" + x + "]  "));

		System.out.println("");
		System.out.println("");

		System.out.println("========== A6: sorted ==========");

		Stream.of(1, 77, 1, 33, 2, 2, 18)
				.sorted()
				.forEach(x -> System.out.print("[" + x + "]  "));
		System.out.println("");
		Stream.of(1, 77, 1, 33, 2, 2, 18)
				.sorted(Comparator.reverseOrder())
				.forEach(x -> System.out.print("[" + x + "]  "));
		System.out.println("");
		Stream.of(1, 77, 1, 33, 2, 2, 18)
				.sorted((x, y) -> (y & 1000011) - (x & 1000011))
				.forEach(x -> System.out.print("[" + x + "]  "));

		System.out.println("");
		System.out.println("");

		System.out.println("========== A6: peek ==========");

		Arrays.asList("jj", "mm", "ff", "bb")
				.stream()
				.peek(x -> System.out.print("'" + x + "'"))
//				.sorted()
				.forEach(x -> System.out.print("[" + x + "]  "));

		System.out.println("");
		System.out.println("");

		System.out.println("");
		System.out.println("");

		System.out.println("========== A7: Terminal operations ==========");
		System.out.print(".forEach()\t: ");
		Stream.of(1, 2, 3, 4, 5, 6, 7)
				.forEach(x -> System.out.print("[" + x + "]  "));

		System.out.println("");
		long n = Stream.of(1, 2, 3, 4, 5, 6, 7)
				.count();
		System.out.println(".count()\t: " + n);

		System.out.print(".min()\t\t: ");
		Optional<Integer> min = Stream.of(1, 2, 3, 4, 5, 6, 7)
				.min(Comparator.naturalOrder());
		System.out.println(min.get());

		System.out.print(".max()\t\t: ");		// alternative bei verwendung von Optional
		Stream.of(1, 2, 3, 4, 5, 6, 7)
				.max(Comparator.naturalOrder())
				.ifPresent(System.out::println);

		System.out.print(".findFirst()\t: ");
		Stream.of(1, 2, 3, 4, 5, 6, 7)
				.findFirst()
				.ifPresent(System.out::println);
		//		.ifPresentOrElse(System.out::println, ()->System.out.println("null"));

		System.out.print(".findAny()\t: ");
		Stream.of(1, 2, 3, 4, 5, 6, 7)
				.findAny()
				.ifPresent(System.out::println);
		//		.ifPresentOrElse(System.out::println, ()->System.out.println("null"));

	}

}
