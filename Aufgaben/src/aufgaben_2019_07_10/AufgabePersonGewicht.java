package aufgaben_2019_07_10;

import java.util.Arrays;

class Person
{
	String name;
	int gewicht;
	public Person(String name, int gewicht)
	{
		super();
		this.name = name;
		this.gewicht = gewicht;
	}

	public String getName()
	{
		return this.name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public int getGewicht()
	{
		return this.gewicht;
	}
	public void setGewicht(int gewicht)
	{
		this.gewicht = gewicht;
	}
	
	@Override
	public String toString()
	{
		return this.name + " (" + this.gewicht + ")";
	}

}

public class AufgabePersonGewicht
{
	public static void main(String[] args)
	{

		Person[] personen = {
				new Person("Tom",70),
				new Person("Jerry",50),
				new Person("Peter",60),
				new Person("Mary",45)
		};
		
		// alle nach gewicht zwischen 50 und 65 
		System.out.println("========== A1 ==========");
		long n = Arrays.stream(personen)
		.filter( x -> x.getGewicht()>=50 && x.getGewicht()<=50)
		.peek(System.out::println)
		.count();
		System.out.println("count = "+n);
		System.out.println("");
		
		// element mit lexikographisch groesstem namen
		System.out.println("========== A2 ==========");
		Arrays.stream(personen)
		.max( (x,y)->x.getName().compareTo(y.getName()) )
		.ifPresent(System.out::println);
		System.out.println("count = "+n);


	}
}
