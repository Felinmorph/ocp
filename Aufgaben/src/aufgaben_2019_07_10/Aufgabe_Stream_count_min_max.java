package aufgaben_2019_07_10;

import java.util.Arrays;
import java.util.Locale;

public class Aufgabe_Stream_count_min_max
{

	public static void main(String[] args)
	{
		Locale[] locales = Locale.getAvailableLocales();

		System.out.println("========== A1 ==========");

		Arrays.stream(locales)
				.max((x, y) -> x.getDisplayCountry().compareTo(y.getDisplayCountry()))
				.ifPresent(System.out::println);

		System.out.println();
		System.out.println("========== A2 ==========");

		long l = Arrays.stream(locales)
				.filter(x -> x.getLanguage().contains("de"))
				.sorted((x, y) -> x.getDisplayCountry().compareTo(y.getDisplayCountry()))
				.peek(x->System.out.printf("%-6s | %-25s | %s\n", x,x.getDisplayCountry(), x.getDisplayLanguage()))
				.count();
		System.out.println("count: " + l);

		System.out.println();
		System.out.println("========== A3 ==========");
		Arrays.stream(locales).filter(x -> x.getDisplayCountry().contains("t"))
				.min((x, y) -> x.getDisplayLanguage().compareTo(y.getDisplayLanguage()))
				.ifPresent(x -> System.out.printf("%s\n%s\n%s\n", x,x.getDisplayCountry(), x.getDisplayLanguage()));
	}

}
