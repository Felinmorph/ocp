package aufgaben_2019_07_09_Streams;

import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class Test
{

	public static void main(String[] args)
	{
		// Jeder Stream darf nur EINMAL verwendet werden!!
		// daher am besten keine zwischenvariablen nutzen.
		
		System.out.println("========== Stream.of() ==========");
		Stream.of(3, 6, 9, 17, 44)						// Stream-Quelle			//  |
			.filter(x -> x % 2 != 0)					// 'bearbeiter' / filter	//  |  Pipeline
			.map(x -> x*x)								// 'bearbeiter' / filter	//  |
			.forEach(x -> System.out.print(x+", "));	// verbraucher				// \|/

		System.out.println("");
		System.out.println("");
		
		System.out.println("========== Stream.generate() ==========");
		Supplier<Integer> s = ()->(int)(Math.random()*100)+1;
		Stream.generate(s)								// endlose datenquelle
			.limit(10)									// begrenzer
			.forEach(x->System.out.print(x+" "));

		System.out.println("");
		System.out.println("");
		System.out.println("========== Stream.iterate() ==========");
		Integer seed = 1;
		UnaryOperator<Integer> uo = x->x+2;
		Stream.iterate(seed, uo)
			.limit(10)
			.forEach(x->System.out.print(x+" "));

	
		System.out.println("");
		System.out.println("");
		System.out.println("========== Stream.concat() ==========");
		Stream<Integer> s1 = Stream.iterate(1, x->x+1)
								   .filter(x->x%2!=0)
								   .limit(10);
		Stream<Integer> s2 = Stream.iterate(1, x->x+1)
				   				   .filter(x->x%2==0)
				   				   .limit(10);
		Stream<Integer> s3 = Stream.concat(s1, s2);

		s3.forEach(x->System.out.print(x+" "));

		System.out.println("");
		System.out.println("");
		System.out.println("========== aufgabe: Integer & Double -> concat ==========");
		Stream<Integer> s21 = Stream.of(12,13,14);
		Stream<Double> s22 = Stream.of(0.6,0.7,0.8);
		Stream.concat(s21, s22).forEach(x->System.out.print(x+" "));
		
		System.out.println("");
		System.out.println("");
		System.out.println("========== aufgabe: Rechteck ==========");
		class Rechteck
		{
			int b,h;
			public Rechteck(int b, int h)
			{
				this.b=b;
				this.h=h;
			}
			public int getFlaeche() { return this.b*this.h; }
			@Override
			public String toString() { return "Rechteck "+this.b+" x "+this.h; }
		}
		
		Stream.of( new Rechteck(2, 3),
				   new Rechteck(2, 2),
				   new Rechteck(3, 3),
				   new Rechteck(3, 4)
				 )
			.filter(x->x.getFlaeche()>7)
			.forEach(x->System.out.println(x));
		
		
		
	

	}

}
