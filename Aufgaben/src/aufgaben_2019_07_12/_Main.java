package aufgaben_2019_07_12;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class _Main
{

	public static void main(String[] args)
	{
		/*	==================== Stram ...  .collect(,,) ====================
		 * 
		 *     <R> R collect(Supplier  <R>    supplier,				// Sammelpunkt + startwert
         *                   BiConsumer<R, T> accumulator,			// arbeitet die einzelnen schritte ab
         *                   BiConsumer<R, R> combiner);			// bei parallelisierung: fuegt teilergebnisse zusammen
         *                   
         *                   <R> = Typ in dem die Elemente gesammelt werden (arbeits- und rueckgabe-typ)
         *                   <T> = die eigendlichen Elemente die bearbeitet werden sollen (Stream-Quelle)
		 */
		
		a1();
		a2();
		a3();
		a4();
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		a5();
		a6();
		a7();
		a8();
		
	}

	private static void a1()
	{
		Integer[] werte = {1,7,0,-3};
		System.out.println("Werte fuer Tests : " + Arrays.toString(werte));
		System.out.println("");
		System.out.println("========== .collect(,,) ==========");
		
		Supplier<List<Integer>> supplier = ()->new ArrayList<Integer>();
		BiConsumer<List<Integer>,Integer> accumulator = (l,v)->l.add(v);
		BiConsumer<List<Integer>,List<Integer>> combiner = (x,y)->x.addAll(y);
		
		List<Integer> tester = Arrays.stream(werte)
						  			 .collect( supplier, accumulator, combiner);
		System.out.println("Liste: " + tester);
	}

	private static void a2()
	{
		System.out.println("");
		System.out.println("========== Aufgabe: string in HashSet with collect ==========");
		String[] buchstaben = {"a","b","c","d"};
		System.out.println("Werte fuer Tests : " + Arrays.toString(buchstaben));
		HashSet<String> set = Arrays.stream(buchstaben)
				.collect( ()->new HashSet<String>(), (hs,v)->hs.add(v), (x,y)->x.addAll(y));
		System.out.println("set: " + set);
		System.out.println("");
	}
	
	private static void test1()
	{
		//////////////////////////////////////////		
		//		geht nicht, da immutable
		//////////////////////////////////////////		
		//		String[] buchstaben = {"a","b","c","d"};
		//		String kette = Arrays.stream(werte)
		//		          .collect( ()-> new String(""), (String x, Integer y)->x=x+y.toString(), (x,y)->x=x+y);
		//		System.out.println(" kette: " + kette);
		
		// geht
		System.out.println("");
		System.out.println("========== .collect(,,) mit AtomicInteger==========");
		Integer[] werte = {1,7,0,-3};
		System.out.println("Werte fuer Tests : " + Arrays.toString(werte));
		AtomicInteger summe = Arrays.stream(werte)
			  			            .collect( ()-> new AtomicInteger(0), (x,y)->x.set(x.get()+y), (x,y)->x.set(x.get()+y.get()));
		System.out.println("summe: " + summe);
	}
	
	private static void a3()
	{
		test1();
	}

	private static void a4()
	{
		System.out.println("");
		System.out.println("========== Aufgabe: string concat with collect ==========");
		String[] buchstaben = {"a","b","c","d"};
		System.out.println("Werte fuer Tests : " + Arrays.toString(buchstaben));
		StringBuilder sb = Arrays.stream(buchstaben)
		          .collect( ()->new StringBuilder()
		        		  , (sb1,s)->sb1.append(s)
		        		  , (sb1,sb2)->sb1.append(sb2)
		        		  );
		System.out.println("set: " + sb);
		System.out.println("");
	}

	private static void a5()
	{
		// jetzt mit einem Collector
		System.out.println("");
		System.out.println("========== Collector ==========");
		Integer[] werte = {1,2,3,4,5,6};
		System.out.println("Werte fuer Tests : " + Arrays.toString(werte));
		System.out.println("·····································");
		
		Collector<Integer, ?, List<Integer>> collector = Collectors.toList();
		
		//List<Integer> list = Stream.of(1,2,3,4,5).collect(collector);
		List<Integer> list = Arrays.stream(werte).collect(collector);
		System.out.println(".toList()   : " + list);
		System.out.println(".counting() : " + Arrays.stream(werte).collect(Collectors.counting()));
	}
	
	private static void a6()
	{
		System.out.println("");
		System.out.println("========== Aufgabe String[] -> ArrayDeque ==========");
		String[] werte = {"heute","ist","Freitag"};
		System.out.println("Werte fuer Tests : " + Arrays.toString(werte));
		System.out.println("·····································");
		
		Collector<String, ?, ArrayDeque<String>> collector = Collectors.toCollection(ArrayDeque::new);
		
		ArrayDeque<String> erg = Arrays.stream(werte).collect(collector);
		System.out.println("als Deque : " + erg);
	}

	private static void a7()
	{
		System.out.println("");
		System.out.println("========== Aufgabe Locales -> TreeSet ==========");

		
		TreeSet<String> countries = Arrays.stream(Locale.getAvailableLocales())
				.map(x->x.getDisplayCountry())
				.collect(Collectors.toCollection(TreeSet::new));
		
		
		System.out.println("als TreeSet : " + countries);
	}
	private static void a8()
	{
		System.out.println("");
		System.out.println("========== .toMap(,) ==========");

		Collector<String,?,Map<String,Integer>> collector = Collectors.toMap(x->x, x->x.length());
		
		Map<String,Integer> m = Stream.of("abc","dd","hallo","xy").collect(collector);
		
		System.out.println("Keys  : " + m.keySet());
		System.out.println("Values: " + m.values());
	}

}
