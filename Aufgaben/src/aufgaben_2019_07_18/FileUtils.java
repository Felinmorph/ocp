package aufgaben_2019_07_18;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;

public class FileUtils
{
	public static long copyTextFile(String fromFile, String toFile)
	{
		return copyTextFile(fromFile, toFile, false);
	}
	public static long copyTextFile(String fromFile, String toFile, boolean append)
	{
		File inFile = new File(fromFile);
		File outFile = new File(toFile);
//		if (!inFile.isFile() || outFile.isFile())
//			throw new IllegalArgumentException("fromFile must be a valif File and toFile must not exist!");
//		
		int len;
		long nbytes = 0;
		char[] cbuf = new char[512];
		try(Reader in = new FileReader(inFile); Writer out = new FileWriter(outFile, append) ) {
			while( (len = in.read(cbuf)) != -1 ) {
				nbytes += len;				
				out.write(cbuf, 0, len);
			}			
		} catch (Exception e){ e.printStackTrace();	} 
		return nbytes;
	}
}
