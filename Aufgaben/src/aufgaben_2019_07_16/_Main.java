package aufgaben_2019_07_16;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import words.Words;


public class _Main
{

	public static void main(String[] args)
	{
		v1();
		v1a1();
		v1a2();
		v2();
		v2a1();
		v3();
		v3a1();
		v3a2();
		v4();
		v5();
		v6();
	}

	private static void v1()
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  V1 .collector(mapper, downstream)
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== V1: .collector(mapper, downstream) ==========");
		Integer[] array = {1,2,3,4,5};
		
		System.out.println("Integer[] -> List<Integer> ");
		Collector<Integer, ?, List<Integer>> c1 = Collectors.toList();
		List<Integer> li1 = Arrays.stream(array)
				.collect(c1);
		System.out.println(li1);
		
		System.out.println("");
		System.out.println("Integer[] -> (Integer->String) -> List<String> ");
		Collector<String, ?, List<String>> c2 = Collectors.toList();
		List<String> li2 = Arrays.stream(array)
				.map(x->"<"+x+">")
				.collect(c2);
		System.out.println(li2);
		
		
		
		System.out.println("");
		System.out.println("Integer[] -> List<String> (mapper+downstream)");
		Function<Integer, String> mapper = x->"<"+x+">";
		Collector<String, ? , List<String>> downstream = Collectors.toList();
		Collector<Integer, ? , List<String>> c3 = Collectors.mapping(mapper, downstream);
		List<String> li3 = Arrays.stream(array)
				.collect(c3);
		System.out.println(li3);
	}

	private static void v1a1()
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  V1A1: String[] -> (toUpperCase) -> List<String>
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("");
		System.out.println("========== V1A1 String[] -> (toUpperCase) -> List<String> ==========");
		String[] worte = {"Mo","Di","Mi"};
		
		Function<String, String> mapper = x->x.toUpperCase();
		Collector<String, ? , List<String>> downstream = Collectors.toList();
		Collector<String, ? , List<String>> col = Collectors.mapping(mapper, downstream);
		List<String> li = Arrays.stream(worte)
				.collect(col);
		
		List<String> li2 = Arrays.stream(worte)
				.collect(Collectors.mapping	(x->x.toUpperCase(), Collectors.toList()) );
		System.out.println(li);
		
	}
	private static void v1a2()
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  V1A2 
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("");
		System.out.println("========== A1A2  ==========");
		String[] in = {"de","pl","fr","en"};
		ArrayList<Locale> out;
		
		
		Function<String, Locale> mapper = x->new Locale(x);
//		Function<String, Locale> mapper = x->new Locale(x,x);
//		Function<String, Locale> mapper = x->Locale.forLanguageTag(x+"-"+x);
		Collector<Locale, ? , ArrayList<Locale>> downstream = Collectors.toCollection( ()->new ArrayList<Locale>() );
		Collector<String, ? , ArrayList<Locale>> col = Collectors.mapping(mapper, downstream);
		out = Arrays.stream(in)
				.collect(col);
		System.out.println(out);
		out.forEach(x-> System.out.println(x.getDisplayLanguage()));		
		
	}
	private static void v2()
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  V2: .groupingBy(classifier)
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("");
		System.out.println("========== V2: .groupingBy(classifier) ==========");
		Integer[] in = {22,0,-3,7,18,0,-9};
		Map<String,List<Integer>> out;

		Function<Integer, String> classifier = x-> x<0?"negativ":x==0?"null":"positiv";
		Collector<Integer,?,Map<String,List<Integer>>> col = Collectors.groupingBy(classifier);
		
		out = Arrays.stream(in)
					.collect(col);
		System.out.println(out);
		out = Arrays.stream(in)
					.collect( Collectors.groupingBy( x-> x%2==0?"gerade":"ungerade" ) );
		System.out.println(out);
	}
	
	private static void v2a1()
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  V2-A1: .groupingBy(classifier)
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("");
		System.out.println("========== V2-A1 ==========");
		Map<Integer,List<String>> out;

		Function<String, Integer> classifier = x->x.length();
		Collector<String,?,Map<Integer,List<String>>> col = Collectors.groupingBy(classifier);
		
		
		out = Words.passwords().stream()
					.collect(col);
		System.out.println(out);
		
//		System.out.println("Number of different word-lengths: " + out.keySet().size());
//		for (Integer i : out.keySet() )
//		{
//			System.out.printf("Key : %2d -> %4d Values\n",i, out.get(i).size());
//		}
		out.forEach( (k,v) -> System.out.printf("Key : %2d -> %4d Values\n",k, out.get(k).size()) );
	}
	
	private static void v3()
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  V2-A2: .groupingBy(classifier,downstream)
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("");
		System.out.println("========== V3 .groupingBy(classifier,downstream) ==========");
		Integer[] in = {2,-7,-8,22,2,-8};
		Map<String, Set<Integer>> out;

		Function<Integer,String> classifier = x->x<0?"negativ":"positiv";
		Collector<Integer, ?, Set<Integer>> downstream = Collectors.toSet();
		Collector<Integer, ?, Map<String,Set<Integer>>> col = Collectors.groupingBy(classifier,downstream);
		
		
		out = Arrays.stream(in)
					.collect(col);
		System.out.println(out);
		out.forEach( (k,v) -> System.out.printf("\"%s\" -> %s\n",k, v) );
	}
	
	private static void v3a1()
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  V3-A1: .groupingBy(classifier,downstream)
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("");
		System.out.println("========== V3A1 ==========");
		
		class Tier
		{
			String name;
			int alter;
			public Tier(String name, int alter)
			{
				super();
				this.name = name;
				this.alter = alter;
			}
			@Override
			public String toString()
			{
				return this.name + " (" + this.alter + ")";
			}
			public int getAlter()
			{
				return this.alter;
			}
			
		}
		
		Tier[] tiere = {
		                new Tier("Tom",2),
		                new Tier("Jerry",7),
		                new Tier("Bello",3),
		                new Tier("Rex",5),
					   };
		
		Map<String, TreeSet<Tier>> out;

		Function<Tier,String> classifier = x->x.getAlter()<4?"jung":"alt";
		Collector<Tier, ?, TreeSet<Tier>> downstream = Collectors.toCollection( ()->new TreeSet<Tier>( (x,y)->x.getAlter()-y.getAlter() ) );
		Collector<Tier, ?, Map<String,TreeSet<Tier>>> col = Collectors.groupingBy(classifier,downstream);
		
		
		out = Arrays.stream(tiere)
					.collect(col);
		out.forEach( (k,v) -> System.out.printf("\"%s\" -> %s\n",k, v) );

		System.out.println("- - - - ");
		Arrays.stream(tiere)
				.collect( Collectors.groupingBy(x->x.getAlter()<4 , Collectors.toCollection( ()->new TreeSet<Tier>( (x,y)->x.getAlter()-y.getAlter() ) ) ) )
				.forEach( (k,v) -> System.out.printf("\"%s\" -> %s\n",k, v) );

	}
	
	private static void v3a2()
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  V3-A2: .groupingBy(classifier,downstream)
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("");
		System.out.println("========== V3A2 ==========");
		

		Map<Integer, Long> out;

		Function<String,Integer> classifier = x->x.length();
		Collector<String, ?, Long> downstream = Collectors.counting();
		Collector<String, ?, Map<Integer,Long>> col = Collectors.groupingBy(classifier,downstream);
		
		out = Words.passwords().stream()
							   .collect(col);
		out.forEach( (k,v) -> System.out.printf("%2d -> %4dx\n",k, v) );

	}
	
	private static void v4()
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  V4: .groupingBy(classifier,mapFactory,downstream)
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("");
		System.out.println("========== V4: .groupingBy(classifier,mapFactory,downstream) ==========");
		Map<Character, List<String>> out;

		//
		// alt:
		//
//		Function<String,Character> classifier = x->x.charAt(0);
//		Collector<String, ?, List<String>> downstream = Collectors.toList();
//		Collector<String, ?, Map<Character,List<String>>> col = Collectors.groupingBy(classifier,downstream);
		
		//
		// neu:
		//
		Function<String,Character> classifier = x->x.charAt(0);
		Supplier<Map<Character,List<String>>> mapFactory = TreeMap::new;
		Collector<String, ?, List<String>> downstream = Collectors.toList();
		Collector<String, ?, Map<Character,List<String>>> col = Collectors.groupingBy(classifier,mapFactory,downstream);
		
		out = Words.englishWords().stream()
							   .collect(col);
		out.forEach( (k,v) -> System.out.printf("\'%c\' -> %4dx\n",k, v.size()) );
		
//		// kurzform mit geaenderter sortierung
//		Words.englishWords()
//				.stream()
//				.collect( Collectors.groupingBy( x->x.charAt(0)
//											   , ()-> new TreeMap<>( (c1,c2)-> c2-c1)
//											   , Collectors.toList()
//												)
//						)
//				.forEach( (k,v) -> System.out.printf("\'%c\' -> %4dx\n",k, v.size()) );
	}
	
	private static void v5()
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  V5: .partitionBy(classifier)
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("");
		System.out.println("========== V5: .partitionBy(classifier) ==========");
		Integer[] in = {1,2,3,4,5};
		Map<Boolean, List<Integer>> out;

		Arrays.stream(in)
				.collect( Collectors.partitioningBy( x->x%2 == 0) )
				.forEach( (k,v) -> System.out.printf("%-6s -> %s\n",k, v) );
	}
	private static void v6()
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  V6: .partitionBy(classifier,downstream)
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("");
		System.out.println("========== V5: .partitionBy(classifier,downstream) ==========");
		Integer[] in = {1,2,3,4,5};
		Map<Boolean, HashSet<Integer>> out;

		Predicate<Integer> classifier = x->x%2 == 0;
		Collector<Integer,?,HashSet<Integer>> downstream = Collectors.toCollection( HashSet::new );
		Collector<Integer,?,Map<Boolean,HashSet<Integer>>> col = Collectors.partitioningBy( classifier, downstream );
		
		out = Arrays.stream(in)
				.collect( col );
		out.forEach( (k,v) -> System.out.printf("%-6s -> %s\n",k, v) );
	}
	
}
