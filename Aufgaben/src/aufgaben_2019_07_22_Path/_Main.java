package aufgaben_2019_07_22_Path;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class _Main
{
	public static Path basePath = Paths.get("src\\aufgaben_2019_07_22_Path\\").toAbsolutePath();

	public static void main(String[] args)
	{
		erzeugeTestumgebung();

		System.out.println("----------------------------------------------------");
		
		entferneTestumgebung();
	}

	private static void erzeugeTestumgebung()
	{
		System.out.println("Erzeuge Pfade...");
		Path pAuto = basePath.resolve("autos");
		Path pPKW = pAuto.resolve("pkws");
		Path pLKW = pAuto.resolve("lkws");
		Path pMAN = pLKW.resolve("man.txt");
		Path pVW = pPKW.resolve("vw.txt");
		
		createdir(pAuto);
		createdir(pPKW);
		createFile(pVW);
		createdir(pLKW);
		createFile(pMAN);
	}

	private static void createFile(Path p)
	{
		if (!Files.exists(p))
		{
			try
			{
				Files.createFile(p);
				System.out.println("created: : " + p);
			} catch (Exception e){
				System.out.println("Fehler: " + e);
				e.printStackTrace();
			}
		}
		else
		{
			System.out.println("Pfad  \""+ p +"\" existiert bereits!");
		}
	}

	private static void createdir(Path p)
	{
		if (!Files.exists(p))
		{
			try
			{
				Files.createDirectory(p);
				System.out.println("created: : " + p);
			} catch (Exception e){
				System.out.println("Fehler: " + e);
				e.printStackTrace();
			}
		}
		else
		{
			System.out.println("Pfad \""+ p +"\" existiert bereits!");
		}
	}

	private static void entferneTestumgebung()
	{
		System.out.println("Entferne Pfade...");
		Path pAuto = basePath.resolve("autos");
		Path pPKW = pAuto.resolve("pkws");
		Path pLKW = pAuto.resolve("lkws");
		Path pMAN = pLKW.resolve("man.txt");
		Path pVW = pPKW.resolve("vw.txt");
		
		delete(pVW);
		delete(pMAN);
		delete(pLKW);
		delete(pPKW);
		delete(pAuto);
		
	}

	private static void delete(Path p)
	{
		try
		{
			Files.deleteIfExists(p);
		} catch (IOException e)
		{
			System.out.println("Can not delete \""+ p +"\" !");
			e.printStackTrace();
		}
		System.out.println("delete: " + p);
	}

}
