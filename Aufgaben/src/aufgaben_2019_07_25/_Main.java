package aufgaben_2019_07_25;

import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class _Main
{

	public static void main(String[] args)
	{
		Map<String,List<String>> out =
		ZoneId
		.getAvailableZoneIds()
		.stream()
		//.sorted(Comparator.naturalOrder())
		.collect(Collectors.groupingBy( (String x)->x.contains("/")?x.substring(0, x.indexOf('/')):"<Special>") );

		for (String k : out.keySet())
		{
			System.out.printf("%-10s -> (%2d) -> %s%n",k,out.get(k).size(), out.get(k));
		}
		
		
		
	}

}
