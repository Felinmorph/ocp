package aufgaben_2019_06_19;

public class Rennwagen
{
	private String marke;
	private Fahrer fahrer;
	private Motor motor;
	{
		this.motor = new Motor();
	}
	public class Motor
	{
		@Override
		public String toString()
		{
			return "Motor Type1 aus dem Rennwagen " + Rennwagen.this.marke;
		}
	}
	
	static public class Fahrer
	{
		String vorname,nachname;
		public Fahrer(String vorname, String nachname)
		{
			this.vorname = vorname;
			this.nachname = nachname;
		}
		@Override
		public String toString()
		{
			return this.vorname + " " + this.nachname;
		}
		
		
	}
	
	public Rennwagen(String marke)
	{
		this.marke = marke;
	}
	
	private Motor getMotor()
	{
		return this.motor;
	}

	private void setFahrer(Fahrer f)
	{
		this.fahrer = f;
	}	

	@Override
	public String toString()
	{
		assert this.fahrer != null : "Fahrer darf nicht null sein!"; 
		return "Rennwagen " + this.marke + ", Fahrer:" + this.fahrer.toString();
	}






	public static void main(String[] args) {
		
	    Rennwagen rw = new Rennwagen("Mercedes");
	
	    Rennwagen .Fahrer f = new Rennwagen.Fahrer("M.", "Schuhmacher");
	    rw.setFahrer(f);
	
	    Rennwagen.Motor m = rw.getMotor();
	
	    System.out.println(rw);		//Zeile A
	    System.out.println(m);		//Zeile B
	}

}
