package aufgaben_2019_08_09;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class A1
{
	private static char[] createCharArray(char cMin, char cMax)
	{
		if(cMax==cMin)
		{
			return new char[]{cMax};
		}
		if(cMax<cMin)
		{
			return new char[]{cMax,cMin};
		}
		char[] cRet = new char[1+cMax-cMin];
		for (int i = 0; i <= (cMax-cMin); i++)
		{
			cRet[i]=(char)(cMin+i);
		}
		return cRet;
	}
	
	public static void main(String[] args)
	{
		System.out.println("========== main Start ==========");


		char[] arr = createCharArray('a','e');
		
		System.out.println("startarray: " + Arrays.toString(arr));
		System.out.println("");
		
		BigInteger anzahl = factorial(BigInteger.valueOf(arr.length));
		System.out.println("Anzahl der Permutationen: " + anzahl);

		System.out.println("Permutationen:");
		BlockingQueue<char[]> queue = new ArrayBlockingQueue<>(100);
		permutate_neu(arr, arr.length, queue);
		queue.forEach(System.out::println);

		System.out.println("========== main end ==========");
	}

	private static void permutate_neu(char[] arr, int pointer, BlockingQueue<char[]> queue)
	{
	    if(pointer==1) {
	    	queue.offer(Arrays.copyOf(arr,arr.length));
	        return;
	    }
	    
		for (int i = 0; i < pointer-1; i++) {
			permutate_neu(arr, pointer-1,queue);
		    
			if(pointer%2==0) {
			    char tmp = arr[pointer-1];
			    arr[pointer-1] = arr[i];
			    arr[i] = tmp;
			} else {
			    char tmp = arr[pointer-1];
			    arr[pointer-1] = arr[0];
			    arr[0] = tmp;
			}
		}
		
		permutate_neu(arr, pointer-1,queue);
	}


	/*
	 * Erzeugt rekursiv alle möglichen Permutationen der Zeichen 
	 * in dem übergebenen Array mit dem Algorithmus von B. R. Heap
	 * (https://en.wikipedia.org/wiki/Heap%27s_algorithm)
	 *
	 * Z.B. für das Array [a, b, c] werden folgende 6 Permutationen erzeugt:
	 *
	 * 		[a, b, c] 
	 * 		[b, a, c] 
	 * 		[c, a, b] 
	 * 		[a, c, b] 
	 * 		[b, c, a] 
	 * 		[c, b, a] 
	 *
	 * Für den initiirenden Aufruf verwendet man die Länge 
	 * des Arrays als pointer-Argument:
	 *
	 *		char[] array = { 'a', 'b', 'c' };
	 *		permutate(array, array.length);
	 * 
	 */
	public static void permutate_simple(char[] arr, int pointer) {
	    if(pointer==1) {
	        System.out.printf("%s %n", Arrays.toString(arr));
	        return;
	    }
	    
		for (int i = 0; i < pointer-1; i++) {
			permutate_simple(arr, pointer-1);
		    
			if(pointer%2==0) {
			    char tmp = arr[pointer-1];
			    arr[pointer-1] = arr[i];
			    arr[i] = tmp;
			} else {
			    char tmp = arr[pointer-1];
			    arr[pointer-1] = arr[0];
			    arr[0] = tmp;
			}
		}
		
		permutate_simple(arr, pointer-1);
	}
	
	/* Berechnet rekursiv die Fakultät einer Zahl bi:
	 *
	 *   1*2*3*4*...*bi
	 *
	 *	https://de.wikipedia.org/wiki/Fakult%C3%A4t_(Mathematik)
	 *
	 *
	 * Mit der Methode kann man z.B. die Anzahl der Permutationen 
	 * für ein char-Array 'array' mit einzigartigen Zeichen berechnen:
	 *
	 * BigInteger bi = BigInteger.valueOf( array.length );
	 * BigInteger numberPermutations = factorial( bi );
	 * 
	 */
	public static BigInteger factorial(BigInteger bi) {
		if(bi.intValue()==1) {
			return bi;
		}
		return bi.multiply( factorial(bi.subtract(BigInteger.ONE)) );
	}

}
