package aufgaben_2019_07_08;

import java.util.function.Supplier;

public class AufgabeExceptionErstellen
{
	public static void main(String[] args)
	{
//		checkAndThrow(args.length == 1, ()->new IllegalArgumentException("Es wird min 1 Parameter erwartet!"));
		//checkAndThrow(args.length == 1, b->{ if (!b){throw  new IllegalArgumentException("Es wird min 1 Parameter erwartet!");}});

		// soll ohne try-catch gehen
		checkAndThrow(args.length == 1, ()-> new IllegalArgumentException("Es wird min 1 Parameter erwartet!") );
		
		// soll abgefangen werden muessen
		try
		{
			checkAndThrow(args.length == 1, ()-> new Exception("Es wird min 1 Parameter erwartet!") );
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}

//	private static void checkAndThrow(boolean condition, Consumer<Boolean> p)
	private static <T extends Exception> void checkAndThrow(boolean condition, Supplier<T> p) throws T 
	{
		if (!condition)
			throw p.get();
	}
}
