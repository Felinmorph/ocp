package aufgaben_2019_07_08;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

interface KannBehandeltWerden
{
	void setGesund(boolean istGesund);
	boolean isGesund();
}

abstract class Tier implements KannBehandeltWerden
{
	boolean gesund;// = true;
	@Override
	public void setGesund(boolean istGesund)
	{
		this.gesund = istGesund;
	}

	@Override
	public boolean isGesund()
	{
		return this.gesund;
	}
}

class Zebra extends Tier
{
}
class Affe extends Tier
{
}
class Mensch implements KannBehandeltWerden
{
	boolean gesund = true;
	@Override
	public void setGesund(boolean istGesund)
	{
		this.gesund = istGesund;
	}
	
	@Override
	public boolean isGesund()
	{
		return this.gesund;
	}
}
class Arzt<T extends KannBehandeltWerden> extends Mensch
{
	void behandeln(T t)
	{
		t.setGesund(true);
	}
}


public class Zoo
{
	private List<Tier> li = new ArrayList<Tier>();

	public void add(Tier tier)
	{
		this.li.add(tier);
	}
	public Collection<Tier> getAnimals()
	{
		return this.li;
	}
	
	
	
	
	
	public static void main(String[] args)
	{
		Zoo z = new Zoo();

		
		Arzt<Affe> affenArtzt = new Arzt<>();
		affenArtzt.behandeln(new Affe());
		
		Arzt<Tier> tierArtzt = new Arzt<>();
		tierArtzt.behandeln(new Zebra());

		Arzt<KannBehandeltWerden> superArtzt = new Arzt<>();
		superArtzt.behandeln(new Mensch());
		superArtzt.behandeln(new Zebra());
	
	}


}
