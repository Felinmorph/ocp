package aufgaben_2019_07_29;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

//import java.time.ZoneId;
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;

public class _Main
{

	public static void main(String[] args)
	{
		LocalDate ld = LocalDate.now();

		ResourceBundle bundle = ResourceBundle.getBundle("aufgaben_2019_07_29.res.res");

//		String s1 = bundle.getString("user.dateFormat");
//		System.out.println("s1="+s1);

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  User-name aus umgebung 
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		Properties props = System.getProperties();
		if (props.containsKey("user.name"))
			System.out.println("Hallo " + props.getProperty("user.name"));
		System.out.println("");

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  geburtstag aus umgebung
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if (props.containsKey("user.birthday"))
		{
			System.out.println("(aus umgebung)");
			System.out.println("geboren am: " + props.getProperty("user.birthday"));
			LocalDate dtbd = LocalDate.parse(props.getProperty("user.birthday"), DateTimeFormatter.ofPattern("d.MM.y", Locale.GERMANY));
			LocalDate t1 = dtbd.withYear(ld.getYear());
			if (t1.isBefore(ld))
			{
				t1 = t1.plusYears(1);
			}
			int n = t1.getDayOfYear() - ld.getDayOfYear();

			if (n == 0)
				System.out.println("Herzlichen glueckwunsch zum Geburtstag!");
			else
				System.out.println("Geburtstag in " + n + " tagen");
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  oder aus properties datei
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		else
		{
			System.out.println("(aus datei)");
			System.out.println("geboren am: " + bundle.getString("user.birthday"));
			LocalDate dtbd = LocalDate.parse(bundle.getString("user.birthday"), DateTimeFormatter.ofPattern(bundle.getString("user.birthday.parse")));
			LocalDate t1 = dtbd.withYear(ld.getYear());
			if (t1.isBefore(ld))
			{
				t1 = t1.plusYears(1);
			}
			int n = t1.getDayOfYear() - ld.getDayOfYear();

			if (n == 0)
				System.out.println(bundle.getString("user.birthday.congrats"));
			else
				System.out.printf(bundle.getString("user.birthday.till"), n);
		}
		System.out.println("");

		if (Locale.getDefault().equals(Locale.GERMANY))
		{
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("'Heute ist 'd. MMMM y'\r\nEs ist 'EEEE.", Locale.GERMANY);
			System.out.println(ld.format(dtf));
		}
		else
		{
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("'Today is 'MMMM d, y'\r\nIt's 'EEEE", Locale.ENGLISH);
			System.out.println(ld.format(dtf));
		}

	}

}
