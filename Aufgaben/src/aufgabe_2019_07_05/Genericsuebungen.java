package aufgabe_2019_07_05;

import java.util.function.Predicate;

class Test<V>
{
	V v1;
	void setV(V v1) {/*this.v1=v1;*/}
	V getV() {return this.v1;}
}


public class Genericsuebungen
{
	public static void main(String[] args)
	{
		Predicate<String> isLeerOrNull = x -> x == null || x.length() == 0;
		Predicate<Number> isPositiv = x -> x.doubleValue() > 0;
		Predicate<Integer> istGerade = x -> x % 2 == 0;

		//Predicate<? extends Number> p1;		// geht theoretisch, jedoch aufruf mit Integer schlaegt fehl!
		Predicate<? super Integer> p1;

		p1 = istGerade;
		System.out.println("" + p1.test(3));
		p1 = isPositiv;
		System.out.println("" + p1.test(3));
		//p1 = isLeerOrNull;

		Test<Double> m0 = new Test<>();
		Test<Integer> m1 = new Test<>();
		Test<? extends Number> m2 = new Test<>();
		Number n = 22;
		//m.setV(n);		// geht nicht, da setV einen generischen parameter hat (und damit theorethisch die Obj-typisierung 'beschaedigen' koennt...)
		m2 = m0;
	}
}
