package aufgabe_2019_07_05;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

interface Lebewesen
{
}

class Mensch implements Lebewesen
{
}

class Tier implements Lebewesen
{
}

class Hund extends Tier
{
}

class Wildhund extends Hund
{
}

class Katze extends Tier
{
}

public class Lebewesenaufgabe
{
	private static void print(Collection<?> lebewesen)		// <? extends Lebewesen>
	{
		System.out.println("Liste aller Lebewese:");
		lebewesen.forEach(x -> System.out.println("\t" + x));
	}

	private static void addHunde(Collection<? super Hund> col)
	{
		col.add(new Hund());
	}

	public static void main(String[] args)
	{

		LinkedList<Hund> hunde = new LinkedList<>();
		hunde.add(new Hund());
		hunde.add(new Wildhund()); // Hund param = new Wildhund();

		print(hunde);
		addHunde(hunde);

		HashSet<Katze> katzen = new HashSet<>();
		katzen.add(new Katze());
		katzen.add(new Katze());

		print(katzen);
		//addHunde(katzen);		// darf nicht

		ArrayList<Tier> tiere = new ArrayList<>();
		tiere.add(new Hund());
		tiere.add(new Wildhund());
		tiere.add(new Katze());
		tiere.add(new Tier());

		print(tiere);
		addHunde(tiere);

		ArrayDeque<Lebewesen> lebewesen = new ArrayDeque<>();
		lebewesen.add(new Mensch());
		lebewesen.add(new Tier());

		print(lebewesen);
		addHunde(lebewesen);

	}

}
