package aufgaben_2019_07_17;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;


public class _Main
{

	public static void main(String[] args)
	{
		v1();
		a1();
//		v1a1();
//		v1a2();
//		v2();
//		v2a1();
//		v3();
//		v3a1();
//		v3a2();
//		v4();
//		v5();
//		v6();
	}

	private static void v1()
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  V1: Streams mit primitiven datentypen
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== V1: Streams mit primitiven datentypen ==========");
		int[] intArray = {1,2,3,4,5};
		long[] longArray = {10L,11L,12L,13L,14L,15L};
		double[] doubleArray = {0.5,1.5,2.5,3.5,4.5,5.5};
		
		System.out.print("IntStream : ");
		IntStream s1 = IntStream.of(intArray);
		s1.filter( i->i%2 == 0)
				.forEach( i->System.out.print(i+" ") );

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("\n\nLongStream");
		LongStream s2 = LongStream.range(10, 16);
					s2
					.peek(l->System.out.print(l+" "))
					.average()
					.ifPresent( d->System.out.print(" ==> durchschnitt = " + d));

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("\n\nDoubleStream");
		DoubleStream s3 = DoubleStream.iterate(0.5, d->d+1);
		double summe = s3.limit(6)
				.peek(d->System.out.print(d+" "))
				.sum();
		System.out.print(" ==> Summe = " + summe);
		System.out.println("");
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println( 
					Arrays.stream(doubleArray)
							.summaryStatistics()
				);

	}

	private static void a1()
	{
		String rootName="c:\\Windows";
		System.out.print("\n\nAll Directorys in " + rootName + " : ");
		List<File> dirs = listDirectories(rootName);
		dirs.forEach(f->System.out.print( f.getName() + "  " ));
		System.out.println("(" + dirs.size() + ")");
		
		System.out.print("\n\nAll Files in " + rootName + " : ");
		List<File> files = listFiles(rootName);
		files.forEach(f->System.out.print( f.getName() + "  " ));
		System.out.println("(" + files.size() + ")");
		
		long summe = getSizeOfAllFilesInDirectory(rootName);
		System.out.printf("\n\nSumme der Dateigroessen aller Dateien in \"%s\" = %d byte  ~= %d MB",rootName , summe, summe/(1024*1024));
		
		long summe2 = getSizeOfAllFilesInSubDirectoryOf(rootName);
		System.out.printf("\n\nSumme der Dateigroessen aller Dateien in allen unterverzeichnissen von \"%s\" = %d byte  ~= %d MB", rootName , summe2, summe2/(1024*1024));
		
		
	}

	private static long getSizeOfAllFilesInDirectory(String rootName)
	{
		File root = new File(rootName);
		if (!root.isDirectory())
		{
			//throw new NoSuchElementException("\"" + rootName + "\" is not a viable path!");
			return -1;
		}
		File[] liste = root.listFiles(File::isFile);
		return Arrays.stream( (new File(rootName)).listFiles() )
				.filter(File::isFile)
				.mapToLong(File::length)
				.sum();
	}
	private static long getSizeOfAllFilesInSubDirectoryOf(String rootName)
	{
		File root = new File(rootName);
		if (!root.isDirectory())
			return -1;

		File[] liste = root.listFiles(File::isDirectory);
		
		return Arrays.stream( (new File(rootName)).listFiles() )
				.filter(File::isDirectory)
//				.map( x-> x.listFiles(File::isFile) )
//				.filter(x-> x != null)
//				.flatMap(x->Arrays.stream(x))
				.map( x-> x.listFiles(File::isFile) )
				.filter(x-> x != null)
				.flatMap(x->Arrays.stream(x))

				.mapToLong(File::length)
				.sum();
	}

	private static List<File> listDirectories(String rootName)
	{
		File root = new File(rootName);
		if (!root.isDirectory())
		{
			//throw new NoSuchElementException("\"" + rootName + "\" is not a viable path!");
			return new ArrayList<File>();
		}
		File[] liste = root.listFiles(File::isDirectory);
		return new ArrayList<File>(Arrays.asList(liste));
		//return Arrays.stream(liste).filter(f->f.isDirectory()).collect(Collectors.toList());
	}
	private static List<File> listFiles(String rootName)
	{
		File root = new File(rootName);
		if (!root.isDirectory())
		{
			return new ArrayList<File>();
		}
		File[] liste = root.listFiles( File::isFile );
		return new ArrayList<File>(Arrays.asList(liste));
	}
	
	
	
}
