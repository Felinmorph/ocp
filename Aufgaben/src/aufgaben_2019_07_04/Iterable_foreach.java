package aufgaben_2019_07_04;

import java.util.ArrayList;
import java.util.List;

public class Iterable_foreach
{

	public static void main(String[] args)
	{
		List<Integer> l = new ArrayList<>();
		
		for (int i = 0; i < 10; i++)
		{
			l.add((int) (Math.random() * 100));
		}

		l.forEach(x->System.out.printf(x+", "));

	}

}
