package aufgaben_2019_07_04;

import java.util.ArrayList;
import java.util.List;

public class Collection_removeIf
{
	public static void main(String[] args)
	{

		List<String> liste = new ArrayList<>();

		liste.add("mo");
		liste.add("di");
		liste.add("mi");
		liste.add("do");
		liste.add("fr");


		
		System.out.println("vorher: " + liste);
		
		liste.removeIf(x->x.startsWith("m"));
		
		System.out.println("vorher: " + liste);
		
		//liste.forEach(x->System.out.printf(x+", "));
	}

}
