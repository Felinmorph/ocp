package aufgaben_2019_06_26;

import java.time.LocalDate;
import java.util.Comparator;

public class Labdas
{
	public static void main(String[] args)
	{
		LocalDate ld1 = LocalDate.of(2013, 4, 10);
		LocalDate ld2 = LocalDate.of(2019, 6, 26);	
		
		Comparator<LocalDate> c = (o1,o2)->o1.getDayOfMonth()-o2.getDayOfMonth();
		Comparator<LocalDate> c2 = Comparator.comparing(LocalDate::getDayOfMonth);
		
		System.out.println("ld1="+ld1);
		System.out.println("ld2="+ld2);
		System.out.println("Vergleich der Monatstage:");
		System.out.println("ld1 > ld2 ? "+c.compare(ld1, ld2));
		System.out.println("ld1 > ld2 ? "+c2.compare(ld1, ld2));
	}
}
