package aufgabe_2019_06_28_Hashset;

import java.util.HashSet;

class Person
{
	String vorname = "Max";
	String nachname = "Musterman";

	public Person(String vornname, String nachname)
	{
		super();
		this.vorname = vornname;
		this.nachname = nachname;
	}

	@Override
	public int hashCode()
	{
		return this.nachname.length()*1024 + this.vorname.length();
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (!this.nachname.equals(other.nachname))
			return false;
		if (!this.vorname.equals(other.vorname))
			return false;
		
		return true;
	}

	@Override
	public String toString()
	{
		return vorname + " " + nachname;
	}
	
}

public class HashSetMitPersonen
{

	public static void main(String[] args)
	{
		HashSet<Person> leute = new HashSet<>();
		leute.add(new Person("Hans","Wellman"));
		leute.add(new Person("Hans","Mueller"));
		leute.add(new Person("Elfriede","Freilich"));
		leute.add(new Person("Guenter","Lach"));
		leute.add(new Person("Hans","Mueller"));
		
		System.out.println("leute.size() = " + leute.size());
		System.out.println("leute: " + leute);
		
	}

}
