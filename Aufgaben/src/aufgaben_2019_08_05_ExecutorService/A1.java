package aufgaben_2019_08_05_ExecutorService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class A1
{
	public static int value;
	
	public static synchronized void IncValue()
	{
		value++;
	}

	public static void main(String[] args)
	{
		System.out.println("========== main Start ==========");

		ExecutorService service = Executors.newCachedThreadPool();
		
		for (int i = 0; i < 200; i++) {
			
			Runnable task = () -> {for (int n = 0; n < 1_000; n++) { IncValue(); }}; 
			service.execute(task);
		}
		
		service.shutdown();
		while( !service.isTerminated() ) {
			try{
				Thread.sleep(100);
			} catch (InterruptedException e)
			{ /* done waiting...*/ }
		}
		System.out.printf("value = %,d%n",value);

		System.out.println("========== main end ==========");
	}

}
