package aufgaben_2019_07_15;

import java.util.Comparator;

import words.Words;


public class _Main
{

	public static void main(String[] args)
	{
		a1();
		a2();

	}

	private static void a1()
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A1 - Words
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("========== A1 - Words ==========");
		System.out.println("Das Laengste Wort: ");
		Words.englishWords().stream()
				.max(Comparator.comparing(String::length))
				.ifPresent(System.out::println);
	}

	private static void a2()
	{
		System.out.println("========== A2 - anz. Woerter ==========");
		long n = Words.englishWords().stream()
				.filter(x->x.length()==5)
				.count();
		System.out.println("Anzahl der woerter: " + n);

		
	}

}
