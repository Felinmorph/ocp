package aufgabe_Function_StringTransform;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Function_StringTransform
{


	public static void main(String[] args) {
        
        // Transformationen vordefinieren:
        StringTransform t1 = new StringTransform()
                .addTransformation( s -> s.toUpperCase() )
                .addTransformation( s -> s + "!" );
        
        // Transformationen durchführen:
        String s = t1.process("Hallo");
        System.out.println(s); // HALLO!
        
        s = t1.process("Java ist toll");
        System.out.println(s); // JAVA IST TOLL!
    }
}

class StringTransform
{
	private List<Function<String,String>> fktListe;
	
	
	public StringTransform()
	{
		this.fktListe = new ArrayList<Function<String,String>>();
	}

	public StringTransform addTransformation(Function<String,String> f)
	{
		this.fktListe.add(f);
		return this;
	}

	public String process(String s)
	{
//		for (int i = 0; i < fktListe.size(); i++)
//		{
//			s = fktListe.get(i).apply(s);
//		}
		for (Function<String, String> f : this.fktListe)
		{
			s = f.apply(s);
		}
		return s;
	}


}
