package aufgaben_2019_07_01;

import java.util.Comparator;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.function.Function;

public class AufgabeTreeSet
{
	static class Tier
	{
		String name;
		int alter;

		public Tier(String name, int alter)
		{
			super();
			this.name = name;
			this.alter = alter;
		}

		@Override
		public String toString()
		{
			return this.name + " (" + this.alter + ")";
		}

	}

	public static void main(String[] args)
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A1
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		{
			//TreeSet<Tier> tst = new TreeSet<>( (o1,o2)->o1.toString().compareTo(o2.toString()));

			Function<Tier, String> keyExtractor = Tier::toString;
			Comparator<Tier> cmp = Comparator.comparing(keyExtractor);
			TreeSet<Tier> tst = new TreeSet<>(cmp);

			tst.add(new Tier("Tom", 3));
			tst.add(new Tier("Jerry", 2));
			tst.add(new Tier("Tom", 1));

			System.out.println("Set:" + tst);
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A2 - kombinierter Comperator
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		{
			Comparator<Tier> cmp1 = (o1, o2) -> o1.name.compareTo(o2.name);
			Comparator<Tier> cmp2 = (o1, o2) -> o1.alter - o2.alter;

			Comparator<Tier> cmp = cmp1.thenComparing(cmp2);
			TreeSet<Tier> tst = new TreeSet<>(cmp);

			tst.add(new Tier("Tom", 3));
			tst.add(new Tier("Jerry", 2));
			tst.add(new Tier("Tom", 1));
			tst.add(new Tier("Bello", 100));
			tst.add(new Tier("Bello", 22));

			System.out.println("Set:" + tst);
		}
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A3: untermenge
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		{
			Comparator<Tier> cmp1 = Comparator.comparing(x -> x.name);
			Comparator<Tier> cmp2 = Comparator.comparing(x -> x.alter);
			Comparator<Tier> cmp = cmp1.thenComparing(cmp2);
			TreeSet<Tier> tst = new TreeSet<>(cmp);

			tst.add(new Tier("Tom", 3));
			tst.add(new Tier("Jerry", 2));
			tst.add(new Tier("Tom", 1));
			tst.add(new Tier("Bello", 100));
			tst.add(new Tier("Bello", 22));

			System.out.println("Set:" + tst);
			NavigableSet<Tier> tstss = tst.subSet(new Tier("Bello", 100), true, new Tier("Tom", 1), true);
			System.out.println("SubSet:" + tstss);
			
		}
	}

}
