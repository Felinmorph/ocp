package aufgaben_2019_07_30_threads;

class MyThread extends Thread
{
	private int n = -1;
	public MyThread(int n)
	{
		this.n = n;
	}

	@Override
	public void run()
	{
		System.out.printf("running thread %3d%n",this.n);
	}
}

public class _Main
{

	public static void main(String[] args)
	{
		System.out.println("main Start:");
		
		for (int i = 0; i < 307; i++)
		{
			System.out.printf("starting %3d%n",i);
			Thread th = new MyThread(i);
			th.start();
		}
		
		System.out.println("main end");
	}

}
