package aufgaben_2019_07_11;

import java.util.Arrays;


public class Test
{

	public static void main(String[] args)
	{
		Integer[] werte = {1,7,0,-3};
		System.out.println("Werte fuer Tests : " + Arrays.toString(werte));
		System.out.println("");
		System.out.println("========== .allMatch() ==========");
		boolean sindAlleWertePositiv = Arrays.stream(werte)
											 .allMatch(x->x>0);
		System.out.println(" alle werte > 0 : " + sindAlleWertePositiv);

		
		System.out.println("");
		System.out.println("========== .anyMatch() ==========");
		boolean istEinWertPositiv = Arrays.stream(werte)
										  .anyMatch(x->x>0);
		System.out.println(" ein wert > 0 : " + istEinWertPositiv);

		System.out.println("");
		System.out.println("========== .noneMatch() ==========");
		boolean istKeinWertPositiv = Arrays.stream(werte)
										  .noneMatch(x->x>0);
		System.out.println(" kein wert > 0 : " + istKeinWertPositiv);
		
		System.out.println("");
		System.out.println("========== .reduce() ==========");
		Arrays.stream(werte)
				.reduce( (x,y) -> x+y )
				.ifPresent(x -> System.out.println(x));
		
		System.out.println("");
		System.out.println("========== Aufgabe 'String concat with .reduce()' ==========");
		String[] items = {"Java"," ","ist"," ","toll","!"};
		Arrays.stream(items)
				.reduce( (x,y) -> x+y )
				.ifPresent(x -> System.out.println(x));

		System.out.println("");
		System.out.println("========== .reduce(,) ==========");
		int n = Arrays.stream(werte)
				.reduce( 0 , (x,y) -> x+y );
		System.out.println("n="+n);
				
		System.out.println("");
		System.out.println("========== Aufgabe .reduce(,) ==========");
		Integer[] werte2 = {2,3,2,3};
		System.out.println("Werte: " + Arrays.toString(werte2));
		int kettenProdukt = Arrays.stream(werte2)
								  .reduce( 1, (x,y) -> x*y );
		System.out.println("ketten Produkt : "+kettenProdukt);

		System.out.println("");
		System.out.println("========== Aufgabe gesammtlaenge ==========");
		String[] werte3 = {"aa","bbb","cccc","ddddd"};
		System.out.println("Werte: " + Arrays.toString(werte3));
		String gesammtlaengeStr = Arrays.stream(werte3)
				                        .reduce( "0", (String x, String y) -> ((Integer.parseInt(x) +y.length())+"") );
		int gesammtlaenge = Arrays.stream(werte3)
								  .map(x->x.length())
								  .reduce( 0, (x,y) -> x+y );
		System.out.println("gesammt laenge (String): "+gesammtlaengeStr);
		System.out.println("gesammt laenge (int): "+gesammtlaenge);


		System.out.println("");
		System.out.println("========== Aufgabe .reduce(,,) ==========");
		
		System.out.println("Werte: " + Arrays.toString(werte3));
		int kettenSumme = Arrays.stream(werte3)
								  .reduce( 0, (x,y) -> x+y.length() ,(x,y) -> x+y);	// startwert, summierer, kombinierer
		System.out.println("Summe : "+kettenSumme);

	
	}

}
