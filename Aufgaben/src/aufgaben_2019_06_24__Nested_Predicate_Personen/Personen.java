package aufgaben_2019_06_24__Nested_Predicate_Personen;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

class Aufgabe3Filter implements Predicate<Person>
{
	private int filterYear = 1990;
	public void setFilterYear(int year)
	{
		this.filterYear= year;
	}
	@Override
	public boolean test(Person p)
	{
		return p.getGeburtsjahr() > this.filterYear;
	}
}

public class Personen
{
	static class Aufgabe4Filter implements Predicate<Person>
	{
		@Override
		public boolean test(Person p)
		{
			return p.getName().contains("a");
		}
	}
	public static void main(String[] args)
	{
		new Personen()._Start();
	}
	
	public void _Start()
	{
		Person[] personen = {
				new Person("Mueller", "Heinz",1940),
				new Person("Bach", "Bathelomaeus",1994),
				new Person("Mueller", "Gertrude",1973),
				new Person("Schulze", "Torben",2001),
				new Person(),
		};
		
		System.out.println("======== A1 ========");
		this.print(personen);
		
		System.out.println("======== A3 ========");
		Aufgabe3Filter f3 = new Aufgabe3Filter();
		f3.setFilterYear(1990);
		List<Person> liPersonen = this.filtern(personen, f3);
		this.print(liPersonen);

		System.out.println("======== A4 ========");
		Aufgabe4Filter f4 = new Aufgabe4Filter();
		liPersonen = this.filtern(personen, f4);
		this.print(liPersonen);

		System.out.println("======== A5 ========");
		liPersonen = this.FilterfuerAufgabe5(personen);
		this.print(liPersonen);
	
		System.out.println("======== A6 ========");
		Predicate<Person> f6 = new Predicate<Person>() {
			@Override
			public boolean test(Person p)
			{
				return f3.test(p) && f4.test(p);			// 8 abweichend von aufgabe, da laengere namen...
			}
		};
		liPersonen = this.filtern(personen, f6);
		this.print(liPersonen);
	
		System.out.println("======== A7 ========");
		liPersonen = this.filtern(personen, p->Year.isLeap(p.getGeburtsjahr()));
		this.print(liPersonen);
	
	}

	private List<Person> FilterfuerAufgabe5(Person[] personen)
	{
		
		class Aufgabe5Filter implements Predicate<Person>
		{
			@Override
			public boolean test(Person p)
			{
				return p.getName().length()>=8;			// 8 abweichend von aufgabe, da laengere namen...
			}
		}
		return this.filtern(personen, new Aufgabe5Filter());
	}

	private List<Person> filtern(Person[] personen, Predicate<Person> p)
	{
		List<Person> erg = new ArrayList<Person>();
		for (Person person : personen)
		{
			if (p.test(person))
			{
				erg.add(person);
			}
		}
		return erg;
	}
	
	
	private void print(Person[] personen)
	{
		for (int i = 0; i < personen.length; i++)
		{
			System.out.printf("%2d. %s%n",i+1,personen[i]);
		}
	}
	private void print(List<Person> personen)
	{
		int i=0;
		for (Person p : personen)
		{
			System.out.printf("%2d. %s%n",++i,p);
		}
	}

}
