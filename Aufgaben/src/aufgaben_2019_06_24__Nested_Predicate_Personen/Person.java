package aufgaben_2019_06_24__Nested_Predicate_Personen;

public class Person implements Comparable<Person>
{
	private String name;
	private String vorname;
	private int geburtsjahr;

	public Person()
	{
		this("Musterman", "Max", 1900);
	}

	public Person(String name, String vorname, int geburtsjahr)
	{
		super();
		this.setName(name);
		this.setVorname(vorname);
		this.setGeburtsjahr(geburtsjahr);
	}

	public String getName()
	{
		return this.name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getVorname()
	{
		return this.vorname;
	}

	public void setVorname(String vorname)
	{
		this.vorname = vorname;
	}

	public int getGeburtsjahr()
	{
		return this.geburtsjahr;
	}

	public void setGeburtsjahr(int geburtsjahr)
	{
		this.geburtsjahr = geburtsjahr;
	}

	@Override
	public String toString()
	{
		return this.getVorname() + " " + this.getName() + " (geb." + this.getGeburtsjahr() + ")";
	}

	@Override
	public int compareTo(Person o)
	{
		return this.toString().compareTo(o.toString());
	}

}
