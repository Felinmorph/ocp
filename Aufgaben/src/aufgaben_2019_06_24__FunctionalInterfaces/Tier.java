package aufgaben_2019_06_24__FunctionalInterfaces;

import java.util.function.Function;

public class Tier
{
	int alter;

	public Tier(int alter)
	{
		this.setAlter(alter);
	}

	public int getAlter()
	{
		return this.alter;
	}

	public void setAlter(int alter)
	{
		this.alter = alter;
	}

	@Override
	public String toString()
	{
		return "Tier [alter=" + this.alter + "]";
	}

	
	public static void main(String[] args)
	{
		Function<Integer,Tier> f = i -> new Tier(i);
		
		for (int n = 0; n < 5; n++)
		{
			Tier t = f.apply(n);
			System.out.println(t);
		}
	}
	
}
