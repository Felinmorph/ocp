package aufgaben_2019_06_24__FunctionalInterfaces;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;


public class _Start
{

	public static void main(String[] args)
	{
		
		Supplier<Integer> s = ()-> new Random().nextInt(41) -20;
		
		System.out.println("============ Supplier ============");
		for (int i = 0; i < 10; i++)
		{
			System.out.print(s.get() + " ");
		}
		System.out.println("");

		System.out.println("");

		Consumer<Integer> c = n -> System.out.println(n);
		System.out.println("============ Consumer ============");
		for (int i = 0; i < 3; i++)
		{
			c.accept(i);
		}
		
		Function<Integer,Double> f = (r) -> Math.PI*2*r*r;
		System.out.println("============ Consumer ============");
		System.out.println("Flaeche eines Kreises mit Radius 5cm = " + f.apply(5)+"cm2");

	}

}
