package aufgaben_2019_06_14;

import java.util.ArrayList;
import java.util.Collection;

class NotIntegerArgumentException extends RuntimeException
{
	public NotIntegerArgumentException(String message)
	{
		super(message);
	}
}

class NotPostivieArgumentException extends RuntimeException
{
	public NotPostivieArgumentException(String message)
	{
		super(message);
	}
}

class NullArgumentException extends RuntimeException
{
	public NullArgumentException(String message)
	{
		super(message);
	}
}

public class ArrayListPositive extends ArrayList<Integer>
{

	public ArrayListPositive()
	{
		super();
	}

	public ArrayListPositive(int initialCapacity)
	{
		super(initialCapacity);
	}

	public ArrayListPositive(Collection c)
	{
		super(c.size());
		for (Object o : c)
		{
			if (o == null)
			{
				throw new NullArgumentException("Not an positive Integer: null");
			}
			if (o instanceof Integer)
			{
				this.add((Integer) o);
			}
			else
			{
				throw new NotIntegerArgumentException("Not an positive Integer: Object (" + o.getClass().getSimpleName() + "): " + o);
			}
		}

	}

	@Override
	public boolean add(Integer i)
	{
		if (i == null)
		{
			throw new NullArgumentException("Not an positive Integer: null");
		}
		if (i >= 0)
			return super.add(i);
		else
			throw new NotPostivieArgumentException("Not an positive Integer: " + i);
		//return false;
	}

	@Override
	public void add(int index, Integer i)
	{
		if (i > 0)
			super.add(index, i);
		else
			throw new NotPostivieArgumentException("Not an positive Integer: " + i);
	}

}
