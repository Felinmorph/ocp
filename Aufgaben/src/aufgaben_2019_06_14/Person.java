package aufgaben_2019_06_14;

public class Person implements Comparable<Person>
{
	private String name;
	private String vorname;

	public Person()
	{
		this.setName("");
		this.setVorname("");
	}

	public Person(String name, String vorname)
	{
		super();
		this.setName(name);
		this.setVorname(vorname);
	}

	public String getName()
	{
		return this.name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getVorname()
	{
		return this.vorname;
	}

	public void setVorname(String vorname)
	{
		this.vorname = vorname;
	}

	@Override
	public String toString()
	{
		return "\"" + this.getName() + ", " + this.getVorname() + "\"";
	}

	@Override
	public int compareTo(Person o)
	{
		if(this.getName().equals(o.getName()))
			return this.getVorname().compareTo(o.getVorname());
		return this.getName().compareTo(o.getName());
	}



}
