package aufgaben_2019_06_14;

import java.util.ArrayList;
import java.util.Arrays;

public class _Test
{

	public static void main(String[] args)
	{
		Person[] leute = new Person[4];
		leute[0] = new Person("Mueller", "Heinz");
		leute[1] = new Person("Brecht", "Bathelomaeus");
		leute[2] = new Person("Mueller", "Gertrude");
		leute[3] = new Person("Schulze", "Torben");

		System.out.print("Vorher:            ");
		System.out.println(Arrays.toString(leute));
		System.out.println();
		
		
		System.out.print("nach sort:         ");
		Arrays.sort(leute);
		System.out.println(Arrays.toString(leute));
		System.out.println();

		
		System.out.print("nach MyComperator: ");
		Arrays.sort(leute, new MyComperator());
		System.out.println(Arrays.toString(leute));
		System.out.println();

		
		System.out.println("New (Iteger) ArraylistPositive: ");
		ArrayListPositive al = new ArrayListPositive();
		al.add(5);
		al.add(7);
		al.add(0,3);
		//al.add(-11);
		al.add(null);
		System.out.println(al);
		System.out.println("");
		
		ArrayList<Object> al2 = new ArrayList();
		al2.add(new Integer(11));
		al2.add(new Integer(13));
		//al2.add(new Long(17L));
		//al2.add(new Double(3.1415));
		
		al = new ArrayListPositive(al2);
		System.out.println(al);
		System.out.println("");
		
		
		
	}

}
