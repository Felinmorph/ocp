package aufgaben_2019_07_02;

import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;

class Dozent
{
	String name;

	public String getName()
	{
		return this.name;
	}

	public Dozent(String name)
	{
		super();
		this.name = name;
	}

	@Override
	public String toString()
	{
		return this.name;
	}

}

public class PriorityQuesTest
{

	public static void main(String[] args)
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A1
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		PriorityQueue<Dozent> q = new PriorityQueue<>(Comparator.comparing(Dozent::getName));

		q.offer(new Dozent("Dozent 1"));
		q.offer(new Dozent("Dozent 4"));
		q.offer(new Dozent("Dozent 3"));
		q.offer(new Dozent("Dozent 2"));
		q.offer(new Dozent("Dozent 5"));
		
		System.out.println("Dozenten: " + q);
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A2
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		Iterator<Dozent> it = q.iterator();
		System.out.println("Alle Dozenten: ");
		while (it.hasNext())
		{
			System.out.println(" - " + it.next());
		}

	}

}
