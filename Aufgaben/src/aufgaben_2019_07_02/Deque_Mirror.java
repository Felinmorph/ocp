package aufgaben_2019_07_02;

import java.util.ArrayDeque;
import java.util.Deque;

class Mirror
{
	Deque<Character> spiegel;

	public Mirror()
	{
		super();
		this.spiegel = new ArrayDeque<>();
		this.spiegel.add('|');
	}

	public void add(char ch)
	{
		this.spiegel.addFirst(ch);
		this.spiegel.addLast(ch);
	}

	public boolean isEmpty()
	{
		return this.spiegel.size() <= 1;
	}

	public void remove()
	{
		if (this.spiegel.size() > 0)
		{
			this.spiegel.removeFirst();
			this.spiegel.removeLast();
		}
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder(this.spiegel.size());
		for (Character c : this.spiegel)
		{
			sb.append(c);
		}
		return sb.toString();
	}
	
}

public class Deque_Mirror
{

	public static void main(String[] args)
	{
		Mirror m = new Mirror();
		
		for (char ch = 'a'; ch < 'g'; ch++) {
			m.add(ch);
			System.out.println(m);
		}
		
		while( !m.isEmpty() ) {
			System.out.println(m);
			m.remove();
		}

	}

}
