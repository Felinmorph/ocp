package aufgabe_Supplier_deferred_eval_Tracing;

import java.util.function.Supplier;

public class Supplier_deferred_eval_Tracing___Benchmark
{

	public static void main(String[] args)
	{
		// test-arrays vorbereiten
		int iterations = 10000000;
		String[][] v = new String[iterations][];
		for (int n = 0; n < iterations; n++)
		{
			int rnd = (int) (Math.random() * 5) + 1;
			String[] row = new String[rnd];
			for (int l = 0; l < rnd; l++)
			{
				row[l] = "" + ((int) (Math.random() * 100) + 573);
			}
			v[n] = row;
		}

		// drei Foos erzeugen
		Foo_ori f1 = new Foo_ori();
		Foo2 f2 = new Foo2();
		Foo3 f3 = new Foo3();

		long t1a = System.currentTimeMillis();
		for (int i = 0; i < v.length; i++)
		{
			f1.m(v[i]);
		}
		long t1b = System.currentTimeMillis();

		long t2a = System.currentTimeMillis();
		for (int i = 0; i < v.length; i++)
		{
			f2.m(v[i]);
		}
		long t2b = System.currentTimeMillis();

		long t3a = System.currentTimeMillis();
		for (int i = 0; i < v.length; i++)
		{
			f3.m(v[i]);
		}
		long t3b = System.currentTimeMillis();

		System.out.println("Origial: " + (t1b - t1a) + "ms");
		System.out.println("supplier: " + (t2b - t2a) + "ms");
		System.out.println("public: " + (t3b - t3a) + "ms");
	}

}

/*
 * Bekannt ist, dass die Methode `m` sehr häufig aufgerufen wird. 
 * Da auch beim Ändern vom Tracing-Level im `tracer` auf MyTracer.Level.NONE 
 * bei jedem Aufruf der Methode `m` der neue String gebildet wird, 
 * wurde es entschieden die Methode `trace` so zu ändern, dass das 
 * Bilden vom String nur dann stattfindet, wenn Level.TRACE gesetzt war. 
 * Übernehmen Sie bitte die Aufgabe.
 */

class Foo_ori
{
	MyTracer_ori tracer = new MyTracer_ori(MyTracer_ori.Level.TRACE);

	public void m(String[] args)
	{
		this.tracer.trace("args length: " + args.length);
		//...
	}
}

class Foo2
{
	MyTracer2 tracer = new MyTracer2(MyTracer2.Level.TRACE);

	public void m(String[] args)
	{
//		this.tracer.trace("args length: " + args.length);
		this.tracer.trace(() -> "args length: " + args.length);
		//...
	}
}

class Foo3
{
	MyTracer3 tracer = new MyTracer3(MyTracer3.Level.TRACE);

	public void m(String[] args)
	{
		if (this.tracer.level == MyTracer3.Level.TRACE)
		{
			this.tracer.trace("args length: " + args.length);
			//...
		}
	}
}

class MyTracer_ori
{
	public enum Level
	{
		NONE, TRACE
	}

	//private final Level level;
	private final Level level;

	public MyTracer_ori(Level level)
	{
		this.level = level;
	}

	public void trace(String message)
	{
		if (this.level == Level.TRACE)
		{
			System.out.println(message);
		}
	}
}

class MyTracer2
{
	public enum Level
	{
		NONE, TRACE
	}

	private final Level level;

	public MyTracer2(Level level)
	{
		this.level = level;
	}

	public void trace(Supplier<String> messageSupplier)
	{
		if (this.level == Level.TRACE)
		{
			System.out.println(messageSupplier.get());
		}
	}
}

class MyTracer3
{
	public enum Level
	{
		NONE, TRACE
	}

	public final Level level;

	public MyTracer3(Level level)
	{
		this.level = level;
	}

	public void trace(String message)
	{
//		if (this.level == Level.TRACE)
//		{
		System.out.println(message);
//		}
	}
}