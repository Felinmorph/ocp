package aufgabe_Supplier_deferred_eval_Tracing;

import java.util.function.Function;
import java.util.function.Supplier;

public class Supplier_deferred_eval_Tracing
{

	public static void main(String[] args)
	{
		String[] v = {"a64","b49","c1","d3"};

		new Foo().m(v);
	}

}

/*
 * Bekannt ist, dass die Methode `m` sehr häufig aufgerufen wird. 
 * Da auch beim Ändern vom Tracing-Level im `tracer` auf MyTracer.Level.NONE 
 * bei jedem Aufruf der Methode `m` der neue String gebildet wird, 
 * wurde es entschieden die Methode `trace` so zu ändern, dass das 
 * Bilden vom String nur dann stattfindet, wenn Level.TRACE gesetzt war. 
 * Übernehmen Sie bitte die Aufgabe.
 */

class Foo
{
	MyTracer tracer = new MyTracer(MyTracer.Level.TRACE);

	public void m(String[] args)
	{
		this.tracer.trace(() -> "args length: " + args.length);
		//...
		
		Function<MyTracer.Level, MyTracer> f = l-> new MyTracer(l);
		MyTracer t = f.apply(MyTracer.Level.TRACE);
		System.out.println("t="+t);
		t.trace(() -> "args length: " + args.length);
		
		Function<MyTracer.Level, MyTracer> f2 = MyTracer::new;
		MyTracer t2 = f2.apply(MyTracer.Level.TRACE);
		System.out.println("t2="+t2);
		t2.trace(() -> "args length: " + args.length);
	}
}



class MyTracer
{
	public enum Level
	{
		NONE, TRACE
	}

	private final Level level;

	public MyTracer(Level level)
	{
		this.level = level;
	}

	public void trace(Supplier<String> messageSupplier)
	{
		if (this.level == Level.TRACE)
		{
			System.out.println(messageSupplier.get());
		}
	}
}
