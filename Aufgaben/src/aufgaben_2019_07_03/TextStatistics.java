package aufgaben_2019_07_03;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class TextStatistics
{
	private String text;

	private TextStatistics(String text)
	{
		this.text = text;
	}

	public static TextStatistics of(String text)
	{
		return new TextStatistics(text);
	}

	public Collection<Character> getUniqueChars()
	{
		return this.createCharMap().keySet();
	}

	private Map<Character, Integer> createCharMap()
	{
		Map<Character, Integer> cc = new HashMap<>();		// cc = Character Counter
		for (int i = 0; i < this.text.length(); i++)
		{
			Character c = this.text.charAt(i);
			if (cc.containsKey(c))
			{
				cc.put(c, cc.get(c) + 1);
			}
			else
			{
				cc.put(c, 1);
			}
		}
		return cc;
	}

	public Map<Character, Integer> getCharCounts()
	{
		return this.createCharMap();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//   M A I N
	//
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String[] args)
	{
		TextStatistics tt = TextStatistics.of("Heute ist Montag!");
		Collection<Character> chars = tt.getUniqueChars();
		System.out.println("chars: " + chars);
		System.out.println("");

		Map<Character, Integer> verteilung = tt.getCharCounts();
		System.out.println("Verteilung:");
		System.out.println(verteilung);

	}
}
