package aufgaben_2019_07_03;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileTypes
{
	private File root;

	public FileTypes(String pathName)
	{

		this.root = new File(pathName);

		if (!this.root.isDirectory())
		{
			throw new IllegalArgumentException(pathName + " must be a directory!");
		}
	}

	public Collection<String> getFileTypes()
	{
		return this.createFileTypeMap().keySet();
	}

	private List<File> getFiles(File root)
	{
		List<File> fl;
		File[] files = root.listFiles();
		if (files != null)
		{
			fl = new ArrayList<File>(Arrays.asList(files));
		}
		else
		{
			return new ArrayList<File>();
		}

		int index = 0;
		while (index < fl.size())
		{
			System.out.printf(" %4d : %s\n", index, fl.get(index));
			if (fl.get(index).isDirectory())
			{
				File f = fl.remove(index);
				fl.addAll(index, this.getFiles(f));
			}
			index++;
		}
		return fl;
	}

	public Map<String, Integer> createFileTypeMap()
	{
		Map<String, Integer> em = new HashMap<>();

		//File[] files = this.root.listFiles(File::isFile);  // FileFilter ff = path -> path.isFile();

		List<File> files = this.getFiles(this.root);

		for (File f : files)
		{
			String name = f.getName();
			String ext;
			int pos = name.lastIndexOf('.');
			// wenn kein punkt = keine dateiendung,
			// oder einziger punkt ist am anfang = versteckte datei,
			// oder dateiname endet mit punkt,
			// dann keine endung!
			if (pos < 1 || pos == name.length() - 1)
			{
				ext = "<no ext>";
			}
			else
			{
				ext = name.substring(pos + 1).toLowerCase();
			}

			if (em.containsKey(ext))
			{
				em.put(ext, em.get(ext) + 1);
			}
			else
			{
				em.put(ext, 1);
			}

		}

		return em;
	}

	private void printVerteilung()
	{
//		TreeMap<String, Integer> tm = new TreeMap<>( k,v -> v);
//		tm.putAll(this.createFileTypeMap());
//
//		
//		for (String key : tm.keySet())
//		{
//			System.out.printf("%3d x '%s'",tm.get(key) ,key);
//		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//   M A I N
	//
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static void main(String[] args)
	{
		String basePath = "C:\\Windows";
		FileTypes ft = new FileTypes(basePath);

		Collection<String> extColl = ft.getFileTypes();
		System.out.println("Extensions in : " + basePath);
		System.out.println(extColl);
		System.out.println("");
		System.out.println("Haeufigkeitsverteilung:");
		System.out.println(ft.createFileTypeMap());
		ft.printVerteilung();
	}

}
