package aufgaben_2019_07_03;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class MapTest
{

	public static void main(String[] args)
	{
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  A1
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		Map<Integer,LocalDate> m = new HashMap<>();
		
		m.put(1,  LocalDate.of(2013, 5 , 17));
		m.put(4,  LocalDate.of(2000, 1 , 4));
		m.put(63, LocalDate.of(2013, 3 , 13));
		m.put(5,  LocalDate.of(2019, 11, 8));
		m.put(8,  LocalDate.of(2010, 4 , 29));
		
		for (Integer i : m.keySet())
		{
			System.out.printf(" %2d - %s\n", i, m.get(i).toString());
		}

	}

}
