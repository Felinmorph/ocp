package aufgaben_2019_07_31_threads;

import java.util.ArrayList;
import java.util.List;

class MyThread extends Thread
{
	private List<Integer> li;

	
	public MyThread()
	{
		this.li = new ArrayList<Integer>();
	}

	@Override
	public void run()
	{
		for (int i = 0; i < 100; i++)
		{
			this.li.add( (int)(Math.random()*100-50) );
		}
	}

	public List<Integer> getLi()
	{
		return this.li;
	}
	
	
}

public class _Main
{

	public static void main(String[] args)
	{
		System.out.println("========== main Start ==========");
		
		MyThread th = new MyThread();
		th.start();
		
		try
		{
			th.join();
		} catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("--> " + th.getLi());
		
		
		System.out.println("========== main end ==========");
	}

}
