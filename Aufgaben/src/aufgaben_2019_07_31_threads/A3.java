package aufgaben_2019_07_31_threads;

import java.util.ArrayList;

public class A3
{
	volatile static ArrayList<String> liste;

	public static void main(String[] args) throws InterruptedException
	{

		System.out.println("========== main Start ==========");

		liste = new ArrayList<>();

		Runnable target1 = () -> {
			for (int i = 0; i < 1_000; i++)
			{
				liste.add("A_" + i);
			}
		};
		Runnable target2 = () -> {
			for (int i = 0; i < 1_000; i++)
			{
				liste.add("B_" + i);
			}
		};

		Thread th1 = new Thread(target1);
		Thread th2 = new Thread(target2);

		th1.start();
		th2.start();

		th1.join();
		th2.join();

		System.out.println(liste.size());

		/*
		 * A1. lassen Sie bitte die main abwarten, bis die Inkrementierungen
		 * 		beendet wurden 
		 * 
		 */

		System.out.println("========== main end ==========");
	}

}
