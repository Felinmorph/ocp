package aufgaben_2019_07_31_threads;



public class A2
{
	volatile static int count;

	public static void main(String[] args) throws InterruptedException {
		
		System.out.println("========== main Start ==========");
		
		Runnable target = () -> {
			for (int i = 0; i < 1_000; i++) {
				count++;
			}
		};
		
		Thread th = new Thread(target);
		th.start();
		
		Thread th2 = new Thread(target);
		th2.start();

		th.join();
		th2.join();
		
		System.out.println( count );
		
		/*
		 * A1. lassen Sie bitte die main abwarten, bis die Inkrementierungen
		 * 		beendet wurden 
		 * 
		 */

		System.out.println("========== main end ==========");
	}

}
