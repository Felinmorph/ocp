package aufgaben_2019_08_01_synchronisation;

class Printer
{
	private char c;
	private int n;
	private int z;
	private Thread th;

	public Printer(char c, int n, int zeilen)
	{
		this.c = c;
		this.n = n;
		this.z = zeilen;
		this.th = new Thread(() -> {
			for (int i = 0; i < Printer.this.z; i++)
			{
				//String s = "";
				synchronized ( Printer.class ) {			// System.out  waere besser zum syncen,
															// da von den print methoden auch verwendet
				for (int j = 0; j < Printer.this.n; j++)
				{
					//s += Printer.this.c;
					System.out.print(Printer.this.c);
				}
				System.out.println();
				}
				// System.out.println(s);
			}
		});
	}

	public void start()
	{
		this.th.start();
	}
//	
//	public void join()
//	{
//		try
//		{
//			this.th.join();
//		} catch (InterruptedException e)
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

}

public class _Main
{

	public static void main(String[] args)
	{
		System.out.println("========== main Start ==========");

		Printer p1 = new Printer('a', 10, 20);	// auszugebendes zeichen, wie oft pro zeile, anzahl der zeilen
		p1.start();

		Printer p2 = new Printer('*', 15, 40);
		p2.start();

//		p1.join();
//		p2.join();

		System.out.println("========== main end ==========");
	}

}
