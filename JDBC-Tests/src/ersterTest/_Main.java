package ersterTest;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;

public class _Main
{

	public static void main(String[] args)// throws SQLException
	{
		System.out.println("========== main Start ==========\n");

		System.out.println("Liste aller verfuegbaren DB-Treiber:");
		Enumeration<Driver> d = DriverManager.getDrivers();
		while (d.hasMoreElements())
		{
			System.out.println("--> " + d.nextElement().getClass().getName());
		}
		System.out.println("");

		// access-treiber
		// https://sourceforge.net/projects/ucanaccess/

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  Verbindung zur DB herstellen
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		System.out.println("Verbindung zur DB herstellen...");

		// verbindungs-string zur DB
		//String url = "jdbc:ucanaccess://C:\\Java\\Projekte\\OCP\\Felinmorph\\DB\\Kontaktverwaltung.accdb";
		String url = "jdbc:ucanaccess://../DB/Kontaktverwaltung.accdb";

		// connections sollten auch wieder geschlossen werden,
		// der einfachste weg ist somit ein
		// try with ressources
		try ( Connection conn = DriverManager.getConnection(url); Statement stmt = conn.createStatement(); )
		{
			System.out.println("... Verbindung wurde hergestellt");
			System.out.println("... Befehls-Object wurde erstellt");

			String sql = "SELECT * FROM Mitarbeiter";
			System.out.println("\n" + sql);
			boolean b = stmt.execute(sql);	// b => hat ein result-set
			System.out.println("Select hat ergebnis-set: " + b);

			sql = "UPDATE Mitarbeiter SET Name='Braun' WHERE PersonalNr=6;";
			System.out.println("\n" + sql);
			int count = stmt.executeUpdate(sql);
			System.out.println("Select hat " + count + " ergebnise");

//			sql = "UPDATE Mitarbeiter SET [Tel intern] = [Tel intern] + 7500;";
//			System.out.println("\n"+sql);
//			count = stmt.executeUpdate(sql);
//			System.out.println("Select hat " + count + " ergebnise");

			sql = "SELECT * FROM Mitarbeiter";
			System.out.println("\n" + sql);
			ResultSet rs = stmt.executeQuery(sql);
			System.out.println("Mitarbeiter : ");
			int n = 0;
			while (rs.next())
			{
				System.out.print(rs.getString(1) + ": " + rs.getString("Vorname") + " " + rs.getString("Name") + ", ");
				n++;
			}
			System.out.println("\n--> " + n + " ergebnisse.");

			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//
			//  Aufgabe 1a
			//
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			System.out.println("\n\n========== Aufgabe 1a ==========");
			sql = "SELECT * FROM Mitarbeiter WHERE [Ort]='Frankfurt'";
			System.out.println("\n" + sql);
			rs = stmt.executeQuery(sql);
			System.out.println("Mitarbeiter : ");
			n = 0;
			while (rs.next())
			{
				System.out.println(rs.getString(1) + ": " + rs.getString("Vorname") + " " + rs.getString("Name") + ", ");
				n++;
			}
			System.out.println("\n--> " + n + " ergebnisse.");

			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//
			//  Aufgabe 1b
			//
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			System.out.println("\n\n========== Aufgabe 1b ==========");
			sql = "SELECT * FROM Mitarbeiter";
			System.out.println("\n" + sql);
			rs = stmt.executeQuery(sql);
			System.out.println("Mitarbeiter (nur aus Frankfurt): ");
			n = 0;
			while (rs.next())
			{
				if (rs.getString("Ort").equalsIgnoreCase("Frankfurt"))
				{
					System.out.println(rs.getString(1) + ": " + rs.getString("Vorname") + " " + rs.getString("Name") + ", ");
					n++;
				}
			}
			System.out.println("\n--> " + n + " ergebnisse.");

			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//
			//  Aufgabe 2
			//
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			System.out.println("========== Aufgabe 2 ==========");
			//String neueKat = "Reisebuero";
			String neueKat = "Test";
			sql = "INSERT INTO [Kontaktarten](Kontaktart) VALUES( '" + neueKat + "')";
			System.out.println("\n" + sql);
			n = stmt.executeUpdate(sql);
			System.out.println(n + " eintraege hinzugefuegt!");

		} catch (SQLException e)
		{
			System.out.println("SQL-Fehler!\r\n");
			e.printStackTrace();
		}

		System.out.println("\n========== main end   ==========");
	}

}
