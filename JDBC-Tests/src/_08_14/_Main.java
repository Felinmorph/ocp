package _08_14;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;

public class _Main
{
	/*
	 *  world  
	 *  -> [city] 			
	 *  		-> 'ID', 'Name', 'CountryCode', 'District', 'Population'
	 *
	 *  -> [country] 		
	 *  		-> 'Code', 'Name', 'Continent', 'Region', 'SurfaceArea', 'IndepYear',  
	 * 			   'Population', 'LifeExpectancy', 'GNP', 'GNPOld', 'LocalName', 
	 * 			   'GovernmentForm', 'HeadOfState', 'Capital', 'Code2'
	 * 
	 *  -> [countrylanguage]	
	 * 			-> 'CountryCode', 'Language', 'IsOfficial', 'Percentage'
	 *
	 */
	public static void main(String[] args)// throws SQLException
	{
		System.out.println("========== main Start ==========\n");

		String sql;
		ResultSet rs;
		ResultSetMetaData md;
		int n;
		boolean b;

		String url = "jdbc:mysql://localhost:3306/world?serverTimezone=Europe/Berlin";
		String user = "root";
		String pwd = "12345";
		

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  Liste aller verfuegbaren DB-Treiber
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//printTreiberListe();

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//  Verbindung zur DB herstellen
		//
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		try ( Connection conn = DriverManager.getConnection(url, user, pwd); Statement stmt = conn.createStatement(); )
		{
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//
			//  Liste aller Laender
			//
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//printLaender(stmt);

			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//
			//  Allgemeine herangehensweise
			//
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//printCommonProcedure(stmt);
			
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//
			//  ResultSet Typen
			//
			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			System.out.println("========== ResultSet Typen ==========");
			DatabaseMetaData dbm = conn.getMetaData();
			System.out.println("FORWARD_ONLY ?       " + dbm.supportsResultSetType(ResultSet.TYPE_FORWARD_ONLY));
			System.out.println("SCROLL_INSENSITIVE ? " + dbm.supportsResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE));
			System.out.println("SCROLL_SENSITIVE ?   " + dbm.supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE));

			System.out.println("TYPE_SCROLL_INSENSITIVE & CONCUR_UPDATABLE ?       " + dbm.supportsResultSetConcurrency(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE));
			System.out.println("TYPE_SCROLL_SENSITIVE   & CONCUR_UPDATABLE ?       " + dbm.supportsResultSetConcurrency(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE));

		} catch (SQLException e)
		{
			System.out.println("SQL-Fehler!\r\n");
			e.printStackTrace();
		}

		System.out.println("\n========== main end   ==========");
	}

	private static void printCommonProcedure(Statement stmt) throws SQLException
	{
		String sql;
		ResultSet rs;
		ResultSetMetaData md;
		int n;
		boolean b;
		System.out.println("============ Allgemeine herangehensweise ============");
		//			sql = "SELECT Name,Population FROM city WHERE Population>5000000;";
		sql = "SELECT * FROM city WHERE Population>5000000;";
		//			sql = "SELECT * FROM city WHERE CountryCode='DEU';";
		//			sql = "SELECT country.Name, country.Capital FROM country ;";
		System.out.println("\n" + sql);
		b = stmt.execute(sql);
		if (b)
		{
			rs = stmt.getResultSet();
			md = rs.getMetaData();
			n = 0;
			int[] spaltenbreiten = new int[md.getColumnCount() + 1];

			for (int i = 1; i <= md.getColumnCount(); i++)
			{
				int breite = md.getColumnDisplaySize(i);
				String title = md.getColumnName(i);
				if (title.length() > breite)
				{
					breite = title.length();
				}
				spaltenbreiten[i] = breite;
				System.out.printf("%-" + breite + "s | ", title);
			}
			System.out.println("");
			for (int i = 1; i < spaltenbreiten.length; i++)
			{
				if (i > 1)
					System.out.print("-");
				for (int j = 0; j < spaltenbreiten[i]; j++)
				{
					System.out.print("-");
				}
				System.out.print("-|");
			}
			System.out.println("");
			while (rs.next())
			{
				for (int i = 1; i <= md.getColumnCount(); i++)
				{
					//						if (md.getColumnType(i) == Types.INTEGER)
					//							System.out.printf("%," + spaltenbreiten[i] + "d | ",rs.getInt(i) );
					//						else
					System.out.printf("%-" + spaltenbreiten[i] + "s | ", rs.getString(i));
				}
				System.out.println("");

				//System.out.printf("%-20s (%,6.0fk)%n", name, pop/1000.0);
				n++;
			}
		}
		else
		{
			n = stmt.getUpdateCount();
		}
		System.out.println("--> " + n + " ergebnisse.");
	}

	private static void printLaender(Statement stmt) throws SQLException
	{
		String sql;
		ResultSet rs;
		int n;
		sql = "SELECT Code,Name,Continent,Population FROM country";
		System.out.println("\n" + sql);
		rs = stmt.executeQuery(sql);
		n = 0;
		while (rs.next())
		{
			String code = rs.getString("Code");
			String name = rs.getString("Name");
			String cont = rs.getString("Continent");
			int pop = rs.getInt("Population");
			System.out.printf("[%3s] %13s / %-40s (%,6.1fm)%n", code, cont, name, pop / 1000000.0);
			n++;
		}
		System.out.println("--> " + n + " ergebnisse.");
	}

	private static void printTreiberListe()
	{
		System.out.println("Liste aller verfuegbaren DB-Treiber:");
		Enumeration<Driver> d = DriverManager.getDrivers();
		while (d.hasMoreElements())
		{
			System.out.println("--> " + d.nextElement().getClass().getName());
		}
		System.out.println("");
	}

}
