package projekt.enums;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import projekt.gui.options.PanelOptions;

public enum EStamps
{
	SQUARE_FILLED("Quadrat (gef�llt)", "/projekt/images/shape_quadrat_gefuellt.png", 0b111_111_111),
	SQUARE_HOLLOW("Quadrat (hohl)", "/projekt/images/shape_quadrat_hohl.png", 0b111_101_111),
	PLUS("Plus", "/projekt/images/shape_plus.png", 0b010_111_010), X("X", "/projekt/images/shape_x.png", 0b101_010_101),
	DIAMOND("Raute", "/projekt/images/shape_diamond.png", 0b010_101_010);

	//private final Icon ico;
	private final String iconPath;
	private final String name;
	private final int stampPattern;
	//private boolean[][] stampPattern;

	private EStamps(String name, String iconPath, int bitPattern)
	{
		//this.ico = new ImageIcon(PanelOptions.class.getResource(iconPath));
		this.name = name;
		this.iconPath = iconPath;
		this.stampPattern = bitPattern;
	}

	public Icon getIcon()
	{
		return new ImageIcon(PanelOptions.class.getResource(this.iconPath));
	}

	public String getName()
	{
		return this.name;
	}

	public boolean[][] getStampPattern()
	{
		boolean[][] bitPattern = new boolean[3][3];
		bitPattern[0][0] = (this.stampPattern & 0b100000000) != 0;
		bitPattern[0][1] = (this.stampPattern & 0b010000000) != 0;
		bitPattern[0][2] = (this.stampPattern & 0b001000000) != 0;
		bitPattern[1][0] = (this.stampPattern & 0b000100000) != 0;
		bitPattern[1][1] = (this.stampPattern & 0b000010000) != 0;
		bitPattern[1][2] = (this.stampPattern & 0b000001000) != 0;
		bitPattern[2][0] = (this.stampPattern & 0b000000100) != 0;
		bitPattern[2][1] = (this.stampPattern & 0b000000010) != 0;
		bitPattern[2][2] = (this.stampPattern & 0b000000001) != 0;
		return bitPattern;
	}

	@Override
	public String toString()
	{
		return this.name;
	}
}
