package projekt.enums;

public enum ESizeGameField
{
	SMALL(10, 7), NORMAL(15, 10), BIG(20, 15);

	private int sizeX;
	private int sizeY;

	private ESizeGameField(int x, int y)
	{
		this.sizeX = x;
		this.sizeY = y;
	}

	public int getSizeX() { return this.sizeX; }
	public int getSizeY() { return this.sizeY; }

	@Override
	public String toString()
	{
		return this.name().toLowerCase() + " (" + this.sizeX + "x" + this.sizeY + ")";
	}
}
