package projekt.enums;

public enum EDificulty
{
	EASY("leicht"), NORMAL("normal"), HARD("schwer");

	private String name;
	
	private EDificulty(String name)
	{
		this.name = name;
	}
	@Override
	public String toString()
	{
		return this.name;
	}
}
