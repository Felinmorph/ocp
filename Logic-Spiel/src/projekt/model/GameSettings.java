package projekt.model;

import projekt.enums.EDificulty;
import projekt.enums.ESizeGameField;
import projekt.enums.EStamps;

public class GameSettings
{
	private ESizeGameField size;
	private EStamps stamp;
	private EDificulty dificulty;

	private int level;

	public GameSettings()
	{
		this(ESizeGameField.NORMAL,EStamps.SQUARE_FILLED,EDificulty.NORMAL,1);
	}

	public GameSettings(ESizeGameField size, EStamps stamp, EDificulty dificulty, int level)
	{
		this.setSize(size);
		this.setStamp(stamp);
		this.setDificulty(dificulty);
		this.setLevel(level);
	}

	public ESizeGameField getSize()
	{
		return this.size;
	}

	public void setSize(ESizeGameField size)
	{
		this.size = size;
	}

	public EStamps getStamp()
	{
		return this.stamp;
	}

	public void setStamp(EStamps stamp)
	{
		this.stamp = stamp;
	}

	public EDificulty getDificulty()
	{
		return this.dificulty;
	}

	public void setDificulty(EDificulty dificulty)
	{
		this.dificulty = dificulty;
	}

	public int getLevel()
	{
		return this.level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}
}
