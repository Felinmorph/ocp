package projekt.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import projekt.enums.EDificulty;
import projekt.enums.ESizeGameField;
import projekt.enums.EStamps;

public class Game
{
	private GameSettings gs;
	private HighScore hs;
	private int zuege;

	private int tryCounter = 3;

	private boolean[][] field;
	private boolean[][] stampPattern;

	public Game()
	{
		this(new GameSettings());	// using standar values
	}

	public Game(GameSettings gs)
	{
		this.gs = gs;
		this.hs = new HighScore(new File(System.getProperty("user.home") + "/LogicGame_HighScore.txt"));
		this.newGame();	// using standar values
	}

	public boolean[][] getField()
	{
		return this.field;
	}

	public int getZuege()
	{
		return this.zuege;
	}
	public int getTryCounter()
	{
		return this.tryCounter;
	}

	public int getLevel()
	{
		return this.gs.getLevel();
	}
	public EDificulty getDificulty()
	{
		return this.gs.getDificulty();
	}
	public EStamps getStamp()
	{
		return this.gs.getStamp();
	}
	public ESizeGameField getSize()
	{
		return this.gs.getSize();
	}
	
	public boolean isGameOver()
	{
		return this.getTryCounter() <= 0;
	}

	public void increaseLevel()
	{
		this.gs.setLevel(this.gs.getLevel() + 1);
		this.newLevel();
	}

	public void resetLevel()
	{
		this.tryCounter--;
		this.newLevel();
	}

	public void newGame()
	{
		this.newLevel();
	}

	private void newLevel()
	{
		this.field = new boolean[this.gs.getSize().getSizeY()][this.gs.getSize().getSizeX()];
		this.stampPattern = this.gs.getStamp().getStampPattern();
		this.shuffleBoard(this.gs.getLevel());
		this.zuege = this.calculateMaxMoves();
	}

	private int calculateMaxMoves()
	{
		double m = this.gs.getLevel();
		switch (this.gs.getDificulty())
		{
			default:
			case EASY:
				m *= 1.2;
				m += 2;
				break;
			case NORMAL:
				m *= 1.0;
				m += 2;
				break;
			case HARD:
				m *= 1.0;
				break;
		}
		return (int) m;
	}

	public int getPossiblePlaceInHighscore()
	{
		return this.hs.getPossiblePlaceInHighscore(this.gs.getSize(), this.gs.getStamp(), this.gs.getDificulty(), this.gs.getLevel());
	}

	public void insertNewHighScore(String name)
	{
		this.hs.insertNewHighScore(this.gs.getSize(), this.gs.getStamp(), this.gs.getDificulty(), this.gs.getLevel()-1, name);
	}

	public String getHighScoreScoreAsString(int index)
	{
		return this.hs.getHighScoreScoreAsString(this.gs.getSize(), this.gs.getStamp(), this.gs.getDificulty(),index);
	}

	public String getHighScoreName(int index)
	{
		return this.hs.getHighScoreName(this.gs.getSize(), this.gs.getStamp(), this.gs.getDificulty(),index);
	}

	private void shuffleBoard(int numberOfMoves)
	{
		List<Integer> alreadyClickedCoordinates = new ArrayList<Integer>();
		int x = 0, y = 0;
		Integer t;
		for (int i = 0; i < numberOfMoves; i++)						// fuehre eine entsprechende anzahl clicks auf das spielfeld aus
		{
			do														// vermeide mehrmals die selbe stelle auszuwaehlen
			{
				x = (int) (Math.random() * this.gs.getSize().getSizeX());
				y = (int) (Math.random() * this.gs.getSize().getSizeY());
				t = new Integer(y * 1000 + x);

			} while (alreadyClickedCoordinates.contains(t));
			alreadyClickedCoordinates.add(t);
			this.performMove(x, y);

		}

	}

	public boolean isBoardCleared()
	{
		for (int y = 0; y < this.field.length; y++)
		{
			for (int x = 0; x < this.field[y].length; x++)
			{
				if (this.field[y][x])		// wenn irgendein spielstein gesetzt ist...
					return false;
			}
		}
		return true;
	}

	public void performMove(int x, int y)
	{
		if (x < 0 || y < 0 || x > this.gs.getSize().getSizeX() || y > this.gs.getSize().getSizeY())	// discard illegal moves!
			return;
		this.zuege--;
		// toggle all cells around the clicked one wich are masked by the actual used stamp
		for (int dy = 0; dy < 3; dy++)
		{
			for (int dx = 0; dx < 3; dx++)
			{
				if (this.stampPattern[dx][dy])
				{
					this.toggleSingleField(x + 1 - dx, y + 1 - dy);
				}
			}
		}

	}

	private void toggleSingleField(int x, int y)
	{
		if (x < 0)
		{
			x = x + this.gs.getSize().getSizeX();
		}
		if (y < 0)
		{
			y = y + this.gs.getSize().getSizeY();
		}
		if (x >= this.gs.getSize().getSizeX())
		{
			x = x - this.gs.getSize().getSizeX();
		}
		if (y >= this.gs.getSize().getSizeY())
		{
			y = y - this.gs.getSize().getSizeY();
		}
		this.field[y][x] = !this.field[y][x];
	}

}
