package projekt.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;

import javax.swing.JOptionPane;

import projekt.enums.EDificulty;
import projekt.enums.ESizeGameField;
import projekt.enums.EStamps;

public class HighScore
{
	HashMap<String, String> hshm;
	File hsf;

	public HighScore(File f)
	{
		this.hsf = f;
		//prepare empty HighScore-HashMap
		this.generateDefaultMap();
		this.load();
	}

	public String getHighScoreScoreAsString(ESizeGameField size, EStamps stamp, EDificulty dificulty, int index)
	{
		String key = size.name() + "-" + stamp.name() + "-" + dificulty.name() + "-s" + index;
		if (this.hshm.containsKey(key))
			return this.hshm.get(key);
		return this.hshm.getOrDefault(key, "0");
	}

	public String getHighScoreName(ESizeGameField size, EStamps stamp, EDificulty dificulty, int index)
	{
		String key = size.name() + "-" + stamp.name() + "-" + dificulty.name() + "-n" + index;
		return this.hshm.getOrDefault(key, "---");
	}

	public int getPossiblePlaceInHighscore(ESizeGameField size, EStamps stamp, EDificulty dificulty, int score)
	{
		String hsss = size.name() + "-" + stamp.name() + "-" + dificulty.name() + "-s";		//HighScore-Score-String
		for (int i = 0; i < 5; i++)
		{
			if (this.hshm.containsKey(hsss + i))
			{
				try
				{
					int v = Integer.parseInt(this.hshm.get(hsss + i));
					if (score >= v)
					{
						return i + 1;
					}
				} catch (NumberFormatException e)
				{
					// jemand hat an der datei gespielt - ignorieren...
				}
			}
		}
		return 99;
	}

	public void insertNewHighScore(ESizeGameField size, EStamps stamp, EDificulty dificulty, int score, String name)
	{
		String hsss = size.name() + "-" + stamp.name() + "-" + dificulty.name() + "-s";		//HighScore-Score-String
		String hsns = size.name() + "-" + stamp.name() + "-" + dificulty.name() + "-n";		//HighScore-Name-String
		for (int i = 0; i < 5; i++)
		{
			if (this.hshm.containsKey(hsss + i))
			{
				try
				{
					int v = Integer.parseInt(this.hshm.get(hsss + i));
					if (score >= v)
					{
						// assuming proper formating - here is the point where we insert the new entry
						// but first letzt move this and all later entry one down the line...
						// [4] <- [3]
						// [3] <- [2]
						// ...
						for (int j = 3; j >= i; j--)
						{
							this.hshm.put(hsss + (j + 1), this.hshm.get(hsss + j));
							this.hshm.put(hsns + (j + 1), this.hshm.get(hsns + j));
						}
						// now set our new entry ...
						// [i] <- x
						this.hshm.put(hsss + i, "" + score);
						this.hshm.put(hsns + i, name);
						// ... save ...
						this.writeHighscoreFile();
						// and be done...
						return;
					}
				} catch (NumberFormatException e)
				{
					// jemand hat an der datei gespielt - ignorieren...
				}
			}
		}
	}

	private void generateDefaultMap()
	{
		this.hshm = new HashMap<String, String>(ESizeGameField.values().length * EStamps.values().length * EDificulty.values().length);
		for (ESizeGameField size : ESizeGameField.values())
		{
			for (EStamps stamp : EStamps.values())
			{
				for (EDificulty dif : EDificulty.values())
				{
					String s = size.name() + "-" + stamp.name() + "-" + dif.name();
					for (int i = 0; i < 5; i++)
					{
						this.hshm.put(s + "-n" + i, "---");
						this.hshm.put(s + "-s" + i, "" + (10 - 2 * i));
					}
				}
			}
		}
	}

	private void load()
	{
		if (this.hsf == null)		// no proper file = only temporal Highscores!
			return;
		if (this.hsf.exists())
		{
			try
			{
				Reader in = new FileReader(this.hsf);
				BufferedReader br = new BufferedReader(in);
				while (br.ready())
				{
					String zeile = br.readLine();
					int n = zeile.indexOf('=');				// erstes '=' suchen
					String key = zeile.substring(0, n);		// alles davor ist der key
					String value = zeile.substring(n + 1);	// alles danach der wert
					this.hshm.put(key, value);
				}
				br.close();

			} catch (IOException e)
			{
				JOptionPane.showMessageDialog(null, "Unable to properly access HighScore-File!\nHighscores can not be read or written!", "Warning",
						JOptionPane.WARNING_MESSAGE);
			}

		}
		else
		{
			this.createDefaultFile();
		}

	}

	private void createDefaultFile()
	{
		this.generateDefaultMap();
		this.writeHighscoreFile();
	}

	private void writeHighscoreFile()
	{
		if (this.hsf == null)		// no file = oly temporal HS!
			return;
		FileWriter fw = null;
		StringBuilder sb = new StringBuilder();
		for (String s : this.hshm.keySet())
		{
			sb.append(s);
			sb.append('=');
			sb.append(this.hshm.get(s));
			sb.append('\n');
		}

		try
		{
			this.hsf.createNewFile();
			fw = new FileWriter(this.hsf, false);
			fw.write(sb.toString());
			fw.close();
		} catch (IOException e)
		{
			JOptionPane.showMessageDialog(null, "Unable to properly access HighScore-File!\nHighscores can not be read or written!", "Warning",
					JOptionPane.WARNING_MESSAGE);
		}
	}

}
