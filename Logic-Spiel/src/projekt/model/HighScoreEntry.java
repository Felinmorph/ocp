package projekt.model;

class HighScoreEntry
{
	private String name;
	private int score;
	
	public HighScoreEntry()
	{
		this.setName("---");
		this.setScore(0);
	}

	public HighScoreEntry(String name, int score)
	{
		this.setName(name);
		this.setScore(score);
	}

	public String getName()
	{
		return this.name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getScore()
	{
		return this.score;
	}

	public void setScore(int score)
	{
		this.score = score;
	}
	
}
