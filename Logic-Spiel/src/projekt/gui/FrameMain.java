package projekt.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class FrameMain extends JFrame
{
	JPanel innerPanel;

	public FrameMain()
	{
		this.setPreferredSize(new Dimension(800, 600));
		this.setSize(new Dimension(800, 600));
		this.setMinimumSize(new Dimension(800, 600));
		this.init();

		this.pack();
		this.setVisible(true);
	}

	private void init()
	{
		this.setTitle("Projekt: Logik-Spiel");
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(FrameMain.class.getResource("/projekt/images/dreieck.png")));
		JPanel panelMain = new JPanel();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setContentPane(panelMain);

		panelMain.setBackground(Color.LIGHT_GRAY);
		panelMain.setBorder(new EmptyBorder(6, 6, 6, 6));
		panelMain.setPreferredSize(new Dimension(789, 600));
		panelMain.setLayout(new BorderLayout(0, 0));

		JLabel lblNewLabel = new JLabel("Nur ein weiteres , einfaches Logik-Spiel");
		lblNewLabel.setBorder(new EmptyBorder(6, 6, 6, 6));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 24));
		panelMain.add(lblNewLabel, BorderLayout.NORTH);

		this.innerPanel = new JPanel();
		panelMain.add(this.innerPanel);
		this.innerPanel.setLayout(new BorderLayout(0, 0));

	}

	public void setInnerPanel(JPanel p)
	{
		this.remove(this.innerPanel);
		//this.invalidate();
		this.innerPanel = p;
		this.getContentPane().add(this.innerPanel);
		//this.invalidate();
		this.revalidate();
		this.repaint();
	}
}
