package projekt.gui.options;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;

import projekt.enums.EStamps;

@SuppressWarnings("serial")
public class JPanel_RadioStampButton extends JPanel
{
	private JRadioButton radioButton;
	private JLabel lblNewLabel;
	private EStamps stamp;

	/**
	 * @wbp.parser.constructor
	 */
	public JPanel_RadioStampButton(EStamps stamp, boolean selected)
	{
		this.stamp = stamp;
		this.init();
		this.radioButton.setSelected(selected);
	}

	private void init()
	{
		this.setBorder(new EmptyBorder(3, 3, 3, 3));
		this.setPreferredSize(new Dimension(240, 38));
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JPanel_RadioStampButton.this.radioButton.doClick();
			}
		});
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{21, 34, 10, 0};
		gridBagLayout.rowHeights = new int[]{38, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		this.setLayout(gridBagLayout);
		
		this.radioButton = new JRadioButton("");
		this.radioButton.setBorder(new EmptyBorder(0, 6, 0, 6));
		GridBagConstraints gbc_radioButton = new GridBagConstraints();
		gbc_radioButton.anchor = GridBagConstraints.WEST;
		gbc_radioButton.fill = GridBagConstraints.VERTICAL;
		gbc_radioButton.insets = new Insets(0, 0, 0, 0);
		gbc_radioButton.gridx = 0;
		gbc_radioButton.gridy = 0;
		this.add(this.radioButton, gbc_radioButton);
		
		this.lblNewLabel = new JLabel(this.stamp.getName());
		this.lblNewLabel.setIcon(this.stamp.getIcon());
		this.lblNewLabel.setBorder(new EmptyBorder(0, 0, 0, 6));
		this.lblNewLabel.setIconTextGap(6);
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.fill = GridBagConstraints.VERTICAL;
		gbc_lblNewLabel.insets = new Insets(0, 0, 0, 0);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 0;
		this.add(this.lblNewLabel, gbc_lblNewLabel);
		this.lblNewLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JPanel_RadioStampButton.this.radioButton.doClick();
			}
		});
	}
	
	public void addThisToRadioButtonGroup(ButtonGroup group)
	{
		group.add(this.radioButton);
	}
	
	public boolean isSelected()
	{
		return this.radioButton.isSelected();
	}
	public void setSelected(boolean b)
	{
		this.radioButton.setSelected(b);
	}
	public void toggleSelected()
	{
		this.radioButton.setSelected(!this.radioButton.isSelected());
	}
	public EStamps getCorespondingStampType()
	{
		return this.stamp;
	}

}
