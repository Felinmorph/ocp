package projekt.gui.options;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import projekt.enums.ESizeGameField;

@SuppressWarnings("serial")
public class SubPanel_Size extends JPanel
{
	private JRadioButton[] sizes;

	public SubPanel_Size()
	{
		this.init();
	}

	private void init()
	{
		this.setLayout(new BorderLayout(0, 0));
		this.setPreferredSize(new Dimension(400, 120));

		JPanel panel_Header = new JPanel();
		this.add(panel_Header, BorderLayout.NORTH);
		panel_Header.setLayout(new BorderLayout(0, 0));

		JLabel lblSchwierigkeitsgrad = new JLabel("Spielfeldgröße");
		panel_Header.add(lblSchwierigkeitsgrad);
		lblSchwierigkeitsgrad.setToolTipText("");
		lblSchwierigkeitsgrad.setOpaque(true);
		lblSchwierigkeitsgrad.setBackground(Color.LIGHT_GRAY);
		lblSchwierigkeitsgrad.setHorizontalAlignment(SwingConstants.CENTER);
		lblSchwierigkeitsgrad.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblSchwierigkeitsgrad.setBorder(new EmptyBorder(6, 6, 6, 6));

		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBorder(new EmptyBorder(6, 6, 6, 6));
		lblNewLabel_1.setToolTipText("Die Spielfeldgröße bestimmt die Anzahl an Spielfeldern auf dem Spielbrett.");
		lblNewLabel_1.setOpaque(true);
		lblNewLabel_1.setBackground(Color.LIGHT_GRAY);
		lblNewLabel_1.setIcon(new ImageIcon(PanelOptions.class.getResource("/projekt/images/Fragezeichen16.png")));
		panel_Header.add(lblNewLabel_1, BorderLayout.EAST);

		JPanel panel_Options = new JPanel();
		panel_Options.setBorder(new EmptyBorder(6, 6, 6, 6));
		this.add(panel_Options, BorderLayout.CENTER);
		panel_Options.setLayout(new BoxLayout(panel_Options, BoxLayout.Y_AXIS));

		ButtonGroup group = new ButtonGroup();

		this.sizes = new JRadioButton[ESizeGameField.values().length];

		for (int i = 0; i < ESizeGameField.values().length; i++)
		{
			this.sizes[i] = new JRadioButton(ESizeGameField.values()[i].toString(), (i == 1));
			this.sizes[i].setBorder(new EmptyBorder(6, 6, 6, 6));
			group.add(this.sizes[i]);
			panel_Options.add(this.sizes[i]);
		}
	}

	public ESizeGameField getSelectedSize()
	{
		for (int i = 0; i < this.sizes.length; i++)
		{
			if (this.sizes[i].isSelected())
				return ESizeGameField.values()[i];
		}
		return ESizeGameField.NORMAL;	// default
	}

}
