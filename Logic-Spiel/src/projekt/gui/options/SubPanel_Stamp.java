package projekt.gui.options;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import projekt.enums.EStamps;

@SuppressWarnings("serial")
public class SubPanel_Stamp extends JPanel
{
	
	private JPanel_RadioStampButton[] stamps;

	public SubPanel_Stamp()
	{
		this.init();
	}

	private void init()
	{
		this.setLayout(new BorderLayout(0, 0));
		this.setPreferredSize(new Dimension(400, 150));
		
		JPanel panel_Header = new JPanel();
		this.add(panel_Header, BorderLayout.NORTH);
		panel_Header.setLayout(new BorderLayout(0, 0));
		
		JLabel lblSchwierigkeitsgrad = new JLabel("Stempelform");
		panel_Header.add(lblSchwierigkeitsgrad);
		lblSchwierigkeitsgrad.setToolTipText("");
		lblSchwierigkeitsgrad.setOpaque(true);
		lblSchwierigkeitsgrad.setBackground(Color.LIGHT_GRAY);
		lblSchwierigkeitsgrad.setHorizontalAlignment(SwingConstants.CENTER);
		lblSchwierigkeitsgrad.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblSchwierigkeitsgrad.setBorder(new EmptyBorder(6, 6, 6, 6));
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBorder(new EmptyBorder(6, 6, 6, 6));
		lblNewLabel_1.setToolTipText("Die Stempelform gibt an welche Spielsteine gedreht werden.");
		lblNewLabel_1.setOpaque(true);
		lblNewLabel_1.setBackground(Color.LIGHT_GRAY);
		lblNewLabel_1.setIcon(new ImageIcon(PanelOptions.class.getResource("/projekt/images/Fragezeichen16.png")));
		panel_Header.add(lblNewLabel_1, BorderLayout.EAST);
		
		JPanel panel_Options = new JPanel();
		panel_Options.setBorder(new EmptyBorder(6, 6, 6, 6));
		this.add(panel_Options, BorderLayout.CENTER);
		panel_Options.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 6));
		
		ButtonGroup group = new ButtonGroup();
		
		this.stamps = new JPanel_RadioStampButton[EStamps.values().length];
		
		for (int i = 0; i < EStamps.values().length; i++)
		{

			this.stamps[i] = new JPanel_RadioStampButton(EStamps.values()[i], (i==0));
			this.stamps[i].addThisToRadioButtonGroup(group);
			GridBagConstraints gbc_stamp = new GridBagConstraints();
			gbc_stamp.anchor = GridBagConstraints.WEST;
			gbc_stamp.gridx = 0;
			gbc_stamp.gridy = i;
			panel_Options.add(this.stamps[i], gbc_stamp);
			
		}
		
	}
	
	public EStamps getSelectedStampType()
	{
		for (int i = 0; i < this.stamps.length; i++)
		{
			if(this.stamps[i].isSelected())
				return this.stamps[i].getCorespondingStampType();
		}
		return EStamps.SQUARE_FILLED;	// default
	}

}
