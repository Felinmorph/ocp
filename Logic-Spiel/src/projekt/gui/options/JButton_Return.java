package projekt.gui.options;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class JButton_Return extends JButton
{

	public JButton_Return()
	{
		super();
	}

	public JButton_Return(Icon icon)
	{
		super(icon);
	}

	public JButton_Return(String text)
	{
		super(text);
	}

	public JButton_Return(Action a)
	{
		super(a);
	}

	public JButton_Return(String text, Icon icon)
	{
		super(text, icon);
	}

}
