package projekt.gui.options;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import projekt.enums.EDificulty;
import projekt.enums.ESizeGameField;
import projekt.enums.EStamps;

@SuppressWarnings("serial")
public class PanelOptions extends JPanel
{
	private SubPanel_Dificulty panel_Dificulty;
	private SubPanel_Stamp panel_Stamp;
	private SubPanel_Size panel_Size;

	public PanelOptions()
	{
		this.setLayout(new BorderLayout(0, 0));

		JLabel label_Header = new JLabel("- Otionen -");
		label_Header.setBorder(new EmptyBorder(6, 6, 6, 6));
		label_Header.setHorizontalAlignment(SwingConstants.CENTER);
		label_Header.setFont(new Font("Tahoma", Font.BOLD, 24));
		this.add(label_Header, BorderLayout.NORTH);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		this.add(scrollPane, BorderLayout.CENTER);

		JPanel panel_Content = new JPanel();
		//panel_Content.setPreferredSize(new Dimension(450, 450));
		panel_Content.setBorder(new EmptyBorder(6, 6, 6, 6));
		//scrollPane.setViewportView(panel_Content);
		scrollPane.setColumnHeaderView(panel_Content);
		panel_Content.setLayout(new BoxLayout(panel_Content, BoxLayout.Y_AXIS));

		//------------------------------------------------------------------

		this.panel_Dificulty = new SubPanel_Dificulty();
		panel_Content.add(this.panel_Dificulty);

		JPanel panel_Spacer1 = new JPanel();
		panel_Content.add(panel_Spacer1);

		this.panel_Stamp = new SubPanel_Stamp();
		panel_Content.add(this.panel_Stamp);

		JPanel panel_Spacer2 = new JPanel();
		panel_Content.add(panel_Spacer2);

		this.panel_Size = new SubPanel_Size();
		panel_Content.add(this.panel_Size);

		JPanel panel_1 = new JPanel();
		panel_Content.add(panel_1);
		//------------------------------------------------------------------

		JPanel panel = new JPanel();
		this.add(panel, BorderLayout.SOUTH);

		JButton btnNewButton = new JButton_Return("OK");
		panel.add(btnNewButton);
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton.setPreferredSize(new Dimension(120, 30));
		btnNewButton.setMinimumSize(new Dimension(120, 30));
		btnNewButton.setSize(new Dimension(120, 30));
	}

	public EStamps getSelectedStamp()
	{
		return this.panel_Stamp.getSelectedStampType();
	}

	public EDificulty getSelectedDificulty()
	{
		return this.panel_Dificulty.getSelectedDificulty();
	}

	public ESizeGameField getSelectedSize()
	{
		return this.panel_Size.getSelectedSize();
	}
}
