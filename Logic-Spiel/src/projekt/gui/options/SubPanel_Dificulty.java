package projekt.gui.options;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import projekt.enums.EDificulty;

@SuppressWarnings("serial")
public class SubPanel_Dificulty extends JPanel
{
	private JRadioButton radioButton_Easy;
	private JRadioButton radioButton_Normal;
	private JRadioButton radioButton_Hard;

	public SubPanel_Dificulty()
	{
		this.init();
	}

	private void init()
	{
		this.setLayout(new BorderLayout(0, 0));
		this.setPreferredSize(new Dimension(400, 120));

		JPanel panel_Header = new JPanel();
		this.add(panel_Header, BorderLayout.NORTH);
		panel_Header.setLayout(new BorderLayout(0, 0));

		JLabel lblSchwierigkeitsgrad = new JLabel("Schwierigkeitsgrad");
		panel_Header.add(lblSchwierigkeitsgrad);
		lblSchwierigkeitsgrad.setToolTipText("");
		lblSchwierigkeitsgrad.setOpaque(true);
		lblSchwierigkeitsgrad.setBackground(Color.LIGHT_GRAY);
		lblSchwierigkeitsgrad.setHorizontalAlignment(SwingConstants.CENTER);
		lblSchwierigkeitsgrad.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblSchwierigkeitsgrad.setBorder(new EmptyBorder(6, 6, 6, 6));

		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBorder(new EmptyBorder(6, 6, 6, 6));
		lblNewLabel_1.setToolTipText("Der Schwierigkeitsgrad bestimmt das Start-Level sowie die Anzahl der Bonusz\u00FCge");
		lblNewLabel_1.setOpaque(true);
		lblNewLabel_1.setBackground(Color.LIGHT_GRAY);
		lblNewLabel_1.setIcon(new ImageIcon(PanelOptions.class.getResource("/projekt/images/Fragezeichen16.png")));
		panel_Header.add(lblNewLabel_1, BorderLayout.EAST);

		JPanel panel_Options = new JPanel();
		panel_Options.setBorder(new EmptyBorder(6, 6, 6, 6));
		this.add(panel_Options, BorderLayout.CENTER);
		panel_Options.setLayout(new BoxLayout(panel_Options, BoxLayout.Y_AXIS));

		ButtonGroup group = new ButtonGroup();

		this.radioButton_Easy = new JRadioButton("Leicht");
		group.add(this.radioButton_Easy);
		this.radioButton_Easy.setBorder(new EmptyBorder(6, 6, 6, 6));
		panel_Options.add(this.radioButton_Easy);

		this.radioButton_Normal = new JRadioButton("Normal");
		group.add(this.radioButton_Normal);
		this.radioButton_Normal.setBorder(new EmptyBorder(6, 6, 6, 6));
		panel_Options.add(this.radioButton_Normal);
		this.radioButton_Hard = new JRadioButton("Schwer");
		group.add(this.radioButton_Hard);
		this.radioButton_Hard.setBorder(new EmptyBorder(6, 6, 6, 6));
		panel_Options.add(this.radioButton_Hard);

		this.radioButton_Normal.setSelected(true);
	}

	public EDificulty getSelectedDificulty()
	{
		if (this.radioButton_Hard.isSelected())
			return EDificulty.HARD;
		if (this.radioButton_Normal.isSelected())
			return EDificulty.NORMAL;
		return EDificulty.EASY;
	}

}
