package projekt.gui.game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import projekt.model.GameSettings;

@SuppressWarnings("serial")
public class PanelGameBoard extends JPanel
{
	private int panelWidth = 729;
	private int panelHeight = 489;
	private int numberOfFields_X = 15;
	private int numberOfFields_Y = 10;
	private int fieldSegmentSize = 48;
	private int fieldSize = 40;
	private int fieldBorderSize = 4;
	private int border_X = 0;
	private int border_Y = 0;

	private final static double BORDER_TO_FIELD_RATIO = 1.0/15.0;
	
	private Color color1;			// prepare for theming
	private Color color2;
	private Color colorBorder;

	private boolean[][] panels;

	private BufferedImage bImage;

	public PanelGameBoard()
	{
		this.initPanel();

		// notify us when we have to redraw manually...
		this.addComponentListener(new ComponentListener() {

			@Override
			public void componentShown(ComponentEvent e)
			{
				PanelGameBoard.this.drawOnImage();
			}

			@Override
			public void componentResized(ComponentEvent e)
			{
				PanelGameBoard.this.drawOnImage();
			}

			@Override
			public void componentMoved(ComponentEvent e)
			{
			}

			@Override
			public void componentHidden(ComponentEvent e)
			{
			}
		});
	}

	private void initPanel()
	{
		this.panels = new boolean[this.numberOfFields_Y][this.numberOfFields_X];
		this.color1 = Color.LIGHT_GRAY;
		this.color2 = Color.DARK_GRAY;
		this.colorBorder = Color.BLACK;

		this.setBorder(new EmptyBorder(6, 6, 6, 6));
		this.setLayout(null);							// no need fuer a layoutmanager here
		this.calculateDrawingSizes();
	}

	public void reInit(GameSettings gs)
	{
		this.numberOfFields_X = gs.getSize().getSizeX();
		this.numberOfFields_Y = gs.getSize().getSizeY();
		this.initPanel();
	}

	public void drawOnImage()
	{
		this.calculateDrawingSizes();

		this.bImage = new BufferedImage(this.panelWidth, this.panelHeight, BufferedImage.TYPE_INT_RGB);
		Graphics g = this.bImage.getGraphics();

		//g.setColor(Color.white);
		g.setColor(new Color(240, 240, 240));
		g.fillRect(0, 0, this.panelWidth, this.panelHeight);

		for (int y = 0; y < this.panels.length; y++)
		{
			for (int x = 0; x < this.panels[y].length; x++)
			{
				int px = this.border_X + x * this.fieldSegmentSize + this.fieldBorderSize;
				int py = this.border_Y + y * this.fieldSegmentSize + this.fieldBorderSize;
				if (this.panels[y][x])
					g.setColor(this.color2);
				else
					g.setColor(this.color1);
				g.fillRect(px, py, this.fieldSize, this.fieldSize);
				g.setColor(this.colorBorder);
				g.drawRect(px, py, this.fieldSize, this.fieldSize);
			}
		}
		g.dispose();
		this.revalidate();
		this.repaint();
	}

	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		g.drawImage(this.bImage, 0, 0, null);
	}

	private void calculateDrawingSizes()
	{
		this.panelHeight = this.getHeight();
		this.panelWidth = this.getWidth();
		// gr�sse der quadrate die in der geforderten anzahl aufs spielfeld passen
		this.fieldSegmentSize = Math.max(5, Math.min(this.panelHeight / this.numberOfFields_Y, this.panelWidth / this.numberOfFields_X));	// min 5px !
		// verbleibender rand bei nicht passendem seitenverhaeltnis
		this.border_X = (this.panelWidth - (this.fieldSegmentSize * this.numberOfFields_X)) / 2;
		this.border_Y = (this.panelHeight - (this.fieldSegmentSize * this.numberOfFields_Y)) / 2;

		//groesse des rahmens um die eigendlichen spielfelder
		this.fieldBorderSize = Math.max(1, (int) (this.fieldSegmentSize * BORDER_TO_FIELD_RATIO));	// min 1px!
		this.fieldSize = this.fieldSegmentSize - this.fieldBorderSize - this.fieldBorderSize;
	}

	public void updateGrid(boolean[][] panels)
	{
		this.panels = panels;
		this.drawOnImage();
		this.repaint();
	}

	public void resetColors()
	{
		this.color1 = Color.LIGHT_GRAY;
		this.color2 = Color.DARK_GRAY;
		this.colorBorder = Color.BLACK;
	}

	public void markBoardAsDoneGood()
	{
		this.color1 = Color.GREEN;
		this.color2 = Color.GREEN;
		this.colorBorder = new Color(0, 64, 0);
		this.drawOnImage();
		this.repaint();
	}

	public void markBoardAsDoneOK()
	{
		this.color1 = Color.YELLOW;
		this.color2 = Color.YELLOW;
		this.colorBorder = new Color(64, 64, 0);
		this.drawOnImage();
		this.repaint();
	}

	public void markBoardAsDoneBad()
	{
		this.color1 = Color.RED;
		this.color2 = Color.RED;
		this.colorBorder = new Color(64, 0, 0);
		this.drawOnImage();
		this.repaint();
	}

	public Point getIndexFromScreenPoint(Point p)
	{
		// get segment
		int inSegment_X = (p.x - this.border_X) % this.fieldSegmentSize;
		int inSegment_Y = (p.y - this.border_Y) % this.fieldSegmentSize;
		if (inSegment_X < this.fieldBorderSize || inSegment_Y < this.fieldBorderSize || inSegment_X > this.fieldSize + this.fieldBorderSize
				|| inSegment_Y > this.fieldSize + this.fieldBorderSize)
		{
			// click was in segment but outside the panel
			return null;
		}
		int segment_X = (p.x - this.border_X) / this.fieldSegmentSize;
		int segment_Y = (p.y - this.border_Y) / this.fieldSegmentSize;
		if (segment_X < 0 || segment_Y < 0 || segment_X >= this.numberOfFields_X || segment_Y >= this.numberOfFields_Y)
		{
			// click was on the border somewhere outside the actual game field...
			return null;
		}

		//System.out.println("Index: " + segment_X + "," + segment_Y);
		Point p2 = new Point(segment_X, segment_Y);
		return p2;
	}

}
