package projekt.gui.game;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class PanelHighScoreEntry extends JPanel
{

	private JLabel label_Place;
	private JLabel label_Score;
	private JLabel label_Name;
	private JLabel lblNewLabel;

	public PanelHighScoreEntry()
	{
		this.setBackground(Color.WHITE);
		this.init();
	}

	private void init()
	{
		// Auto-generated...
		this.setBorder(new EmptyBorder(12, 12, 12, 12));
		//this.setPreferredSize(new Dimension(266, 61));
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

		this.label_Place = new JLabel("Platz 0 :");
		this.label_Place.setBorder(new EmptyBorder(3, 3, 3, 12));
		this.label_Place.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.label_Place.setFont(new Font("Tahoma", Font.BOLD, 14));
		this.add(this.label_Place);

		this.label_Score = new JLabel("999");
		this.label_Score.setBorder(new EmptyBorder(3, 3, 3, 3));
		this.label_Score.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.label_Score.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.add(this.label_Score);
		
		this.lblNewLabel = new JLabel("->");
		this.lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.lblNewLabel.setBorder(new EmptyBorder(3, 3, 3, 3));
		this.add(this.lblNewLabel);

		this.label_Name = new JLabel("Max Mustermann");
		this.label_Name.setBorder(new EmptyBorder(3, 3, 3, 3));
		this.label_Name.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.label_Name.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.add(this.label_Name);
	}

	public void setValues(int place, String score, String name)
	{
		this.label_Place.setText("Platz " + place + ": ");
		this.label_Score.setText("" + score);
		this.label_Name.setText(name);
		this.doLayout();
	}
}
