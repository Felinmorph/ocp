package projekt.gui.game;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class PanelHighScoreEntry2 extends JPanel
{
	JLabel[] labels_Places;
	JLabel[] labels_Scores;
	JLabel[] labels_Names;

	public PanelHighScoreEntry2()
	{
		this.labels_Places = new JLabel[5];
		this.labels_Scores = new JLabel[5];
		this.labels_Names = new JLabel[5];


		this.init();
	}

	private void init()
	{
		// Auto-generated...
		this.setBorder(new EmptyBorder(6, 6, 6, 6));
		this.setPreferredSize(new Dimension(500, 400));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 70, 65, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		this.setLayout(gridBagLayout);

		
		JLabel lblNewLabel = new JLabel("Platz 1:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 3, 6);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		this.add(lblNewLabel, gbc_lblNewLabel);

		JLabel lblNewLabel_5 = new JLabel("Score:");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblNewLabel_5 = new GridBagConstraints();
		gbc_lblNewLabel_5.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_5.insets = new Insets(0, 0, 3, 3);
		gbc_lblNewLabel_5.gridx = 1;
		gbc_lblNewLabel_5.gridy = 0;
		this.add(lblNewLabel_5, gbc_lblNewLabel_5);

		JLabel lblNewLabel_1 = new JLabel("999");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 3, 0);
		gbc_lblNewLabel_1.gridx = 2;
		gbc_lblNewLabel_1.gridy = 0;
		this.add(lblNewLabel_1, gbc_lblNewLabel_1);

		JLabel lblNewLabel_6 = new JLabel("Name:");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblNewLabel_6 = new GridBagConstraints();
		gbc_lblNewLabel_6.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_6.insets = new Insets(0, 0, 12, 3);
		gbc_lblNewLabel_6.gridx = 1;
		gbc_lblNewLabel_6.gridy = 1;
		this.add(lblNewLabel_6, gbc_lblNewLabel_6);

		JLabel lblNewLabel_2 = new JLabel("Max Mustermann");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 12, 0);
		gbc_lblNewLabel_2.gridx = 2;
		gbc_lblNewLabel_2.gridy = 1;
		this.add(lblNewLabel_2, gbc_lblNewLabel_2);

		JLabel lblPlatz_3 = new JLabel("Platz 2:");
		lblPlatz_3.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblPlatz_3 = new GridBagConstraints();
		gbc_lblPlatz_3.anchor = GridBagConstraints.WEST;
		gbc_lblPlatz_3.insets = new Insets(0, 0, 3, 6);
		gbc_lblPlatz_3.gridx = 0;
		gbc_lblPlatz_3.gridy = 2;
		this.add(lblPlatz_3, gbc_lblPlatz_3);

		JLabel label = new JLabel("Score:");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.anchor = GridBagConstraints.WEST;
		gbc_label.insets = new Insets(0, 0, 3, 3);
		gbc_label.gridx = 1;
		gbc_label.gridy = 2;
		this.add(label, gbc_label);

		JLabel label_8 = new JLabel("999");
		label_8.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_8 = new GridBagConstraints();
		gbc_label_8.anchor = GridBagConstraints.WEST;
		gbc_label_8.insets = new Insets(0, 0, 3, 0);
		gbc_label_8.gridx = 2;
		gbc_label_8.gridy = 2;
		this.add(label_8, gbc_label_8);

		JLabel label_4 = new JLabel("Name:");
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.anchor = GridBagConstraints.WEST;
		gbc_label_4.insets = new Insets(0, 0, 12, 3);
		gbc_label_4.gridx = 1;
		gbc_label_4.gridy = 3;
		this.add(label_4, gbc_label_4);

		JLabel label_12 = new JLabel("Max Mustermann");
		label_12.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_12 = new GridBagConstraints();
		gbc_label_12.anchor = GridBagConstraints.WEST;
		gbc_label_12.insets = new Insets(0, 0, 12, 0);
		gbc_label_12.gridx = 2;
		gbc_label_12.gridy = 3;
		this.add(label_12, gbc_label_12);

		JLabel lblPlatz_2 = new JLabel("Platz 3:");
		lblPlatz_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblPlatz_2 = new GridBagConstraints();
		gbc_lblPlatz_2.anchor = GridBagConstraints.WEST;
		gbc_lblPlatz_2.insets = new Insets(0, 0, 3, 6);
		gbc_lblPlatz_2.gridx = 0;
		gbc_lblPlatz_2.gridy = 4;
		this.add(lblPlatz_2, gbc_lblPlatz_2);

		JLabel label_1 = new JLabel("Score:");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.anchor = GridBagConstraints.WEST;
		gbc_label_1.insets = new Insets(0, 0, 3, 3);
		gbc_label_1.gridx = 1;
		gbc_label_1.gridy = 4;
		this.add(label_1, gbc_label_1);

		JLabel label_9 = new JLabel("999");
		label_9.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_9 = new GridBagConstraints();
		gbc_label_9.anchor = GridBagConstraints.WEST;
		gbc_label_9.insets = new Insets(0, 0, 3, 0);
		gbc_label_9.gridx = 2;
		gbc_label_9.gridy = 4;
		this.add(label_9, gbc_label_9);

		JLabel label_5 = new JLabel("Name:");
		label_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.anchor = GridBagConstraints.WEST;
		gbc_label_5.insets = new Insets(0, 0, 12, 3);
		gbc_label_5.gridx = 1;
		gbc_label_5.gridy = 5;
		this.add(label_5, gbc_label_5);

		JLabel label_13 = new JLabel("Max Mustermann");
		label_13.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_13 = new GridBagConstraints();
		gbc_label_13.anchor = GridBagConstraints.WEST;
		gbc_label_13.insets = new Insets(0, 0, 12, 0);
		gbc_label_13.gridx = 2;
		gbc_label_13.gridy = 5;
		this.add(label_13, gbc_label_13);

		JLabel lblPlatz_1 = new JLabel("Platz 4:");
		lblPlatz_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblPlatz_1 = new GridBagConstraints();
		gbc_lblPlatz_1.anchor = GridBagConstraints.WEST;
		gbc_lblPlatz_1.insets = new Insets(0, 0, 3, 6);
		gbc_lblPlatz_1.gridx = 0;
		gbc_lblPlatz_1.gridy = 6;
		this.add(lblPlatz_1, gbc_lblPlatz_1);

		JLabel label_2 = new JLabel("Score:");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.anchor = GridBagConstraints.WEST;
		gbc_label_2.insets = new Insets(0, 0, 3, 3);
		gbc_label_2.gridx = 1;
		gbc_label_2.gridy = 6;
		this.add(label_2, gbc_label_2);

		JLabel label_10 = new JLabel("999");
		label_10.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_10 = new GridBagConstraints();
		gbc_label_10.anchor = GridBagConstraints.WEST;
		gbc_label_10.insets = new Insets(0, 0, 3, 0);
		gbc_label_10.gridx = 2;
		gbc_label_10.gridy = 6;
		this.add(label_10, gbc_label_10);

		JLabel label_6 = new JLabel("Name:");
		label_6.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_6 = new GridBagConstraints();
		gbc_label_6.anchor = GridBagConstraints.WEST;
		gbc_label_6.insets = new Insets(0, 0, 12, 3);
		gbc_label_6.gridx = 1;
		gbc_label_6.gridy = 7;
		this.add(label_6, gbc_label_6);

		JLabel label_14 = new JLabel("Max Mustermann");
		label_14.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_14 = new GridBagConstraints();
		gbc_label_14.anchor = GridBagConstraints.WEST;
		gbc_label_14.insets = new Insets(0, 0, 12, 0);
		gbc_label_14.gridx = 2;
		gbc_label_14.gridy = 7;
		this.add(label_14, gbc_label_14);

		JLabel lblPlatz = new JLabel("Platz 5:");
		lblPlatz.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblPlatz = new GridBagConstraints();
		gbc_lblPlatz.anchor = GridBagConstraints.WEST;
		gbc_lblPlatz.insets = new Insets(0, 0, 3, 6);
		gbc_lblPlatz.gridx = 0;
		gbc_lblPlatz.gridy = 8;
		this.add(lblPlatz, gbc_lblPlatz);

		JLabel label_3 = new JLabel("Score:");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.anchor = GridBagConstraints.WEST;
		gbc_label_3.insets = new Insets(0, 0, 3, 3);
		gbc_label_3.gridx = 1;
		gbc_label_3.gridy = 8;
		this.add(label_3, gbc_label_3);

		JLabel label_11 = new JLabel("999");
		label_11.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_11 = new GridBagConstraints();
		gbc_label_11.anchor = GridBagConstraints.WEST;
		gbc_label_11.insets = new Insets(0, 0, 3, 0);
		gbc_label_11.gridx = 2;
		gbc_label_11.gridy = 8;
		this.add(label_11, gbc_label_11);

		JLabel label_7 = new JLabel("Name:");
		label_7.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_7 = new GridBagConstraints();
		gbc_label_7.anchor = GridBagConstraints.WEST;
		gbc_label_7.insets = new Insets(0, 0, 12, 3);
		gbc_label_7.gridx = 1;
		gbc_label_7.gridy = 9;
		this.add(label_7, gbc_label_7);

		JLabel label_15 = new JLabel("Max Mustermann");
		label_15.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_15 = new GridBagConstraints();
		gbc_label_15.anchor = GridBagConstraints.WEST;
		gbc_label_15.insets = new Insets(0, 0, 12, 0);
		gbc_label_15.gridx = 2;
		gbc_label_15.gridy = 9;
		this.add(label_15, gbc_label_15);
	}

	public void setValues(int place, int score, String name)
	{

	}
}
