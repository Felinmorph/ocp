package projekt.gui.game;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class PanelGameOver extends JPanel
{
	private JTextField txtSascha;
	private JLabel label_Rank;
	private JLabel lblLevel;
	private JPanel panel_inner;

	public PanelGameOver()
	{
		this.initPanel();
	}

	private void initPanel()
	{
		this.setBorder(new EmptyBorder(6, 6, 6, 6));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 675, 0 };
		gridBagLayout.rowHeights = new int[] { 421, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		this.setLayout(gridBagLayout);

		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		this.add(panel, gbc_panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JLabel lblGameOver = new JLabel("Game Over");
		lblGameOver.setAlignmentX(0.5f);
		lblGameOver.setFont(new Font("Tahoma", Font.BOLD, 60));
		panel.add(lblGameOver);

		this.lblLevel = new JLabel("Level: 99");
		this.lblLevel.setBorder(new EmptyBorder(6, 0, 0, 0));
		this.lblLevel.setAlignmentX(0.5f);
		this.lblLevel.setFont(new Font("Tahoma", Font.PLAIN, 32));
		panel.add(this.lblLevel);

		this.panel_inner = new JPanel();
		this.panel_inner.setBorder(new EmptyBorder(24, 0, 6, 0));
		panel.add(this.panel_inner);
		this.panel_inner.setLayout(new BoxLayout(this.panel_inner, BoxLayout.Y_AXIS));

		this.label_Rank = new JLabel("This ranks you at place: 3");
		this.label_Rank.setBorder(new CompoundBorder(new LineBorder(new Color(0, 0, 0)), new EmptyBorder(3, 3, 3, 3)));
		this.label_Rank.setFont(new Font("Tahoma", Font.PLAIN, 18));
		this.label_Rank.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.panel_inner.add(this.label_Rank);

		JLabel lblPleaseEnterYour = new JLabel("Please enter Your Name:");
		lblPleaseEnterYour.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPleaseEnterYour.setBorder(new EmptyBorder(12, 0, 6, 0));
		lblPleaseEnterYour.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.panel_inner.add(lblPleaseEnterYour);
		
				this.txtSascha = new JTextField();
				this.txtSascha.setHorizontalAlignment(SwingConstants.CENTER);
				this.txtSascha.setFont(new Font("Tahoma", Font.PLAIN, 16));
				this.panel_inner.add(this.txtSascha);
				this.txtSascha.setColumns(10);
	}

	public void init(int score, int place)
	{
		this.panel_inner.setVisible((place < 6));
		this.lblLevel.setText("geschafte Level: " + score);
		this.label_Rank.setText("Damit bist du in der Highscore auf Platz: " + place);
		this.txtSascha.setName("");
		this.doLayout();
	}

	public String getEnteredName()
	{
		return this.txtSascha.getText();
	}

}
