package projekt.gui.game;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class JButton_SaveHighScore extends JButton
{

	public JButton_SaveHighScore()
	{
		super();
	}

	public JButton_SaveHighScore(Icon icon)
	{
		super(icon);
	}

	public JButton_SaveHighScore(String text)
	{
		super(text);
	}

	public JButton_SaveHighScore(Action a)
	{
		super(a);
	}

	public JButton_SaveHighScore(String text, Icon icon)
	{
		super(text, icon);
	}

}
