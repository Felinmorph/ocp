package projekt.gui.game;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class JButton_GiveUp extends JButton
{

	public JButton_GiveUp()
	{
		super();
	}

	public JButton_GiveUp(Icon icon)
	{
		super(icon);
	}

	public JButton_GiveUp(String text)
	{
		super(text);
	}

	public JButton_GiveUp(Action a)
	{
		super(a);
	}

	public JButton_GiveUp(String text, Icon icon)
	{
		super(text, icon);
	}

}
