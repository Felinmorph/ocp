package projekt.gui.game;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import projekt.model.GameSettings;

@SuppressWarnings("serial")
public class PanelGame extends JPanel
{
	private PanelGameBoard panelGameBoard;
	private PanelGameOver panelGameOver;
	private JPanel panel_Holder;
	private CardLayout cl;
	private JButton_Retry button_Retry;
	private JButton_GiveUp button_Back;
	private JButton_HS_Ok button_Ok;

	private boolean gameOverPanelShown = false;

	public PanelGame()
	{
		this.panelGameBoard = new PanelGameBoard();
		this.panelGameOver = new PanelGameOver();
		this.init();
	}

	private void init()
	{
		this.setBorder(new EmptyBorder(6, 6, 6, 6));
		this.setPreferredSize(new Dimension(800, 720));
		this.setLayout(new BorderLayout(0, 0));

		JPanel panel = new PanelGameHeader();
		this.add(panel, BorderLayout.NORTH);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EmptyBorder(6, 6, 6, 6));
		this.add(panel_1, BorderLayout.SOUTH);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 180, 50, 180, 50, 180, 0 };
		gbl_panel_1.rowHeights = new int[] { 25, 0 };
		gbl_panel_1.columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		Dimension stdButtonSize = new Dimension(180, 25);
		
		this.button_Back = new JButton_GiveUp("Abbruch / Hauptmenue");
		this.button_Back.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.button_Back.setMinimumSize(stdButtonSize);
		this.button_Back.setMaximumSize(stdButtonSize);
		this.button_Back.setPreferredSize(stdButtonSize);
		GridBagConstraints gbc_button_Back = new GridBagConstraints();
		gbc_button_Back.anchor = GridBagConstraints.NORTHWEST;
		gbc_button_Back.insets = new Insets(0, 0, 0, 5);
		gbc_button_Back.gridx = 0;
		gbc_button_Back.gridy = 0;
		panel_1.add(this.button_Back, gbc_button_Back);

		this.button_Ok = new JButton_HS_Ok("OK");
		this.button_Ok.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.button_Ok.setMinimumSize(stdButtonSize);
		this.button_Ok.setMaximumSize(stdButtonSize);
		this.button_Ok.setPreferredSize(stdButtonSize);
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.fill = GridBagConstraints.VERTICAL;
		gbc_btnOk.insets = new Insets(0, 0, 0, 5);
		gbc_btnOk.gridx = 2;
		gbc_btnOk.gridy = 0;
		panel_1.add(this.button_Ok, gbc_btnOk);

		this.button_Retry = new JButton_Retry("Wiederhole Level");
		this.button_Retry.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.button_Retry.setMinimumSize(stdButtonSize);
		this.button_Retry.setMaximumSize(stdButtonSize);
		this.button_Retry.setPreferredSize(stdButtonSize);
		GridBagConstraints gbc_button_Retry = new GridBagConstraints();
		gbc_button_Retry.anchor = GridBagConstraints.NORTHEAST;
		gbc_button_Retry.gridx = 4;
		gbc_button_Retry.gridy = 0;
		panel_1.add(this.button_Retry, gbc_button_Retry);

		this.panel_Holder = new JPanel();
		this.add(this.panel_Holder, BorderLayout.CENTER);
		this.cl = new CardLayout(0, 0);
		this.panel_Holder.setLayout(this.cl);
		this.panel_Holder.add(this.panelGameBoard, "game_board");
		this.panel_Holder.add(this.panelGameOver, "game_over");

	}

	public void InitGameBoard(GameSettings gs)
	{
		this.panelGameBoard.reInit(gs);
	}

	public void showGameOverPanel(int score, int place)
	{
		this.button_Retry.setVisible(false);
		this.button_Back.setVisible(false);
		this.button_Ok.setVisible(true);
		this.panelGameOver.init(score, place);
		this.cl.last(this.panel_Holder);
		this.gameOverPanelShown = true;
		this.doLayout();
	}

	public void showGamePanel()
	{
		this.button_Retry.setVisible(true);
		this.button_Back.setVisible(true);
		this.button_Ok.setVisible(false);
		this.cl.first(this.panel_Holder);
		this.gameOverPanelShown = false;
		this.doLayout();
	}

	public String getEnteredNameForHighScore()
	{
		return this.panelGameOver.getEnteredName();
	}

	public boolean isGameOverPanelShown()
	{
		return this.gameOverPanelShown;
	}
}
