package projekt.gui.game;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class PanelGameHeader extends JPanel
{
	JLabel labelDificulty;
	JLabel labelLevel;
	JLabel labelProgress;
	private JLabel labelTrys;

	public PanelGameHeader(String dificultyText, int level, int progress, int progressMax, int score)
	{
		this.setLayout(new GridLayout(0, 4, 0, 0));

		this.labelDificulty = new JLabel("Schwierigkeitsgrad: " + dificultyText);
		this.labelLevel = new JLabel("Level: " + level);
		this.labelProgress = new JLabel("Fortschritt: " + progress + "/" + progressMax);

		this.add(this.labelDificulty);
		this.add(this.labelLevel);
		this.add(this.labelProgress);
	}

	public PanelGameHeader()
	{
		this.setBorder(new CompoundBorder(new EmptyBorder(3, 6, 3, 6), new CompoundBorder(new LineBorder(new Color(0, 0, 0)), new EmptyBorder(3, 3, 3, 3))));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 174, 0, 64, 0, 125, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 20, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		this.setLayout(gridBagLayout);

		this.labelDificulty = new JLabel("Schwierigkeitsgrad: leicht");
		this.labelDificulty.setFont(new Font("Tahoma", Font.BOLD, 14));

		GridBagConstraints gbc_labelDificulty = new GridBagConstraints();
		gbc_labelDificulty.anchor = GridBagConstraints.WEST;
		gbc_labelDificulty.insets = new Insets(0, 0, 0, 5);
		gbc_labelDificulty.gridx = 0;
		gbc_labelDificulty.gridy = 0;
		this.add(this.labelDificulty, gbc_labelDificulty);
		this.labelLevel = new JLabel("Level: 0");
		this.labelLevel.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_labelLevel = new GridBagConstraints();
		gbc_labelLevel.insets = new Insets(0, 0, 0, 5);
		gbc_labelLevel.gridx = 2;
		gbc_labelLevel.gridy = 0;
		this.add(this.labelLevel, gbc_labelLevel);
		this.labelProgress = new JLabel("Z\u00FCge: 99");
		this.labelProgress.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_labelProgress = new GridBagConstraints();
		gbc_labelProgress.insets = new Insets(0, 0, 0, 5);
		gbc_labelProgress.gridx = 4;
		gbc_labelProgress.gridy = 0;
		this.add(this.labelProgress, gbc_labelProgress);
		
		this.labelTrys = new JLabel("Versuche: 3");
		this.labelTrys.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_labelTrys = new GridBagConstraints();
		gbc_labelTrys.anchor = GridBagConstraints.EAST;
		gbc_labelTrys.gridx = 6;
		gbc_labelTrys.gridy = 0;
		this.add(this.labelTrys, gbc_labelTrys);
	}

	public void setDificulty(String dificultyText)
	{
		this.labelDificulty.setText("Schwierigkeitsgrad: " + dificultyText);
		this.doLayout();
	}

	public void setLevel(int level)
	{
		this.labelLevel.setText("Level: " + level);
		this.doLayout();
	}

	public void setProgress(int progress)
	{
		this.labelProgress.setText("Z�ge: " + progress );
		this.doLayout();
	}

	public void setTrys(int trys)
	{
		this.labelTrys.setText("Versuche: " + trys);
		this.doLayout();
	}

}
