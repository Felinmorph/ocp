package projekt.gui.game;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import projekt.enums.EDificulty;
import projekt.enums.ESizeGameField;
import projekt.enums.EStamps;

@SuppressWarnings("serial")
public class PanelHighScore extends JPanel
{
	PanelHighScoreEntry[] entrys;

	private JLabel label_Difficulty;
	private JLabel label_Size;
	private JLabel label_Stamp;

	public PanelHighScore()
	{
		this.init();
	}

	private void init()
	{
		this.setBorder(new EmptyBorder(6, 6, 6, 6));
		this.setPreferredSize(new Dimension(600, 400));
		this.setLayout(new BorderLayout(0, 0));

		JLabel label_Header = new JLabel("Highscores");
		label_Header.setHorizontalAlignment(SwingConstants.CENTER);
		label_Header.setFont(new Font("Tahoma", Font.BOLD, 24));
		label_Header.setBorder(new EmptyBorder(6, 6, 6, 6));
		this.add(label_Header, BorderLayout.NORTH);

		this.entrys = new PanelHighScoreEntry[5];

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		this.add(panel, BorderLayout.CENTER);

		JPanel panel_3 = new JPanel();
		panel.add(panel_3);
		panel_3.setBackground(Color.WHITE);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[] { 164, 0 };
		gbl_panel_3.rowHeights = new int[] { 96, 0 };
		gbl_panel_3.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_3.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel_3.setLayout(gbl_panel_3);

		JPanel panel_Content = new JPanel();
		GridBagConstraints gbc_panel_Content = new GridBagConstraints();
		gbc_panel_Content.anchor = GridBagConstraints.NORTH;
		gbc_panel_Content.gridx = 0;
		gbc_panel_Content.gridy = 0;
		panel_3.add(panel_Content, gbc_panel_Content);
		panel_Content.setBackground(Color.WHITE);
		panel_Content.setLayout(new BoxLayout(panel_Content, BoxLayout.Y_AXIS));

		JPanel panel_Settings = new JPanel();
		panel_Settings.setBackground(Color.WHITE);
		panel_Settings.setBorder(new EmptyBorder(12, 12, 12, 12));
		panel_Content.add(panel_Settings);
		panel_Settings.setLayout(new BoxLayout(panel_Settings, BoxLayout.Y_AXIS));

		JLabel lblNewLabel = new JLabel("Current Settings:");
		lblNewLabel.setBorder(new EmptyBorder(6, 6, 6, 6));
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel_Settings.add(lblNewLabel);

		this.label_Difficulty = new JLabel("Schwer");
		this.label_Difficulty.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel_Settings.add(this.label_Difficulty);

		this.label_Size = new JLabel("Gross (20x15)");
		this.label_Size.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel_Settings.add(this.label_Size);

		this.label_Stamp = new JLabel("Quadrat (Gefuellt)");
		this.label_Stamp.setAlignmentX(Component.CENTER_ALIGNMENT);
		panel_Settings.add(this.label_Stamp);

		JPanel panel_1 = new JPanel();
		this.add(panel_1, BorderLayout.SOUTH);

		JButton btnOk = new JButton("OK");
		btnOk.setPreferredSize(new Dimension(120, 30));
		btnOk.setSize(new Dimension(120, 30));
		btnOk.setMinimumSize(new Dimension(120, 30));
		btnOk.setMaximumSize(new Dimension(120, 30));
		panel_1.add(btnOk);
		for (int i = 0; i < 5; i++)
		{
			this.entrys[i] = new PanelHighScoreEntry();
			panel_Content.add(this.entrys[i]);
		}

	}

	public void setEnvironment(ESizeGameField size, EStamps stamp, EDificulty dif)
	{
		this.label_Difficulty.setText(dif.toString());
		this.label_Size.setText(size.toString());
		this.label_Stamp.setText(stamp.toString());
		this.doLayout();
	}

	public void setPlace(int place, String score, String name)
	{
		if (place < 1 || place > 5)
			return;
		this.entrys[place - 1].setValues(place, score, name);
	}
}
