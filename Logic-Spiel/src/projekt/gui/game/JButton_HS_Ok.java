package projekt.gui.game;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class JButton_HS_Ok extends JButton
{

	public JButton_HS_Ok()
	{
		super();
	}

	public JButton_HS_Ok(Icon icon)
	{
		super(icon);
	}

	public JButton_HS_Ok(String text)
	{
		super(text);
	}

	public JButton_HS_Ok(Action a)
	{
		super(a);
	}

	public JButton_HS_Ok(String text, Icon icon)
	{
		super(text, icon);
	}

}
