package projekt.gui.game;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class JButton_Retry extends JButton
{

	public JButton_Retry()
	{
		super();
	}

	public JButton_Retry(Icon icon)
	{
		super(icon);
	}

	public JButton_Retry(String text)
	{
		super(text);
	}

	public JButton_Retry(Action a)
	{
		super(a);
	}

	public JButton_Retry(String text, Icon icon)
	{
		super(text, icon);
	}

}
