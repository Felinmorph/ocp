package projekt.gui.mainMenue;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class JButton_HighScore extends JButton
{

	public JButton_HighScore()
	{
		super();
	}

	public JButton_HighScore(Action a)
	{
		super(a);
	}

	public JButton_HighScore(Icon icon)
	{
		super(icon);
	}

	public JButton_HighScore(String text, Icon icon)
	{
		super(text, icon);
	}

	public JButton_HighScore(String text)
	{
		super(text);
	}

}
