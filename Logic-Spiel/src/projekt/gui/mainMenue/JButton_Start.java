package projekt.gui.mainMenue;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class JButton_Start extends JButton
{

	public JButton_Start()
	{
		super();
	}

	public JButton_Start(Icon icon)
	{
		super(icon);
	}

	public JButton_Start(String text)
	{
		super(text);
	}

	public JButton_Start(Action a)
	{
		super(a);
	}

	public JButton_Start(String text, Icon icon)
	{
		super(text, icon);
	}

}
