package projekt.gui.mainMenue;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class JButton_Options extends JButton
{

	public JButton_Options()
	{
		super();
	}

	public JButton_Options(Action a)
	{
		super(a);
	}

	public JButton_Options(Icon icon)
	{
		super(icon);
	}

	public JButton_Options(String text, Icon icon)
	{
		super(text, icon);
	}

	public JButton_Options(String text)
	{
		super(text);
	}


}
