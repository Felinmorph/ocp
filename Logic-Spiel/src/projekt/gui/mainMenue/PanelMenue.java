package projekt.gui.mainMenue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class PanelMenue extends JPanel
{

	/**
	 * Create the panel.
	 */
	public PanelMenue()
	{
//		this.setBackground(Color.LIGHT_GRAY);
//		this.setBorder(new EmptyBorder(6, 6, 6, 6));
//		this.setPreferredSize(new Dimension(789, 600));
//		this.setLayout(new BorderLayout(0, 0));
//		
//		JLabel lblNewLabel = new JLabel("Nur ein weiteres , einfaches Logik-Spiel");
//		lblNewLabel.setBorder(new EmptyBorder(6, 6, 6, 6));
//		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
//		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 24));
//		this.add(lblNewLabel, BorderLayout.NORTH);
//		
//		JPanel panel = new JPanel();
//		this.add(panel);
		this.setLayout(new BorderLayout(0, 0));

		JLabel label_Header = new JLabel("   ");
		label_Header.setHorizontalAlignment(SwingConstants.CENTER);
		label_Header.setFont(new Font("Tahoma", Font.BOLD, 24));
		label_Header.setBorder(new EmptyBorder(6, 6, 6, 6));
		this.add(label_Header, BorderLayout.NORTH);

		JPanel panel_Centerizer = new JPanel();
		this.add(panel_Centerizer);
		GridBagLayout gbl_panel_Centerizer = new GridBagLayout();
		gbl_panel_Centerizer.columnWidths = new int[] { 530, 0 };
		gbl_panel_Centerizer.rowHeights = new int[] { 115, 0, 0 };
		gbl_panel_Centerizer.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_Centerizer.rowWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		panel_Centerizer.setLayout(gbl_panel_Centerizer);

		JPanel panel_Content = new JPanel();
		GridBagConstraints gbc_panel_Content = new GridBagConstraints();
		gbc_panel_Content.gridx = 0;
		//gbc_panel_1.insets = new Insets(0, 0, 0,0); 
		gbc_panel_Content.gridy = 0;
		panel_Centerizer.add(panel_Content, gbc_panel_Content);
		panel_Content.setSize(200, 300);
		panel_Content.setLayout(new GridLayout(3, 1, 30, 30));

		JButton button_Start = new JButton_Start("Neues Spiel");
		panel_Content.add(button_Start);
		button_Start.setMinimumSize(new Dimension(150, 75));
		button_Start.setFont(new Font("Tahoma", Font.BOLD, 16));
		button_Start.setPreferredSize(new Dimension(150, 75));

		JButton button_Options = new JButton_Options("Optionen");
		panel_Content.add(button_Options);
		button_Options.setMinimumSize(new Dimension(120, 60));
		button_Options.setFont(new Font("Tahoma", Font.PLAIN, 16));
		button_Options.setPreferredSize(new Dimension(120, 60));

		JButton button_HS = new JButton_HighScore("HighScore");
		panel_Content.add(button_HS);
		button_HS.setMinimumSize(new Dimension(150, 75));
		button_HS.setFont(new Font("Tahoma", Font.PLAIN, 16));
		button_HS.setPreferredSize(new Dimension(150, 75));

		JLabel lblBySaschaJogszat = new JLabel("by Sascha Jogszat");
		lblBySaschaJogszat.setBorder(new EmptyBorder(0, 0, 3, 6));
		GridBagConstraints gbc_lblBySaschaJogszat = new GridBagConstraints();
		gbc_lblBySaschaJogszat.anchor = GridBagConstraints.EAST;
		gbc_lblBySaschaJogszat.gridx = 0;
		gbc_lblBySaschaJogszat.gridy = 1;
		panel_Centerizer.add(lblBySaschaJogszat, gbc_lblBySaschaJogszat);

	}
}
