package projekt.control;

import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;

import projekt.gui.FrameMain;
import projekt.gui.game.JButton_GiveUp;
import projekt.gui.game.JButton_HS_Ok;
import projekt.gui.game.JButton_Retry;
import projekt.gui.game.PanelGame;
import projekt.gui.game.PanelGameBoard;
import projekt.gui.game.PanelGameHeader;
import projekt.gui.game.PanelHighScore;
import projekt.gui.mainMenue.JButton_HighScore;
import projekt.gui.mainMenue.JButton_Options;
import projekt.gui.mainMenue.JButton_Start;
import projekt.gui.mainMenue.PanelMenue;
import projekt.gui.options.JButton_Return;
import projekt.gui.options.PanelOptions;
import projekt.model.Game;
import projekt.model.GameSettings;

public class Controller
{
	private Game game;
	//private GameSettings gs;
	private FrameMain frameMain;
	private PanelMenue panelMenue;
	private PanelOptions panelOptions;
	private PanelHighScore panelHighScore;
	private PanelGame panelGame;

	private PanelGameHeader pgh;
	private PanelGameBoard pgb;

	private boolean displayInterLevelState = false;
	private int lastReachedPlace = 99;

	public Controller()
	{
		this.init();
	}

	private void init()
	{
		System.out.println("Starting...");

		// creating game
		this.game = new Game();		// just in case - remove if unused when finished!!
		//this.gs = new GameSettings();
		this.frameMain = new FrameMain();
		this.panelMenue = new PanelMenue();
		this.panelOptions = new PanelOptions();
		this.panelHighScore = new PanelHighScore();
		this.panelGame = new PanelGame();

		this.frameMain.setInnerPanel(this.panelMenue);

		this.rigUpUI();
		this.pgh.setDificulty(this.game.getDificulty().name().toLowerCase());
		this.updateHeader();

	}

	private void updateHeader()
	{
		this.pgh.setLevel(this.game.getLevel());
		this.pgh.setProgress(this.game.getZuege());
		this.pgh.setTrys(this.game.getTryCounter());
	}

	private void rigUpUI()
	{
		this.traversePanelMenue(this.panelMenue);
		this.traversePanelOptions(this.panelOptions);
		this.traversePanelHighScore(this.panelHighScore);
		this.traversePanelGame(this.panelGame);
	}

	private void traversePanelMenue(Component rootComp)
	{
		for (Component child : ((Container) rootComp).getComponents())
		{
			//System.out.println(child.getClass().getSimpleName());
			if (child instanceof JButton_HighScore)
			{
				((JButton_HighScore) child).addActionListener(this::onClickMenueButtonHighscore);
			}
			if (child instanceof JButton_Options)
			{
				((JButton_Options) child).addActionListener(this::onClickMenueButtonOptions);
			}
			if (child instanceof JButton_Start)
			{
				((JButton_Start) child).addActionListener(this::onClickMenueButtonNewGame);
			}
			this.traversePanelMenue(child);
		}
	}

	private void traversePanelOptions(Component rootComp)
	{
		for (Component child : ((Container) rootComp).getComponents())
		{
			//System.out.println(child.getClass().getSimpleName());
			if (child instanceof JButton_Return)
			{
				//System.out.println("Found!");
				((JButton_Return) child).addActionListener(this::onClickOptionsButtonBack);
			}
			this.traversePanelOptions(child);
		}
	}

	private void traversePanelHighScore(Component rootComp)
	{
		for (Component child : ((Container) rootComp).getComponents())
		{
			//System.out.println(child.getClass().getSimpleName());
			if (child instanceof JButton)
			{
				((JButton) child).addActionListener(this::onClickHSButtonBack);
			}
			this.traversePanelHighScore(child);
		}
	}

	private void traversePanelGame(Component rootComp)
	{
		for (Component child : ((Container) rootComp).getComponents())
		{
			if (child instanceof JButton_HS_Ok)
			{
				((JButton_HS_Ok) child).addActionListener(this::onClickGameButtonSaveHS);
			}
			if (child instanceof PanelGameHeader)
			{
				this.pgh = (PanelGameHeader) child;
			}
			if (child instanceof PanelGameBoard)
			{
				this.pgb = (PanelGameBoard) child;
				//pgb.addActionListener(this::onClickGameButtonBack);
				this.pgb.addMouseListener(new MouseAdapter() {
					@Override
					public void mousePressed(MouseEvent e)
					{
						Controller.this.onClickGameBoard(e.getPoint());
					}
				});
			}
			if (child instanceof JButton_GiveUp)
			{
				((JButton_GiveUp) child).addActionListener(this::onClickGameButtonBack);
			}
			if (child instanceof JButton_Retry)
			{
				((JButton_Retry) child).addActionListener(this::onClickGameButtonRetry);
			}

			this.traversePanelGame(child);
		}
	}

	public void onClickMenueButtonHighscore(ActionEvent e)
	{
		GameSettings gs = this.getCurrentGameSettings();
		this.game = new Game(gs);		//unn�tig?
		this.panelHighScore.setEnvironment(gs.getSize(), gs.getStamp(), gs.getDificulty());
		for (int i = 0; i < 5; i++)
		{
			this.panelHighScore.setPlace(i + 1, this.game.getHighScoreScoreAsString(i), this.game.getHighScoreName(i));
		}
		this.frameMain.setInnerPanel(this.panelHighScore);
	}

	private GameSettings getCurrentGameSettings()
	{
		GameSettings gs = new GameSettings();
		gs.setStamp(this.panelOptions.getSelectedStamp());
		gs.setDificulty(this.panelOptions.getSelectedDificulty());
		gs.setSize(this.panelOptions.getSelectedSize());
		return gs;
	}

	public void onClickMenueButtonOptions(ActionEvent e)
	{
		this.frameMain.setInnerPanel(this.panelOptions);
	}

	public void onClickMenueButtonNewGame(ActionEvent e)
	{
		GameSettings gs = this.getCurrentGameSettings();
		this.game = new Game(gs);
		this.panelGame.showGamePanel();
		this.frameMain.setInnerPanel(this.panelGame);
		this.panelGame.InitGameBoard(gs);
		this.pgb.updateGrid(this.game.getField());
		this.updateHeader();
	}

	public void onClickOptionsButtonBack(ActionEvent e)
	{
		this.frameMain.setInnerPanel(this.panelMenue);
	}

	public void onClickHSButtonBack(ActionEvent e)
	{
		this.frameMain.setInnerPanel(this.panelMenue);
	}

	public void onClickGameButtonBack(ActionEvent e)
	{
		this.frameMain.setInnerPanel(this.panelMenue);
	}

	public void onClickGameButtonRetry(ActionEvent e)
	{
		if (this.displayInterLevelState)	// quick and dirty solution -.-
			return;
		this.game.resetLevel();
		this.pgb.updateGrid(this.game.getField());
		this.updateHeader();
	}

	private void progressGameOver()
	{
		this.lastReachedPlace = this.game.getPossiblePlaceInHighscore();
		this.panelGame.showGameOverPanel(this.game.getLevel()-1, this.lastReachedPlace);	// -1 da das aktuelle level NICHT geschafft wurde
	}

	private void onClickGameButtonSaveHS(ActionEvent e)
	{
		if (this.lastReachedPlace < 6)	// wenn ein name eingegeben werden konnte
		{
			String name = this.panelGame.getEnteredNameForHighScore();
			if (!name.isEmpty())
			{
				this.game.insertNewHighScore(name);
			}
			this.lastReachedPlace = 99;
			this.panelHighScore.setEnvironment(this.game.getSize(), this.game.getStamp(), this.game.getDificulty());
			for (int i = 0; i < 5; i++)
			{
				this.panelHighScore.setPlace(i + 1, this.game.getHighScoreScoreAsString(i), this.game.getHighScoreName(i));
			}
			this.frameMain.setInnerPanel(this.panelHighScore);
			this.panelGame.showGamePanel();
		}
		else
		{
			this.panelGame.showGamePanel();
			this.frameMain.setInnerPanel(this.panelMenue);
		}
	}

	private void progressInterLevelClick()
	{
		this.displayInterLevelState = false;
		this.pgb.resetColors();
		if (this.game.getZuege() <= 0)
			this.game.resetLevel();
		else
			this.game.increaseLevel();
		if (this.game.isGameOver())
		{
			this.progressGameOver();
			return;
		}
		this.pgb.updateGrid(this.game.getField());
		this.updateHeader();
	}

	public void onClickGameBoard(Point p)
	{
		if (this.displayInterLevelState)
		{
			this.progressInterLevelClick();
			return;
		}

		// perform the click
		Point p2 = this.pgb.getIndexFromScreenPoint(p);
		//System.out.println("clicked on panel: " + p2);
		if (p2 != null)		// only act on legal moves...
		{
			this.game.performMove(p2.x, p2.y);
			this.pgb.updateGrid(this.game.getField());
			this.updateHeader();
		}
		// if this move was our last move:
		if (this.game.isBoardCleared())
		{
			if (this.game.getZuege() > 0)		// mit letztem zug gecleart ist noch gut
				this.pgb.markBoardAsDoneGood();
			else
				this.pgb.markBoardAsDoneOK();
			this.displayInterLevelState = true;
			return;
		}
		if (this.game.getZuege() <= 0)
		{
			this.pgb.markBoardAsDoneBad();
			this.pgh.setTrys(this.game.getTryCounter() - 1);	// trys shown should already be reduced here
			this.displayInterLevelState = true;
			return;
		}
	}

}
