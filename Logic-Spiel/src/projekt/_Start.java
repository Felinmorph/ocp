package projekt;

import java.awt.EventQueue;

import projekt.control.Controller;

public class _Start
{

	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run()
			{
				try
				{
					new Controller();
//					FrameMain frame = new FrameMain();
//					frame.setVisible(true);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

}
