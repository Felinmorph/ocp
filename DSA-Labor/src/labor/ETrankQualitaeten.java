package labor;

public enum ETrankQualitaeten
{
	v1("M"),
	v2("A"),
	v3("B"),
	v4("C"),
	v5("D"),
	v6("E"),
	v7("F");

	String name;

	private ETrankQualitaeten(String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return this.name;
	}

}
