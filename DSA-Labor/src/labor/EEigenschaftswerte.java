package labor;

public enum EEigenschaftswerte
{
	v6(6),
	v7(7),
	v8(8),
	v9(9),
	v10(10),
	v11(11),
	v12(12),
	v13(13),
	v14(14),
	v15(15),
	v16(16),
	v17(17),
	v18(18),
	v19(19),
	v20(20),
	v21(21),
	;

	int value;

	private EEigenschaftswerte(int value)
	{
		this.value = value;
	}

	@Override
	public String toString()
	{
		return "" + this.value;
	}

}
