package labor;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

@SuppressWarnings("serial")
public class Panel_Labor_Werte extends JPanel
{
	JPanel panel_Content = new JPanel();
	JButton buttonExpand;
	private JSlider slider;
	private JSpinner spinner;
	private JLabel lblAusstattung;

	/**
	 * Create the panel.
	 */
	public Panel_Labor_Werte()
	{
		this.initGUI();

	}

	private void initGUI()
	{
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 500, 0 };
		gridBagLayout.rowHeights = new int[] { 0, -1, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		this.setLayout(gridBagLayout);

		this.buttonExpand = new JButton("-");
		this.buttonExpand.addActionListener(this::expandContent);
		this.buttonExpand.setPreferredSize(new Dimension(16, 16));
		this.buttonExpand.setMinimumSize(new Dimension(16, 16));
		this.buttonExpand.setMaximumSize(new Dimension(18, 18));
		this.buttonExpand.setMargin(new Insets(0, 0, 0, 0));
		this.buttonExpand.setAlignmentX(0.5f);
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 0, 5);
		gbc_button.gridx = 0;
		gbc_button.gridy = 0;
		this.add(this.buttonExpand, gbc_button);
		
		this.lblAusstattung = new JLabel("Ausstattung:");
		this.lblAusstattung.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblAusstattung = new GridBagConstraints();
		gbc_lblAusstattung.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblAusstattung.insets = new Insets(0, 0, 0, 0);
		gbc_lblAusstattung.gridx = 1;
		gbc_lblAusstattung.gridy = 0;
		this.add(this.lblAusstattung, gbc_lblAusstattung);
		GridBagConstraints gbc_panel_Content = new GridBagConstraints();
		gbc_panel_Content.fill = GridBagConstraints.BOTH;
		gbc_panel_Content.gridx = 1;
		gbc_panel_Content.gridy = 1;
		this.panel_Content.setBorder(new CompoundBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), new EmptyBorder(3, 3, 3, 3)));
		this.panel_Content.setAlignmentY(Component.TOP_ALIGNMENT);
		this.add(this.panel_Content, gbc_panel_Content);
		GridBagLayout gbl_panel_Content = new GridBagLayout();
		gbl_panel_Content.columnWidths = new int[] { 20, 54, 10, 0, 0 };
		gbl_panel_Content.rowHeights = new int[] { 26, 0, 0, 0 };
		gbl_panel_Content.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_panel_Content.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		this.panel_Content.setLayout(gbl_panel_Content);

		JLabel lblLaborstufe = new JLabel("Laborstufe");
		GridBagConstraints gbc_lblLaborstufe = new GridBagConstraints();
		gbc_lblLaborstufe.anchor = GridBagConstraints.WEST;
		gbc_lblLaborstufe.insets = new Insets(0, 0, 5, 5);
		gbc_lblLaborstufe.gridx = 0;
		gbc_lblLaborstufe.gridy = 0;
		this.panel_Content.add(lblLaborstufe, gbc_lblLaborstufe);
		lblLaborstufe.setFont(new Font("Tahoma", Font.PLAIN, 14));

		JComboBox<ELaborstufen> comboBox_1 = new JComboBox<ELaborstufen>();
		comboBox_1.setMinimumSize(new Dimension(120, 23));
		comboBox_1.setPreferredSize(new Dimension(120, 23));
		comboBox_1.setModel(new DefaultComboBoxModel<ELaborstufen>(ELaborstufen.values()));
		GridBagConstraints gbc_comboBox_1 = new GridBagConstraints();
		gbc_comboBox_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_1.gridwidth = 2;
		gbc_comboBox_1.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_1.gridx = 1;
		gbc_comboBox_1.gridy = 0;
		this.panel_Content.add(comboBox_1, gbc_comboBox_1);

		JLabel lblDesLabors = new JLabel("Wert des Labors");
		GridBagConstraints gbc_lblDesLabors = new GridBagConstraints();
		gbc_lblDesLabors.anchor = GridBagConstraints.WEST;
		gbc_lblDesLabors.insets = new Insets(0, 0, 5, 10);
		gbc_lblDesLabors.gridx = 0;
		gbc_lblDesLabors.gridy = 1;
		this.panel_Content.add(lblDesLabors, gbc_lblDesLabors);
		lblDesLabors.setFont(new Font("Tahoma", Font.PLAIN, 14));

		this.spinner = new JSpinner();
		this.spinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				int i = (int) ((JSpinner) e.getSource()).getValue();
				Panel_Labor_Werte.this.slider.setValue(i/100);
			}
		});
		this.spinner.setPreferredSize(new Dimension(60, 23));
		this.spinner.setModel(new SpinnerNumberModel(0, null, null, 10));
		GridBagConstraints gbc_spinner = new GridBagConstraints();
		gbc_spinner.anchor = GridBagConstraints.WEST;
		gbc_spinner.insets = new Insets(0, 0, 5, 5);
		gbc_spinner.gridx = 1;
		gbc_spinner.gridy = 1;
		this.panel_Content.add(this.spinner, gbc_spinner);

		this.slider = new JSlider();
		this.slider.setMinorTickSpacing(1);
		this.slider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				int i = ((JSlider) e.getSource()).getValue();
				Panel_Labor_Werte.this.spinner.getModel().setValue(i*100);
			}
		});
		this.slider.setValue(0);
		this.slider.setPaintTicks(true);
		this.slider.setMajorTickSpacing(5);
		this.slider.setMaximum(50);
		this.slider.setPreferredSize(new Dimension(100, 23));
		GridBagConstraints gbc_slider = new GridBagConstraints();
		gbc_slider.fill = GridBagConstraints.HORIZONTAL;
		gbc_slider.insets = new Insets(0, 0, 5, 5);
		gbc_slider.gridx = 2;
		gbc_slider.gridy = 1;
		this.panel_Content.add(this.slider, gbc_slider);

	}

	private void expandContent(ActionEvent e)
	{
		this.panel_Content.setVisible(!this.panel_Content.isVisible());
		if (this.panel_Content.isVisible())
			this.buttonExpand.setText("-");
		else
			this.buttonExpand.setText("+");
		this.doLayout();
	}
	
	public int getPrice()
	{
		return (int) this.spinner.getValue();
	}
	public void setPrice(int price)
	{
		this.spinner.getModel().setValue(price);
		this.slider.setValue(price);
	}
}
