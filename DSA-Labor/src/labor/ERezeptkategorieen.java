package labor;

public enum ERezeptkategorieen
{
	v1("Aus dem Handgelenk"),
	v2("Elixier der Tugenden"),
	v3("Gegenstands-Elixier"),
	v4("Gift"),
	v5("Heilmittel"),
	v6("Rauschmittel"),
	v7("Wandlungselixier"),
	v8("Zaubermittel");


	String name;

	private ERezeptkategorieen(String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return this.name;
	}


}
