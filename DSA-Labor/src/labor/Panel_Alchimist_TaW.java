package labor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

@SuppressWarnings("serial")
public class Panel_Alchimist_TaW extends JPanel
{
	JPanel panel_Content = new JPanel();
	JButton buttonExpand;

	/**
	 * Create the panel.
	 */
	public Panel_Alchimist_TaW()
	{
		this.initGUI();

	}

	private void initGUI()
	{
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 500, 0 };
		gridBagLayout.rowHeights = new int[] { 0, -1, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		this.setLayout(gridBagLayout);

		this.buttonExpand = new JButton("-");
		this.buttonExpand.addActionListener(this::expandContent);
		this.buttonExpand.setPreferredSize(new Dimension(16, 16));
		this.buttonExpand.setMinimumSize(new Dimension(16, 16));
		this.buttonExpand.setMaximumSize(new Dimension(18, 18));
		this.buttonExpand.setMargin(new Insets(0, 0, 0, 0));
		this.buttonExpand.setAlignmentX(0.5f);
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 0, 5);
		gbc_button.gridx = 0;
		gbc_button.gridy = 0;
		this.add(this.buttonExpand, gbc_button);

		JLabel label = new JLabel("Talentwerte:");
		label.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 0, 0);
		gbc_label.anchor = GridBagConstraints.WEST;
		gbc_label.gridx = 1;
		gbc_label.gridy = 0;
		this.add(label, gbc_label);
		GridBagConstraints gbc_panel_Content = new GridBagConstraints();
		gbc_panel_Content.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_Content.anchor = GridBagConstraints.NORTH;
		gbc_panel_Content.gridx = 1;
		gbc_panel_Content.gridy = 1;
		this.panel_Content.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		this.panel_Content.setAlignmentY(Component.TOP_ALIGNMENT);
		this.add(this.panel_Content, gbc_panel_Content);
		GridBagLayout gbl_panel_Content = new GridBagLayout();
		gbl_panel_Content.columnWidths = new int[] { 20, 250, 10, 0 };
		gbl_panel_Content.rowHeights = new int[] { 26, 0, 0, 0 };
		gbl_panel_Content.columnWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_panel_Content.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		this.panel_Content.setLayout(gbl_panel_Content);

		JPanel panel_content = new JPanel();
		FlowLayout fl_panel_content = (FlowLayout) panel_content.getLayout();
		fl_panel_content.setAlignOnBaseline(true);
		GridBagConstraints gbc_panel_content = new GridBagConstraints();
		gbc_panel_content.insets = new Insets(0, 0, 0, 5);
		gbc_panel_content.fill = GridBagConstraints.BOTH;
		gbc_panel_content.gridx = 0;
		gbc_panel_content.gridy = 0;
		this.panel_Content.add(panel_content, gbc_panel_content);

		JLabel label_1 = new JLabel("Alchimie");
		panel_content.add(label_1);
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));

		JComboBox<EEigenschaftswerte> comboBox = new JComboBox<EEigenschaftswerte>();
		panel_content.add(comboBox);
		comboBox.setModel(new DefaultComboBoxModel<EEigenschaftswerte>(EEigenschaftswerte.values()));
		comboBox.setSelectedIndex(6);

		Component horizontalStrut = Box.createHorizontalStrut(10);
		panel_content.add(horizontalStrut);

		JLabel label_2 = new JLabel("Spezialisierung:");
		panel_content.add(label_2);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBorder(new CompoundBorder(new EmptyBorder(3, 0, 0, 0), new EtchedBorder(EtchedBorder.LOWERED, null, null)));
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.gridheight = 2;
		gbc_scrollPane_1.anchor = GridBagConstraints.WEST;
		gbc_scrollPane_1.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane_1.gridx = 1;
		gbc_scrollPane_1.gridy = 0;
		this.panel_Content.add(scrollPane_1, gbc_scrollPane_1);
		scrollPane_1.setAlignmentY(Component.TOP_ALIGNMENT);
		scrollPane_1.setAlignmentX(Component.LEFT_ALIGNMENT);
		scrollPane_1.setPreferredSize(new Dimension(250, 75));
		scrollPane_1.setSize(new Dimension(300, 100));
		scrollPane_1.setMaximumSize(new Dimension(300, 100));
		scrollPane_1.setBounds(256, 158, 302, 70);
		scrollPane_1.getVerticalScrollBar().setUnitIncrement(5);

		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		scrollPane_1.setViewportView(panel_3);
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));

		JCheckBox chckbxNewCheckBox = new JCheckBox("Brandmittel");
		chckbxNewCheckBox.setBackground(Color.WHITE);
		panel_3.add(chckbxNewCheckBox);

		JCheckBox chckbxFarbenLacke = new JCheckBox("Farben / Lacke");
		chckbxFarbenLacke.setBackground(Color.WHITE);
		panel_3.add(chckbxFarbenLacke);

		JCheckBox chckbxGifte = new JCheckBox("Gifte");
		chckbxGifte.setBackground(Color.WHITE);
		panel_3.add(chckbxGifte);

		JCheckBox chckbxLaborpraxis = new JCheckBox("Laborpraxis");
		chckbxLaborpraxis.setBackground(Color.WHITE);
		panel_3.add(chckbxLaborpraxis);

		JCheckBox chckbxZauberelixiere = new JCheckBox("Zauber-Elixiere");
		chckbxZauberelixiere.setBackground(Color.WHITE);
		panel_3.add(chckbxZauberelixiere);

		JCheckBox chckbxVeredelungGlaeser = new JCheckBox("Veredelung : Glaeser");
		chckbxVeredelungGlaeser.setBackground(Color.WHITE);
		panel_3.add(chckbxVeredelungGlaeser);

		JCheckBox chckbxVeredelungGolfmacherei = new JCheckBox("Veredelung : Goldmacherei");
		chckbxVeredelungGolfmacherei.setBackground(Color.WHITE);
		panel_3.add(chckbxVeredelungGolfmacherei);

		JCheckBox chckbxVeredelungLegierungen = new JCheckBox("Veredelung : Legierungen");
		chckbxVeredelungLegierungen.setBackground(Color.WHITE);
		panel_3.add(chckbxVeredelungLegierungen);

		JCheckBox chckbxVeredelungPorzellan = new JCheckBox("Veredelung : Porzellan");
		chckbxVeredelungPorzellan.setBackground(Color.WHITE);
		panel_3.add(chckbxVeredelungPorzellan);

		JCheckBox chckbxThoealch = new JCheckBox("Theoretische Alchimie (Sympathetik)");
		chckbxThoealch.setBackground(Color.WHITE);
		panel_3.add(chckbxThoealch);

		JCheckBox chckbxThoealchelementarismus = new JCheckBox("Theoretische Alchimie (Elementarismus)");
		chckbxThoealchelementarismus.setBackground(Color.WHITE);
		panel_3.add(chckbxThoealchelementarismus);

	}

	private void expandContent(ActionEvent e)
	{
		this.panel_Content.setVisible(!this.panel_Content.isVisible());
		if (this.panel_Content.isVisible())
			this.buttonExpand.setText("-");
		else
			this.buttonExpand.setText("+");
		this.doLayout();
	}
}
