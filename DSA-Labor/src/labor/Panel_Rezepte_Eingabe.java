package labor;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

@SuppressWarnings("serial")
public class Panel_Rezepte_Eingabe extends JPanel
{
	JPanel panel_Content = new JPanel();
	JButton buttonExpand;
	private JTextField txtNameDesTrankes;
	private Panel_Rezepte_Eingabe_Stufen panel_Stufen;

	/**
	 * Create the panel.
	 */
	public Panel_Rezepte_Eingabe()
	{
		this.initGUI();
		//this.panel_Content.setVisible(false);
		this.expandContent(null);
//		setValues
		this.panel_Stufen.setValues(0, 6, 12, 18, 24, 30, 0, 1, 2, 3, 4, 5, 6);
	}

	private void initGUI()
	{
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, -1, 180, -1, 0 };
		gridBagLayout.rowHeights = new int[] { 0, -1, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		this.setLayout(gridBagLayout);

		this.buttonExpand = new JButton("-");
		this.buttonExpand.addActionListener(this::expandContent);
		this.buttonExpand.setPreferredSize(new Dimension(16, 16));
		this.buttonExpand.setMinimumSize(new Dimension(16, 16));
		this.buttonExpand.setMaximumSize(new Dimension(18, 18));
		this.buttonExpand.setMargin(new Insets(0, 0, 0, 0));
		this.buttonExpand.setAlignmentX(0.5f);
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 5, 5);
		gbc_button.gridx = 0;
		gbc_button.gridy = 0;
		this.add(this.buttonExpand, gbc_button);

		JLabel lblAusstattung = new JLabel("Neuer Trank:");
		lblAusstattung.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblAusstattung = new GridBagConstraints();
		gbc_lblAusstattung.anchor = GridBagConstraints.EAST;
		gbc_lblAusstattung.insets = new Insets(0, 0, 5, 5);
		gbc_lblAusstattung.gridx = 1;
		gbc_lblAusstattung.gridy = 0;
		this.add(lblAusstattung, gbc_lblAusstattung);

		this.txtNameDesTrankes = new JTextField();
		this.txtNameDesTrankes.setText("Name des Trankes");
		this.txtNameDesTrankes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_txtNameDesTrankes = new GridBagConstraints();
		gbc_txtNameDesTrankes.insets = new Insets(0, 0, 5, 5);
		gbc_txtNameDesTrankes.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNameDesTrankes.gridx = 2;
		gbc_txtNameDesTrankes.gridy = 0;
		this.add(this.txtNameDesTrankes, gbc_txtNameDesTrankes);
		this.txtNameDesTrankes.setColumns(10);
		GridBagConstraints gbc_panel_Content = new GridBagConstraints();
		gbc_panel_Content.insets = new Insets(0, 0, 0, 5);
		gbc_panel_Content.gridwidth = 3;
		gbc_panel_Content.anchor = GridBagConstraints.WEST;
		gbc_panel_Content.fill = GridBagConstraints.VERTICAL;
		gbc_panel_Content.gridx = 1;
		gbc_panel_Content.gridy = 1;
		this.panel_Content.setBorder(new CompoundBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), new EmptyBorder(3, 3, 3, 3)));
		this.panel_Content.setAlignmentY(Component.TOP_ALIGNMENT);
		this.add(this.panel_Content, gbc_panel_Content);
		GridBagLayout gbl_panel_Content = new GridBagLayout();
		gbl_panel_Content.columnWidths = new int[] { -1, -1, -1, 0, 0, 10, 0 };
		gbl_panel_Content.rowHeights = new int[] { -1, -1, -1, -1, -1, 0 };
		gbl_panel_Content.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_panel_Content.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		this.panel_Content.setLayout(gbl_panel_Content);

		JLabel lblLaborstufe = new JLabel("Kategorie:    ");
		GridBagConstraints gbc_lblLaborstufe = new GridBagConstraints();
		gbc_lblLaborstufe.anchor = GridBagConstraints.WEST;
		gbc_lblLaborstufe.insets = new Insets(0, 0, 5, 5);
		gbc_lblLaborstufe.gridx = 0;
		gbc_lblLaborstufe.gridy = 0;
		this.panel_Content.add(lblLaborstufe, gbc_lblLaborstufe);
		lblLaborstufe.setFont(new Font("Tahoma", Font.PLAIN, 14));

		JComboBox<ERezeptkategorieen> comboBox_1 = new JComboBox<ERezeptkategorieen>();
		comboBox_1.setMinimumSize(new Dimension(150, 23));
		comboBox_1.setPreferredSize(new Dimension(150, 23));
		comboBox_1.setModel(new DefaultComboBoxModel<ERezeptkategorieen>(ERezeptkategorieen.values()));
		GridBagConstraints gbc_comboBox_1 = new GridBagConstraints();
		gbc_comboBox_1.anchor = GridBagConstraints.WEST;
		gbc_comboBox_1.gridwidth = 4;
		gbc_comboBox_1.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_1.gridx = 1;
		gbc_comboBox_1.gridy = 0;
		this.panel_Content.add(comboBox_1, gbc_comboBox_1);

		JLabel lblNewLabel = new JLabel("Brau-/Analyse-Schwierigkeit    ");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.gridwidth = 2;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		this.panel_Content.add(lblNewLabel, gbc_lblNewLabel);

		JSpinner spinner = new JSpinner();
		spinner.setMinimumSize(new Dimension(50, 20));
		spinner.setPreferredSize(new Dimension(60, 20));
		GridBagConstraints gbc_spinner = new GridBagConstraints();
		gbc_spinner.anchor = GridBagConstraints.WEST;
		gbc_spinner.insets = new Insets(0, 0, 5, 5);
		gbc_spinner.gridx = 2;
		gbc_spinner.gridy = 1;
		this.panel_Content.add(spinner, gbc_spinner);

		JLabel lblNewLabel_1 = new JLabel("/");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 3;
		gbc_lblNewLabel_1.gridy = 1;
		this.panel_Content.add(lblNewLabel_1, gbc_lblNewLabel_1);

		JSpinner spinner_1 = new JSpinner();
		spinner_1.setMinimumSize(new Dimension(50, 20));
		spinner_1.setPreferredSize(new Dimension(60, 20));
		GridBagConstraints gbc_spinner_1 = new GridBagConstraints();
		gbc_spinner_1.anchor = GridBagConstraints.WEST;
		gbc_spinner_1.insets = new Insets(0, 0, 5, 5);
		gbc_spinner_1.gridx = 4;
		gbc_spinner_1.gridy = 1;
		this.panel_Content.add(spinner_1, gbc_spinner_1);

		JCheckBox chckbxNewCheckBox = new JCheckBox("Primäre nutzung der Alchimieschale möglich");
		chckbxNewCheckBox.setFont(new Font("Tahoma", Font.PLAIN, 12));
		GridBagConstraints gbc_chckbxNewCheckBox = new GridBagConstraints();
		gbc_chckbxNewCheckBox.gridwidth = 5;
		gbc_chckbxNewCheckBox.anchor = GridBagConstraints.WEST;
		gbc_chckbxNewCheckBox.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxNewCheckBox.gridx = 0;
		gbc_chckbxNewCheckBox.gridy = 2;
		this.panel_Content.add(chckbxNewCheckBox, gbc_chckbxNewCheckBox);

		JCheckBox chckbxNewCheckBox_1 = new JCheckBox("Benötigt elementare Transmutation");
		chckbxNewCheckBox_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		GridBagConstraints gbc_chckbxNewCheckBox_1 = new GridBagConstraints();
		gbc_chckbxNewCheckBox_1.gridwidth = 5;
		gbc_chckbxNewCheckBox_1.anchor = GridBagConstraints.BELOW_BASELINE_LEADING;
		gbc_chckbxNewCheckBox_1.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxNewCheckBox_1.gridx = 0;
		gbc_chckbxNewCheckBox_1.gridy = 3;
		this.panel_Content.add(chckbxNewCheckBox_1, gbc_chckbxNewCheckBox_1);

		this.panel_Stufen = new Panel_Rezepte_Eingabe_Stufen();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.anchor = GridBagConstraints.NORTHWEST;
		gbc_panel.gridwidth = 6;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 4;
		this.panel_Content.add(this.panel_Stufen, gbc_panel);

	}

	private void expandContent(ActionEvent e)
	{
		this.panel_Content.setVisible(!this.panel_Content.isVisible());
		if (this.panel_Content.isVisible())
			this.buttonExpand.setText("-");
		else
			this.buttonExpand.setText("+");
		this.doLayout();
	}

}
