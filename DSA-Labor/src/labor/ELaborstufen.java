package labor;

public enum ELaborstufen
{
	v1(1,"Archaisches Labor",10),
	v2(2,"Hexenkueche",100),
	v3(3,"Labor",500);

	int index;
	int grundpreis;
	String name;

	private ELaborstufen(int index, String name, int grundpreis)
	{
		this.index = index;
		this.name = name;
		this.grundpreis = grundpreis;
	}

	@Override
	public String toString()
	{
		return this.name;
	}


}
