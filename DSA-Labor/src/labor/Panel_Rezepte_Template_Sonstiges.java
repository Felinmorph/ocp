package labor;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

@SuppressWarnings("serial")
public class Panel_Rezepte_Template_Sonstiges extends JPanel
{
	JPanel panel_Content = new JPanel();
	private JCheckBox chckbxExpand;
	private JLabel label_MA2;
	private JLabel label_AB2;
	private JLabel label_BC2;
	private JLabel label_CD2;
	private JLabel label_DE2;
	private JLabel label_EF2;
	private JLabel label_EF1;
	private JLabel label_DE1;
	private JLabel label_CD1;
	private JLabel label_BC1;
	private JLabel label_AB1;
	private JLabel label_MA1;
	private JLabel lblM;
	private JLabel lblA;
	private JLabel lblB;
	private JLabel lblC;
	private JLabel lblD;
	private JLabel lblE;
	private JLabel lblF;

	/**
	 * Create the panel.
	 */
	public Panel_Rezepte_Template_Sonstiges()
	{
		this.initGUI();
		//this.chckbxExpand.setSelected(false);
		this.expandContent(null);
	}

	public void setValues(int ma, int ab, int bc, int cd, int de, int ef)
	{
		this.label_MA1.setText("" + ma);
		this.label_MA2.setText("" + ma);
		this.label_AB1.setText("" + ab);
		this.label_AB2.setText("" + ab);
		this.label_BC1.setText("" + bc);
		this.label_BC2.setText("" + bc);
		this.label_CD1.setText("" + cd);
		this.label_CD2.setText("" + cd);
		this.label_DE1.setText("" + de);
		this.label_DE2.setText("" + de);
		this.label_EF1.setText("" + ef);
		this.label_EF2.setText("" + ef);
		this.doLayout();
	}

	private void initGUI()
	{
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 500, 0 };
		gridBagLayout.rowHeights = new int[] { 0, -1, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		this.setLayout(gridBagLayout);

		this.chckbxExpand = new JCheckBox("");
		this.chckbxExpand.addActionListener(this::expandContent);
		GridBagConstraints gbc_chckbxExpand = new GridBagConstraints();
		gbc_chckbxExpand.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxExpand.gridx = 0;
		gbc_chckbxExpand.gridy = 0;
		this.add(this.chckbxExpand, gbc_chckbxExpand);
		this.chckbxExpand.setFont(new Font("Tahoma", Font.PLAIN, 14));

		JLabel lblAusstattung = new JLabel("sonstiges");
		lblAusstattung.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblAusstattung = new GridBagConstraints();
		gbc_lblAusstattung.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblAusstattung.insets = new Insets(0, 0, 5, 0);
		gbc_lblAusstattung.gridx = 1;
		gbc_lblAusstattung.gridy = 0;
		this.add(lblAusstattung, gbc_lblAusstattung);
		GridBagConstraints gbc_panel_Content = new GridBagConstraints();
		gbc_panel_Content.fill = GridBagConstraints.BOTH;
		gbc_panel_Content.gridx = 1;
		gbc_panel_Content.gridy = 1;
		this.panel_Content.setBorder(new CompoundBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), new EmptyBorder(3, 3, 3, 3)));
		this.panel_Content.setAlignmentY(Component.TOP_ALIGNMENT);
		this.add(this.panel_Content, gbc_panel_Content);
		GridBagLayout gbl_panel_Content = new GridBagLayout();
		gbl_panel_Content.columnWidths = new int[] { -1, -1, 0, -1, 51, 0, 0 };
		gbl_panel_Content.rowHeights = new int[] { 26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel_Content.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_panel_Content.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		this.panel_Content.setLayout(gbl_panel_Content);

		JLabel lblNewLabel_1 = new JLabel("Probe");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.gridwidth = 4;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 0;
		this.panel_Content.add(lblNewLabel_1, gbc_lblNewLabel_1);

		JLabel lblErgebnis = new JLabel("Ergebnis");
		lblErgebnis.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblErgebnis = new GridBagConstraints();
		gbc_lblErgebnis.insets = new Insets(0, 0, 5, 5);
		gbc_lblErgebnis.gridx = 4;
		gbc_lblErgebnis.gridy = 0;
		this.panel_Content.add(lblErgebnis, gbc_lblErgebnis);

		JLabel lblNewLabel_2 = new JLabel("- ∞");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 1;
		gbc_lblNewLabel_2.gridy = 1;
		this.panel_Content.add(lblNewLabel_2, gbc_lblNewLabel_2);

		JLabel label = new JLabel("-");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 2;
		gbc_label.gridy = 1;
		this.panel_Content.add(label, gbc_label);

		this.label_MA1 = new JLabel("6");
		this.label_MA1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_MA1 = new GridBagConstraints();
		gbc_label_MA1.insets = new Insets(0, 0, 5, 5);
		gbc_label_MA1.gridx = 3;
		gbc_label_MA1.gridy = 1;
		this.panel_Content.add(this.label_MA1, gbc_label_MA1);

		this.lblM = new JLabel("M");
		this.lblM.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblM = new GridBagConstraints();
		gbc_lblM.insets = new Insets(0, 0, 5, 5);
		gbc_lblM.gridx = 4;
		gbc_lblM.gridy = 1;
		this.panel_Content.add(this.lblM, gbc_lblM);

		this.label_MA2 = new JLabel("6");
		this.label_MA2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_MA = new GridBagConstraints();
		gbc_label_MA.insets = new Insets(0, 0, 5, 5);
		gbc_label_MA.gridx = 1;
		gbc_label_MA.gridy = 2;
		this.panel_Content.add(this.label_MA2, gbc_label_MA);

		JLabel label_1 = new JLabel("-");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.gridx = 2;
		gbc_label_1.gridy = 2;
		this.panel_Content.add(label_1, gbc_label_1);

		this.label_AB1 = new JLabel("12");
		this.label_AB1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_AB1 = new GridBagConstraints();
		gbc_label_AB1.insets = new Insets(0, 0, 5, 5);
		gbc_label_AB1.gridx = 3;
		gbc_label_AB1.gridy = 2;
		this.panel_Content.add(this.label_AB1, gbc_label_AB1);

		this.lblA = new JLabel("A");
		this.lblA.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblA = new GridBagConstraints();
		gbc_lblA.insets = new Insets(0, 0, 5, 5);
		gbc_lblA.gridx = 4;
		gbc_lblA.gridy = 2;
		this.panel_Content.add(this.lblA, gbc_lblA);

		this.label_AB2 = new JLabel("12");
		this.label_AB2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_AB = new GridBagConstraints();
		gbc_label_AB.insets = new Insets(0, 0, 5, 5);
		gbc_label_AB.gridx = 1;
		gbc_label_AB.gridy = 3;
		this.panel_Content.add(this.label_AB2, gbc_label_AB);

		JLabel label_2 = new JLabel("-");
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.gridx = 2;
		gbc_label_2.gridy = 3;
		this.panel_Content.add(label_2, gbc_label_2);

		this.label_BC1 = new JLabel("15");
		this.label_BC1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_BC1 = new GridBagConstraints();
		gbc_label_BC1.insets = new Insets(0, 0, 5, 5);
		gbc_label_BC1.gridx = 3;
		gbc_label_BC1.gridy = 3;
		this.panel_Content.add(this.label_BC1, gbc_label_BC1);

		this.lblB = new JLabel("B");
		this.lblB.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblB = new GridBagConstraints();
		gbc_lblB.insets = new Insets(0, 0, 5, 5);
		gbc_lblB.gridx = 4;
		gbc_lblB.gridy = 3;
		this.panel_Content.add(this.lblB, gbc_lblB);

		this.label_BC2 = new JLabel("15");
		this.label_BC2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_BC = new GridBagConstraints();
		gbc_label_BC.insets = new Insets(0, 0, 5, 5);
		gbc_label_BC.gridx = 1;
		gbc_label_BC.gridy = 4;
		this.panel_Content.add(this.label_BC2, gbc_label_BC);

		JLabel label_3 = new JLabel("-");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.insets = new Insets(0, 0, 5, 5);
		gbc_label_3.gridx = 2;
		gbc_label_3.gridy = 4;
		this.panel_Content.add(label_3, gbc_label_3);

		this.label_CD1 = new JLabel("18");
		this.label_CD1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_CD1 = new GridBagConstraints();
		gbc_label_CD1.insets = new Insets(0, 0, 5, 5);
		gbc_label_CD1.gridx = 3;
		gbc_label_CD1.gridy = 4;
		this.panel_Content.add(this.label_CD1, gbc_label_CD1);

		this.lblC = new JLabel("C");
		this.lblC.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblC = new GridBagConstraints();
		gbc_lblC.insets = new Insets(0, 0, 5, 5);
		gbc_lblC.gridx = 4;
		gbc_lblC.gridy = 4;
		this.panel_Content.add(this.lblC, gbc_lblC);

		this.label_CD2 = new JLabel("18");
		this.label_CD2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_CD = new GridBagConstraints();
		gbc_label_CD.insets = new Insets(0, 0, 5, 5);
		gbc_label_CD.gridx = 1;
		gbc_label_CD.gridy = 5;
		this.panel_Content.add(this.label_CD2, gbc_label_CD);

		JLabel label_4 = new JLabel("-");
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.insets = new Insets(0, 0, 5, 5);
		gbc_label_4.gridx = 2;
		gbc_label_4.gridy = 5;
		this.panel_Content.add(label_4, gbc_label_4);

		this.label_DE1 = new JLabel("21");
		this.label_DE1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_DE1 = new GridBagConstraints();
		gbc_label_DE1.insets = new Insets(0, 0, 5, 5);
		gbc_label_DE1.gridx = 3;
		gbc_label_DE1.gridy = 5;
		this.panel_Content.add(this.label_DE1, gbc_label_DE1);

		this.lblD = new JLabel("D");
		this.lblD.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblD = new GridBagConstraints();
		gbc_lblD.insets = new Insets(0, 0, 5, 5);
		gbc_lblD.gridx = 4;
		gbc_lblD.gridy = 5;
		this.panel_Content.add(this.lblD, gbc_lblD);

		this.label_DE2 = new JLabel("21");
		this.label_DE2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_DE = new GridBagConstraints();
		gbc_label_DE.insets = new Insets(0, 0, 5, 5);
		gbc_label_DE.gridx = 1;
		gbc_label_DE.gridy = 6;
		this.panel_Content.add(this.label_DE2, gbc_label_DE);

		JLabel label_5 = new JLabel("-");
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.insets = new Insets(0, 0, 5, 5);
		gbc_label_5.gridx = 2;
		gbc_label_5.gridy = 6;
		this.panel_Content.add(label_5, gbc_label_5);

		this.label_EF1 = new JLabel("24");
		this.label_EF1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_EF1 = new GridBagConstraints();
		gbc_label_EF1.insets = new Insets(0, 0, 5, 5);
		gbc_label_EF1.gridx = 3;
		gbc_label_EF1.gridy = 6;
		this.panel_Content.add(this.label_EF1, gbc_label_EF1);

		this.lblE = new JLabel("E");
		this.lblE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblE = new GridBagConstraints();
		gbc_lblE.insets = new Insets(0, 0, 5, 5);
		gbc_lblE.gridx = 4;
		gbc_lblE.gridy = 6;
		this.panel_Content.add(this.lblE, gbc_lblE);

		this.label_EF2 = new JLabel("24");
		this.label_EF2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_EF = new GridBagConstraints();
		gbc_label_EF.insets = new Insets(0, 0, 5, 5);
		gbc_label_EF.gridx = 1;
		gbc_label_EF.gridy = 7;
		this.panel_Content.add(this.label_EF2, gbc_label_EF);

		JLabel label_6 = new JLabel("-");
		GridBagConstraints gbc_label_6 = new GridBagConstraints();
		gbc_label_6.insets = new Insets(0, 0, 5, 5);
		gbc_label_6.gridx = 2;
		gbc_label_6.gridy = 7;
		this.panel_Content.add(label_6, gbc_label_6);

		JLabel label_7 = new JLabel("+ ∞");
		label_7.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_7 = new GridBagConstraints();
		gbc_label_7.insets = new Insets(0, 0, 5, 5);
		gbc_label_7.gridx = 3;
		gbc_label_7.gridy = 7;
		this.panel_Content.add(label_7, gbc_label_7);

		this.lblF = new JLabel("F");
		this.lblF.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblF = new GridBagConstraints();
		gbc_lblF.insets = new Insets(0, 0, 5, 5);
		gbc_lblF.gridx = 4;
		gbc_lblF.gridy = 7;
		this.panel_Content.add(this.lblF, gbc_lblF);

	}

	private void expandContent(ActionEvent e)
	{
		this.panel_Content.setVisible(this.chckbxExpand.isSelected());
		this.doLayout();
	}

}
