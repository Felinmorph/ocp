package labor;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

@SuppressWarnings("serial")
public class Panel_Alchimist_Eigenschaften extends JPanel
{
	JPanel panel_Content = new JPanel();
	JButton buttonExpand;

	/**
	 * Create the panel.
	 */
	public Panel_Alchimist_Eigenschaften()
	{
		this.initGUI();

	}

	private void initGUI()
	{
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 300, 0 };
		gridBagLayout.rowHeights = new int[] { 0, -1, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		this.setLayout(gridBagLayout);

		this.buttonExpand = new JButton("-");
		this.buttonExpand.addActionListener(this::expandContent);
		this.buttonExpand.setPreferredSize(new Dimension(16, 16));
		this.buttonExpand.setMinimumSize(new Dimension(16, 16));
		this.buttonExpand.setMaximumSize(new Dimension(18, 18));
		this.buttonExpand.setMargin(new Insets(0, 0, 0, 0));
		this.buttonExpand.setAlignmentX(0.5f);
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 0, 5);
		gbc_button.gridx = 0;
		gbc_button.gridy = 0;
		this.add(this.buttonExpand, gbc_button);

		JLabel lblEigenschaftswerte = new JLabel("Eigenschaftswerte:");
		lblEigenschaftswerte.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblEigenschaftswerte = new GridBagConstraints();
		gbc_lblEigenschaftswerte.anchor = GridBagConstraints.WEST;
		gbc_lblEigenschaftswerte.insets = new Insets(0, 0, 0, 0);
		gbc_lblEigenschaftswerte.gridx = 1;
		gbc_lblEigenschaftswerte.gridy = 0;
		this.add(lblEigenschaftswerte, gbc_lblEigenschaftswerte);

		FlowLayout fl_panel_Content = (FlowLayout) this.panel_Content.getLayout();
		fl_panel_Content.setAlignment(FlowLayout.LEFT);
		GridBagConstraints gbc_panel_Content = new GridBagConstraints();
		gbc_panel_Content.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_Content.anchor = GridBagConstraints.NORTH;
		gbc_panel_Content.gridx = 1;
		gbc_panel_Content.gridy = 1;
		this.panel_Content.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		this.add(this.panel_Content, gbc_panel_Content);

		JLabel label_1 = new JLabel("MU");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.panel_Content.add(label_1);

		JComboBox<EEigenschaftswerte> comboBox = new JComboBox<>();
		comboBox.setModel(new DefaultComboBoxModel<EEigenschaftswerte>(EEigenschaftswerte.values()));
		comboBox.setSelectedIndex(6);
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.panel_Content.add(comboBox);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		horizontalStrut.setPreferredSize(new Dimension(10, 0));
		this.panel_Content.add(horizontalStrut);

		JLabel label_2 = new JLabel("KL");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.panel_Content.add(label_2);

		JComboBox<EEigenschaftswerte> comboBox_1 = new JComboBox<EEigenschaftswerte>();
		comboBox_1.setModel(new DefaultComboBoxModel<EEigenschaftswerte>(EEigenschaftswerte.values()));
		comboBox_1.setSelectedIndex(9);
		this.panel_Content.add(comboBox_1);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		horizontalStrut_1.setPreferredSize(new Dimension(10, 0));
		this.panel_Content.add(horizontalStrut_1);

		JLabel label_3 = new JLabel("FF");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.panel_Content.add(label_3);

		JComboBox<EEigenschaftswerte> comboBox_2 = new JComboBox<EEigenschaftswerte>();
		comboBox_2.setModel(new DefaultComboBoxModel<EEigenschaftswerte>(EEigenschaftswerte.values()));
		comboBox_2.setSelectedIndex(5);
		this.panel_Content.add(comboBox_2);
	}

	private void expandContent(ActionEvent e)
	{
		this.panel_Content.setVisible(!this.panel_Content.isVisible());
		if (this.panel_Content.isVisible())
			this.buttonExpand.setText("-");
		else
			this.buttonExpand.setText("+");
		this.doLayout();
	}
}
