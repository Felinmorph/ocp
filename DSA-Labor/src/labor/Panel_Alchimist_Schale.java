package labor;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;

@SuppressWarnings("serial")
public class Panel_Alchimist_Schale extends JPanel
{
	JPanel panel_Content = new JPanel();
	JButton buttonExpand;
	private JCheckBox chckbxWeiheDerSchale;
	private JCheckBox chckbxAnalyse;
	private JCheckBox chckbxChymischeHochzeit;
	private JCheckBox chckbxFeuerUndEis;
	private JCheckBox chckbxMandriconsBindung;
	private JCheckBox chckbxTransmutation;

	/**
	 * Create the panel.
	 */
	public Panel_Alchimist_Schale()
	{
		this.initGUI();

	}

	private void initGUI()
	{
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 300, 0 };
		gridBagLayout.rowHeights = new int[] { 0, -1, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		this.setLayout(gridBagLayout);

		this.buttonExpand = new JButton("-");
		this.buttonExpand.addActionListener(this::expandContent);
		this.buttonExpand.setPreferredSize(new Dimension(16, 16));
		this.buttonExpand.setMinimumSize(new Dimension(16, 16));
		this.buttonExpand.setMaximumSize(new Dimension(18, 18));
		this.buttonExpand.setMargin(new Insets(0, 0, 0, 0));
		this.buttonExpand.setAlignmentX(0.5f);
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 0, 5);
		gbc_button.gridx = 0;
		gbc_button.gridy = 0;
		this.add(this.buttonExpand, gbc_button);

		JLabel lblEigenschaftswerte = new JLabel("Schale der Alchimie:");
		lblEigenschaftswerte.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblEigenschaftswerte = new GridBagConstraints();
		gbc_lblEigenschaftswerte.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblEigenschaftswerte.insets = new Insets(0, 0, 0, 0);
		gbc_lblEigenschaftswerte.gridx = 1;
		gbc_lblEigenschaftswerte.gridy = 0;
		this.add(lblEigenschaftswerte, gbc_lblEigenschaftswerte);
		GridBagConstraints gbc_panel_Content = new GridBagConstraints();
		gbc_panel_Content.anchor = GridBagConstraints.NORTH;
		gbc_panel_Content.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_Content.gridx = 1;
		gbc_panel_Content.gridy = 1;
		this.panel_Content.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		this.add(this.panel_Content, gbc_panel_Content);
		GridBagLayout gbl_panel_Content = new GridBagLayout();
		gbl_panel_Content.columnWidths = new int[] { 20, 94, 0, 0 };
		gbl_panel_Content.rowHeights = new int[] { 23, 0, 0 };
		gbl_panel_Content.columnWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_panel_Content.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		this.panel_Content.setLayout(gbl_panel_Content);

		this.chckbxWeiheDerSchale = new JCheckBox("Weihe der Schale");
		this.chckbxWeiheDerSchale.setSelected(true);
		this.chckbxWeiheDerSchale.addChangeListener(this::enableCheckboxes);
		GridBagConstraints gbc_chckbxWeiheDerSchale = new GridBagConstraints();
		gbc_chckbxWeiheDerSchale.anchor = GridBagConstraints.WEST;
		gbc_chckbxWeiheDerSchale.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxWeiheDerSchale.gridx = 0;
		gbc_chckbxWeiheDerSchale.gridy = 0;
		this.panel_Content.add(this.chckbxWeiheDerSchale, gbc_chckbxWeiheDerSchale);

		this.chckbxAnalyse = new JCheckBox("Allegorische Analyse");
		this.chckbxAnalyse.setEnabled(false);
		GridBagConstraints gbc_chckbxNewCheckBox = new GridBagConstraints();
		gbc_chckbxNewCheckBox.anchor = GridBagConstraints.WEST;
		gbc_chckbxNewCheckBox.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxNewCheckBox.gridx = 1;
		gbc_chckbxNewCheckBox.gridy = 0;
		this.panel_Content.add(this.chckbxAnalyse, gbc_chckbxNewCheckBox);

		this.chckbxFeuerUndEis = new JCheckBox("Feuer und Eis");
		this.chckbxFeuerUndEis.setEnabled(false);
		GridBagConstraints gbc_chckbxFeuerUndEis = new GridBagConstraints();
		gbc_chckbxFeuerUndEis.anchor = GridBagConstraints.WEST;
		gbc_chckbxFeuerUndEis.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxFeuerUndEis.gridx = 2;
		gbc_chckbxFeuerUndEis.gridy = 0;
		this.panel_Content.add(this.chckbxFeuerUndEis, gbc_chckbxFeuerUndEis);

		this.chckbxChymischeHochzeit = new JCheckBox("Chymische Hochzeit");
		GridBagConstraints gbc_chckbxChymischeHochzeit = new GridBagConstraints();
		gbc_chckbxChymischeHochzeit.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxChymischeHochzeit.anchor = GridBagConstraints.WEST;
		gbc_chckbxChymischeHochzeit.gridx = 0;
		gbc_chckbxChymischeHochzeit.gridy = 1;
		this.panel_Content.add(this.chckbxChymischeHochzeit, gbc_chckbxChymischeHochzeit);

		this.chckbxMandriconsBindung = new JCheckBox("Mandricons Bindung");
		GridBagConstraints gbc_chckbxMandriconsBindung = new GridBagConstraints();
		gbc_chckbxMandriconsBindung.anchor = GridBagConstraints.WEST;
		gbc_chckbxMandriconsBindung.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxMandriconsBindung.gridx = 1;
		gbc_chckbxMandriconsBindung.gridy = 1;
		this.panel_Content.add(this.chckbxMandriconsBindung, gbc_chckbxMandriconsBindung);

		this.chckbxTransmutation = new JCheckBox("Transmutation der Elemente");
		GridBagConstraints gbc_chckbxTransmutation = new GridBagConstraints();
		gbc_chckbxTransmutation.anchor = GridBagConstraints.WEST;
		gbc_chckbxTransmutation.gridx = 2;
		gbc_chckbxTransmutation.gridy = 1;
		this.panel_Content.add(this.chckbxTransmutation, gbc_chckbxTransmutation);
	}

	private void expandContent(ActionEvent e)
	{
		this.panel_Content.setVisible(!this.panel_Content.isVisible());
		if (this.panel_Content.isVisible())
			this.buttonExpand.setText("-");
		else
			this.buttonExpand.setText("+");
		this.doLayout();
	}

	private void enableCheckboxes(ChangeEvent e)
	{
		if (this.chckbxWeiheDerSchale.isSelected())
		{
			this.chckbxAnalyse.setEnabled(false);
			this.chckbxChymischeHochzeit.setEnabled(true);
			this.chckbxFeuerUndEis.setEnabled(false);
			this.chckbxMandriconsBindung.setEnabled(true);
			this.chckbxTransmutation.setEnabled(true);
		}
		else
		{
			this.chckbxAnalyse.setEnabled(false);
			this.chckbxChymischeHochzeit.setEnabled(false);
			this.chckbxFeuerUndEis.setEnabled(false);
			this.chckbxMandriconsBindung.setEnabled(false);
			this.chckbxTransmutation.setEnabled(false);
		}
	}
}
