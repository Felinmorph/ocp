package labor;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class Panel_Alchimist extends JPanel
{

	public Panel_Alchimist()
	{
		this.setBorder(new EmptyBorder(0, 0, 10, 0));
		this.setLayout(new BorderLayout(0, 0));

		JLabel lblWerteDesAlchimisten = new JLabel("Werte des Alchimisten:");
		lblWerteDesAlchimisten.setFont(new Font("Tahoma", Font.PLAIN, 16));
		this.add(lblWerteDesAlchimisten, BorderLayout.NORTH);
		
				JPanel panel_1 = new JPanel();
				this.add(panel_1, BorderLayout.CENTER);
				panel_1.setBorder(new EmptyBorder(5, 20, 0, 0));
				GridBagLayout gbl_panel_1 = new GridBagLayout();
				gbl_panel_1.columnWidths = new int[] { 298, 0 };
				gbl_panel_1.rowHeights = new int[] { -1, -1, 0, 0, 0 };
				gbl_panel_1.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
				gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
				panel_1.setLayout(gbl_panel_1);
				
				//		panel_Content_TaW = new JPanel();
						JPanel panel_TaW = new Panel_Alchimist_TaW();
						GridBagConstraints gbc_panel_Content_TaW = new GridBagConstraints();
						gbc_panel_Content_TaW.anchor = GridBagConstraints.NORTH;
						gbc_panel_Content_TaW.fill = GridBagConstraints.HORIZONTAL;
						gbc_panel_Content_TaW.insets = new Insets(0, 0, 5, 0);
						gbc_panel_Content_TaW.gridx = 0;
						gbc_panel_Content_TaW.gridy = 1;
						panel_1.add(panel_TaW, gbc_panel_Content_TaW);
						
								JPanel panel_Eigenschaften = new Panel_Alchimist_Eigenschaften();
								GridBagConstraints gbc_panel_2 = new GridBagConstraints();
								gbc_panel_2.anchor = GridBagConstraints.NORTH;
								gbc_panel_2.fill = GridBagConstraints.HORIZONTAL;
								gbc_panel_2.insets = new Insets(0, 0, 5, 0);
								gbc_panel_2.gridx = 0;
								gbc_panel_2.gridy = 0;
								panel_1.add(panel_Eigenschaften, gbc_panel_2);
								
								JPanel panel_Schale = new Panel_Alchimist_Schale();
								GridBagConstraints gbc_panel_Alchimist_Schale = new GridBagConstraints();
								gbc_panel_Alchimist_Schale.anchor = GridBagConstraints.NORTH;
								gbc_panel_Alchimist_Schale.insets = new Insets(0, 0, 5, 0);
								gbc_panel_Alchimist_Schale.fill = GridBagConstraints.HORIZONTAL;
								gbc_panel_Alchimist_Schale.gridx = 0;
								gbc_panel_Alchimist_Schale.gridy = 2;
								panel_1.add(panel_Schale, gbc_panel_Alchimist_Schale);

	}
}
