package labor;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

@SuppressWarnings("serial")
public class Panel_Rezepte_Eingabe_Stufen extends JPanel
{
	JPanel panel_Content = new JPanel();
	private JCheckBox chckbxExpand;
	private JSpinner spinner_MA;
	private JSpinner spinner_AB;
	private JSpinner spinner_BC;
	private JSpinner spinner_CD;
	private JSpinner spinner_DE;
	private JSpinner spinner_EF;
	private JLabel label_MA;
	private JLabel label_AB;
	private JLabel label_BC;
	private JLabel label_CD;
	private JLabel label_DE;
	private JLabel label_EF;
	private JComboBox<ETrankQualitaeten> comboBox_M;
	private JComboBox<ETrankQualitaeten> comboBox_A;
	private JComboBox<ETrankQualitaeten> comboBox_B;
	private JComboBox<ETrankQualitaeten> comboBox_C;
	private JComboBox<ETrankQualitaeten> comboBox_D;
	private JComboBox<ETrankQualitaeten> comboBox_E;
	private JComboBox<ETrankQualitaeten> comboBox_F;

	/**
	 * Create the panel.
	 */
	public Panel_Rezepte_Eingabe_Stufen()
	{
		this.initGUI();
		//this.chckbxExpand.setSelected(false);
		this.expandContent(null);
	}

	public void setValues(int ma, int ab, int bc, int cd, int de, int ef, int kategorieM, int kategorieA, int kategorieB, int kategorieC,
			int kategorieD, int kategorieE, int kategorieF)
	{
		this.spinner_MA.getModel().setValue(ma);
		this.spinner_AB.getModel().setValue(ab);
		this.spinner_BC.getModel().setValue(bc);
		this.spinner_CD.getModel().setValue(cd);
		this.spinner_DE.getModel().setValue(de);
		this.spinner_EF.getModel().setValue(ef);
		this.label_MA.setText("" + ma);
		this.label_AB.setText("" + ab);
		this.label_BC.setText("" + bc);
		this.label_CD.setText("" + cd);
		this.label_DE.setText("" + de);
		this.label_EF.setText("" + ef);
		this.comboBox_M.setSelectedIndex(kategorieM);
		this.comboBox_A.setSelectedIndex(kategorieA);
		this.comboBox_B.setSelectedIndex(kategorieB);
		this.comboBox_C.setSelectedIndex(kategorieC);
		this.comboBox_D.setSelectedIndex(kategorieD);
		this.comboBox_E.setSelectedIndex(kategorieE);
		this.comboBox_F.setSelectedIndex(kategorieF);
		this.doLayout();
	}

	private void initGUI()
	{
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 500, 0 };
		gridBagLayout.rowHeights = new int[] { 0, -1, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		this.setLayout(gridBagLayout);

		this.chckbxExpand = new JCheckBox("");
		this.chckbxExpand.addActionListener(this::expandContent);
		GridBagConstraints gbc_chckbxExpand = new GridBagConstraints();
		gbc_chckbxExpand.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxExpand.gridx = 0;
		gbc_chckbxExpand.gridy = 0;
		this.add(this.chckbxExpand, gbc_chckbxExpand);
		this.chckbxExpand.setFont(new Font("Tahoma", Font.PLAIN, 14));

		JLabel lblAusstattung = new JLabel("sonstiges");
		lblAusstattung.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblAusstattung = new GridBagConstraints();
		gbc_lblAusstattung.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblAusstattung.insets = new Insets(0, 0, 5, 0);
		gbc_lblAusstattung.gridx = 1;
		gbc_lblAusstattung.gridy = 0;
		this.add(lblAusstattung, gbc_lblAusstattung);
		GridBagConstraints gbc_panel_Content = new GridBagConstraints();
		gbc_panel_Content.fill = GridBagConstraints.BOTH;
		gbc_panel_Content.gridx = 1;
		gbc_panel_Content.gridy = 1;
		this.panel_Content.setBorder(new CompoundBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), new EmptyBorder(3, 3, 3, 3)));
		this.panel_Content.setAlignmentY(Component.TOP_ALIGNMENT);
		this.add(this.panel_Content, gbc_panel_Content);
		GridBagLayout gbl_panel_Content = new GridBagLayout();
		gbl_panel_Content.columnWidths = new int[] { -1, -1, 0, -1, 51, 0, 0 };
		gbl_panel_Content.rowHeights = new int[] { 26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel_Content.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_panel_Content.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		this.panel_Content.setLayout(gbl_panel_Content);

		JLabel lblNewLabel_1 = new JLabel("Probe");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_1.gridwidth = 4;
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 0;
		this.panel_Content.add(lblNewLabel_1, gbc_lblNewLabel_1);

		JLabel lblErgebnis = new JLabel("Ergebnis");
		lblErgebnis.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblErgebnis = new GridBagConstraints();
		gbc_lblErgebnis.insets = new Insets(0, 0, 5, 5);
		gbc_lblErgebnis.gridx = 4;
		gbc_lblErgebnis.gridy = 0;
		this.panel_Content.add(lblErgebnis, gbc_lblErgebnis);

		JLabel lblNewLabel_2 = new JLabel("- ∞");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 1;
		gbc_lblNewLabel_2.gridy = 1;
		this.panel_Content.add(lblNewLabel_2, gbc_lblNewLabel_2);

		JLabel label = new JLabel("-");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 2;
		gbc_label.gridy = 1;
		this.panel_Content.add(label, gbc_label);

		this.label_MA = new JLabel("3");
		this.label_MA.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_MA = new GridBagConstraints();
		gbc_label_MA.insets = new Insets(0, 0, 5, 5);
		gbc_label_MA.gridx = 3;
		gbc_label_MA.gridy = 1;
		this.panel_Content.add(this.label_MA, gbc_label_MA);

		this.comboBox_M = new JComboBox<ETrankQualitaeten>();
		this.comboBox_M.setMaximumSize(new Dimension(40, 20));
		this.comboBox_M.setMinimumSize(new Dimension(40, 20));
		this.comboBox_M.setSize(new Dimension(40, 20));
		this.comboBox_M.setPreferredSize(new Dimension(40, 22));
		this.comboBox_M.setModel(new DefaultComboBoxModel<ETrankQualitaeten>(ETrankQualitaeten.values()));
		GridBagConstraints gbc_comboBox_M = new GridBagConstraints();
		gbc_comboBox_M.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_M.gridx = 4;
		gbc_comboBox_M.gridy = 1;
		this.panel_Content.add(this.comboBox_M, gbc_comboBox_M);

		this.spinner_MA = new JSpinner();
		this.spinner_MA.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.spinner_MA.addChangeListener(e -> this.label_MA.setText("1" + this.spinner_MA.getModel().getValue()));
		this.spinner_MA.setPreferredSize(new Dimension(45, 20));
		GridBagConstraints gbc_spinner_MA = new GridBagConstraints();
		gbc_spinner_MA.insets = new Insets(0, 0, 5, 5);
		gbc_spinner_MA.gridx = 1;
		gbc_spinner_MA.gridy = 2;
		this.panel_Content.add(this.spinner_MA, gbc_spinner_MA);

		JLabel label_1 = new JLabel("-");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.gridx = 2;
		gbc_label_1.gridy = 2;
		this.panel_Content.add(label_1, gbc_label_1);

		this.label_AB = new JLabel("12");
		this.label_AB.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_AB = new GridBagConstraints();
		gbc_label_AB.insets = new Insets(0, 0, 5, 5);
		gbc_label_AB.gridx = 3;
		gbc_label_AB.gridy = 2;
		this.panel_Content.add(this.label_AB, gbc_label_AB);

		this.comboBox_A = new JComboBox<ETrankQualitaeten>();
		this.comboBox_A.setModel(new DefaultComboBoxModel<ETrankQualitaeten>(ETrankQualitaeten.values()));
		this.comboBox_A.setSelectedIndex(1);
		this.comboBox_A.setSize(new Dimension(40, 20));
		this.comboBox_A.setPreferredSize(new Dimension(40, 22));
		this.comboBox_A.setMinimumSize(new Dimension(40, 20));
		this.comboBox_A.setMaximumSize(new Dimension(40, 20));
		GridBagConstraints gbc_comboBox_A = new GridBagConstraints();
		gbc_comboBox_A.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_A.gridx = 4;
		gbc_comboBox_A.gridy = 2;
		this.panel_Content.add(this.comboBox_A, gbc_comboBox_A);

		this.spinner_AB = new JSpinner();
		this.spinner_AB.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.spinner_AB.addChangeListener(e -> this.label_AB.setText("" + this.spinner_AB.getModel().getValue()));
		this.spinner_AB.setPreferredSize(new Dimension(45, 20));
		GridBagConstraints gbc_spinner_AB = new GridBagConstraints();
		gbc_spinner_AB.insets = new Insets(0, 0, 5, 5);
		gbc_spinner_AB.gridx = 1;
		gbc_spinner_AB.gridy = 3;
		this.panel_Content.add(this.spinner_AB, gbc_spinner_AB);

		JLabel label_2 = new JLabel("-");
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.insets = new Insets(0, 0, 5, 5);
		gbc_label_2.gridx = 2;
		gbc_label_2.gridy = 3;
		this.panel_Content.add(label_2, gbc_label_2);

		this.label_BC = new JLabel("15");
		this.label_BC.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_BC = new GridBagConstraints();
		gbc_label_BC.insets = new Insets(0, 0, 5, 5);
		gbc_label_BC.gridx = 3;
		gbc_label_BC.gridy = 3;
		this.panel_Content.add(this.label_BC, gbc_label_BC);

		this.comboBox_B = new JComboBox<ETrankQualitaeten>();
		this.comboBox_B.setModel(new DefaultComboBoxModel<ETrankQualitaeten>(ETrankQualitaeten.values()));
		this.comboBox_B.setSelectedIndex(2);
		this.comboBox_B.setSize(new Dimension(40, 20));
		this.comboBox_B.setPreferredSize(new Dimension(40, 22));
		this.comboBox_B.setMinimumSize(new Dimension(40, 20));
		this.comboBox_B.setMaximumSize(new Dimension(40, 20));
		GridBagConstraints gbc_comboBox_B = new GridBagConstraints();
		gbc_comboBox_B.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_B.gridx = 4;
		gbc_comboBox_B.gridy = 3;
		this.panel_Content.add(this.comboBox_B, gbc_comboBox_B);

		this.spinner_BC = new JSpinner();
		this.spinner_BC.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.spinner_BC.addChangeListener(e -> this.label_BC.setText("" + this.spinner_BC.getModel().getValue()));
		this.spinner_BC.setPreferredSize(new Dimension(45, 20));
		GridBagConstraints gbc_spinner_BC = new GridBagConstraints();
		gbc_spinner_BC.insets = new Insets(0, 0, 5, 5);
		gbc_spinner_BC.gridx = 1;
		gbc_spinner_BC.gridy = 4;
		this.panel_Content.add(this.spinner_BC, gbc_spinner_BC);

		JLabel label_3 = new JLabel("-");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.insets = new Insets(0, 0, 5, 5);
		gbc_label_3.gridx = 2;
		gbc_label_3.gridy = 4;
		this.panel_Content.add(label_3, gbc_label_3);

		this.label_CD = new JLabel("18");
		this.label_CD.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_CD = new GridBagConstraints();
		gbc_label_CD.insets = new Insets(0, 0, 5, 5);
		gbc_label_CD.gridx = 3;
		gbc_label_CD.gridy = 4;
		this.panel_Content.add(this.label_CD, gbc_label_CD);

		this.comboBox_C = new JComboBox<ETrankQualitaeten>();
		this.comboBox_C.setModel(new DefaultComboBoxModel<ETrankQualitaeten>(ETrankQualitaeten.values()));
		this.comboBox_C.setSelectedIndex(3);
		this.comboBox_C.setSize(new Dimension(40, 20));
		this.comboBox_C.setPreferredSize(new Dimension(40, 22));
		this.comboBox_C.setMinimumSize(new Dimension(40, 20));
		this.comboBox_C.setMaximumSize(new Dimension(40, 20));
		GridBagConstraints gbc_comboBox_C = new GridBagConstraints();
		gbc_comboBox_C.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_C.gridx = 4;
		gbc_comboBox_C.gridy = 4;
		this.panel_Content.add(this.comboBox_C, gbc_comboBox_C);

		this.spinner_CD = new JSpinner();
		this.spinner_CD.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.spinner_CD.addChangeListener(e -> this.label_CD.setText("" + this.spinner_CD.getModel().getValue()));
		this.spinner_CD.setPreferredSize(new Dimension(45, 20));
		GridBagConstraints gbc_spinner_CD = new GridBagConstraints();
		gbc_spinner_CD.insets = new Insets(0, 0, 5, 5);
		gbc_spinner_CD.gridx = 1;
		gbc_spinner_CD.gridy = 5;
		this.panel_Content.add(this.spinner_CD, gbc_spinner_CD);

		JLabel label_4 = new JLabel("-");
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.insets = new Insets(0, 0, 5, 5);
		gbc_label_4.gridx = 2;
		gbc_label_4.gridy = 5;
		this.panel_Content.add(label_4, gbc_label_4);

		this.label_DE = new JLabel("21");
		this.label_DE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_DE = new GridBagConstraints();
		gbc_label_DE.insets = new Insets(0, 0, 5, 5);
		gbc_label_DE.gridx = 3;
		gbc_label_DE.gridy = 5;
		this.panel_Content.add(this.label_DE, gbc_label_DE);

		this.comboBox_D = new JComboBox<ETrankQualitaeten>();
		this.comboBox_D.setModel(new DefaultComboBoxModel<ETrankQualitaeten>(ETrankQualitaeten.values()));
		this.comboBox_D.setSelectedIndex(4);
		this.comboBox_D.setSize(new Dimension(40, 20));
		this.comboBox_D.setPreferredSize(new Dimension(40, 22));
		this.comboBox_D.setMinimumSize(new Dimension(40, 20));
		this.comboBox_D.setMaximumSize(new Dimension(40, 20));
		GridBagConstraints gbc_comboBox_D = new GridBagConstraints();
		gbc_comboBox_D.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_D.gridx = 4;
		gbc_comboBox_D.gridy = 5;
		this.panel_Content.add(this.comboBox_D, gbc_comboBox_D);

		this.spinner_DE = new JSpinner();
		this.spinner_DE.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.spinner_DE.addChangeListener(e -> this.label_DE.setText("" + this.spinner_DE.getModel().getValue()));
		this.spinner_DE.setPreferredSize(new Dimension(45, 20));
		GridBagConstraints gbc_spinner_DE = new GridBagConstraints();
		gbc_spinner_DE.insets = new Insets(0, 0, 5, 5);
		gbc_spinner_DE.gridx = 1;
		gbc_spinner_DE.gridy = 6;
		this.panel_Content.add(this.spinner_DE, gbc_spinner_DE);

		JLabel label_5 = new JLabel("-");
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.insets = new Insets(0, 0, 5, 5);
		gbc_label_5.gridx = 2;
		gbc_label_5.gridy = 6;
		this.panel_Content.add(label_5, gbc_label_5);

		this.label_EF = new JLabel("24");
		this.label_EF.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_EF = new GridBagConstraints();
		gbc_label_EF.insets = new Insets(0, 0, 5, 5);
		gbc_label_EF.gridx = 3;
		gbc_label_EF.gridy = 6;
		this.panel_Content.add(this.label_EF, gbc_label_EF);

		this.comboBox_E = new JComboBox<ETrankQualitaeten>();
		this.comboBox_E.setModel(new DefaultComboBoxModel<ETrankQualitaeten>(ETrankQualitaeten.values()));
		this.comboBox_E.setSelectedIndex(5);
		this.comboBox_E.setSize(new Dimension(40, 20));
		this.comboBox_E.setPreferredSize(new Dimension(40, 22));
		this.comboBox_E.setMinimumSize(new Dimension(40, 20));
		this.comboBox_E.setMaximumSize(new Dimension(40, 20));
		GridBagConstraints gbc_comboBox_E = new GridBagConstraints();
		gbc_comboBox_E.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_E.gridx = 4;
		gbc_comboBox_E.gridy = 6;
		this.panel_Content.add(this.comboBox_E, gbc_comboBox_E);

		this.spinner_EF = new JSpinner();
		this.spinner_EF.setFont(new Font("Tahoma", Font.PLAIN, 14));
		this.spinner_EF.addChangeListener(e -> this.label_EF.setText("" + this.spinner_EF.getModel().getValue()));
		this.spinner_EF.setPreferredSize(new Dimension(45, 20));
		GridBagConstraints gbc_spinner_EF = new GridBagConstraints();
		gbc_spinner_EF.insets = new Insets(0, 0, 5, 5);
		gbc_spinner_EF.gridx = 1;
		gbc_spinner_EF.gridy = 7;
		this.panel_Content.add(this.spinner_EF, gbc_spinner_EF);

		JLabel label_6 = new JLabel("-");
		GridBagConstraints gbc_label_6 = new GridBagConstraints();
		gbc_label_6.insets = new Insets(0, 0, 5, 5);
		gbc_label_6.gridx = 2;
		gbc_label_6.gridy = 7;
		this.panel_Content.add(label_6, gbc_label_6);

		JLabel label_7 = new JLabel("+ ∞");
		label_7.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_label_7 = new GridBagConstraints();
		gbc_label_7.insets = new Insets(0, 0, 5, 5);
		gbc_label_7.gridx = 3;
		gbc_label_7.gridy = 7;
		this.panel_Content.add(label_7, gbc_label_7);

		this.comboBox_F = new JComboBox<ETrankQualitaeten>();
		this.comboBox_F.setModel(new DefaultComboBoxModel<ETrankQualitaeten>(ETrankQualitaeten.values()));
		this.comboBox_F.setSelectedIndex(6);
		this.comboBox_F.setSize(new Dimension(40, 20));
		this.comboBox_F.setPreferredSize(new Dimension(40, 22));
		this.comboBox_F.setMinimumSize(new Dimension(40, 20));
		this.comboBox_F.setMaximumSize(new Dimension(40, 20));
		GridBagConstraints gbc_comboBox_F = new GridBagConstraints();
		gbc_comboBox_F.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox_F.gridx = 4;
		gbc_comboBox_F.gridy = 7;
		this.panel_Content.add(this.comboBox_F, gbc_comboBox_F);

	}

	private void expandContent(ActionEvent e)
	{
		this.panel_Content.setVisible(this.chckbxExpand.isSelected());
		this.doLayout();
	}

}
