package labor;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

@SuppressWarnings("serial")
public class Frame_Main extends JFrame
{
	JPanel Panel_Main;

	public Frame_Main()
	{
		try	{
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {	}	// try to set look and feel but dont cry if it dosnt work...

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(100, 100, 682, 537);
		JPanel panel_Main = new Panel_Main();
//		panel_Main.setBorder(new EmptyBorder(0, 0, 0, 0));
//		panel_Main.setLayout(new BorderLayout(0, 0));
		this.setContentPane(panel_Main);
	}
}
