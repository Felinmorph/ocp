package labor;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class Panel_Brauvorgang extends JPanel
{

	public Panel_Brauvorgang()
	{
		this.initGUI();
	}

	private void initGUI()
	{
		this.setBorder(new EmptyBorder(0, 0, 10, 0));
		this.setLayout(new BorderLayout(0, 0));

		JLabel lblWerteDesAlchimisten = new JLabel("Brauvorgang:");
		lblWerteDesAlchimisten.setFont(new Font("Tahoma", Font.PLAIN, 16));
		this.add(lblWerteDesAlchimisten, BorderLayout.NORTH);

		JPanel panel_1 = new JPanel();
		this.add(panel_1, BorderLayout.CENTER);
		panel_1.setBorder(new EmptyBorder(5, 20, 0, 0));
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 298, 0 };
		gbl_panel_1.rowHeights = new int[] { -1, 0 };
		gbl_panel_1.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		JPanel panel_Werte = new Panel_Labor_Werte();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.anchor = GridBagConstraints.NORTH;
		gbc_panel_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 0;
		panel_1.add(panel_Werte, gbc_panel_2);
	}
}
