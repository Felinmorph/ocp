package labor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;

@SuppressWarnings("serial")
public class Panel_Main extends JPanel
{

	public Panel_Main()
	{
		this.setLayout(new BorderLayout(0, 0));

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		this.add(panel_1, BorderLayout.NORTH);

		JLabel lblDsaLabor = new JLabel("DSA - Labor");
		lblDsaLabor.setFont(new Font("Tahoma", Font.BOLD, 24));
		panel_1.add(lblDsaLabor);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(new MatteBorder(0, 1, 1, 1, new Color(0, 0, 0)));
		this.add(scrollPane, BorderLayout.CENTER);
		scrollPane.getVerticalScrollBar().setUnitIncrement(10);

		JPanel panel = new JPanel();
		scrollPane.setViewportView(panel);
		panel.setBorder(new CompoundBorder(null, new EmptyBorder(10, 10, 10, 10)));
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{-1, 0};
		gbl_panel.rowHeights = new int[]{-1, -1, -1, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
				JPanel panel_Alchimist = new Panel_Alchimist();
				GridBagConstraints gbc_panel_Alchimist = new GridBagConstraints();
				gbc_panel_Alchimist.fill = GridBagConstraints.HORIZONTAL;
				gbc_panel_Alchimist.insets = new Insets(0, 0, 5, 0);
				gbc_panel_Alchimist.gridx = 0;
				gbc_panel_Alchimist.gridy = 0;
				panel.add(panel_Alchimist, gbc_panel_Alchimist);
		
				JPanel panel_Labor = new Panel_Labor();
				GridBagConstraints gbc_panel_Labor = new GridBagConstraints();
				gbc_panel_Labor.fill = GridBagConstraints.HORIZONTAL;
				gbc_panel_Labor.insets = new Insets(0, 0, 5, 0);
				gbc_panel_Labor.gridx = 0;
				gbc_panel_Labor.gridy = 1;
				panel.add(panel_Labor, gbc_panel_Labor);
		
				JPanel panel_Rezepte = new Panel_Rezepte();
				GridBagConstraints gbc_panel_Rezepte = new GridBagConstraints();
				gbc_panel_Rezepte.fill = GridBagConstraints.HORIZONTAL;
				gbc_panel_Rezepte.gridx = 0;
				gbc_panel_Rezepte.gridy = 2;
				panel.add(panel_Rezepte, gbc_panel_Rezepte);
	}

}
